// Copyright 2018 Impreszions Solutions All Rights Reserved.
// Author : MunHon Chan

#include "CoreMinimal.h"
#include "RHI.h"
#include "RHIDefinitions.h"
#include "RHIResources.h"
#include "TextureResource.h"
#include "HAL/PlatformFilemanager.h"
#include "Engine/Texture2D.h"
#include "Engine/Texture2DDynamic.h"
#include "Engine/TextureRenderTarget2D.h"
#include "Modules/ModuleManager.h"
#include "Misc/FileHelper.h"

//~~~ Image Wrapper ~~~
#include "ImageUtils.h"
#include "IImageWrapper.h"
#include "IImageWrapperModule.h"
//~~~ Image Wrapper ~~~

#define PRE_421_INVERT_RGB	0

namespace RaveMobilePhotoGallery
{
	// Load file contents into a byte array
	extern bool LocalLoadFileToArray(TArray<uint8>& Result, const TCHAR* Filename, uint32 Flags);

	// Checks if a raw file path exists
	extern bool LocalCheckFileExists(const FString &FilePath);

	// Write FColor array into a dynamic texture2D
	void WriteToTexture2D_RenderThread(FTexture2DDynamicResource* TextureResource, const TArray<FColor>& FColorData, bool bUseSRGB = true, bool bIsRGB = true, int rotation = 0)
	{
		check(IsInRenderingThread());
		FTexture2DRHIParamRef TextureRHI = TextureResource->GetTexture2DRHI();

		int32 Width = TextureRHI->GetSizeX();
		int32 Height = TextureRHI->GetSizeY();

		uint32 DestStride = 0;
		uint8* DestData = reinterpret_cast<uint8*>(RHILockTexture2D(TextureRHI, 0, RLM_WriteOnly, DestStride, false, false));

		bool bReverse = false;
		int32 sy = 0;
		int32 ey = Height;
		int32 sx = 0;
		int32 ex = Width;

		const FColor* SrcData = &FColorData[0];
		const FColor* SrcPtr = SrcData;

		while (rotation < 0) rotation += 360;
		while (rotation >= 360) rotation -= 360;

		for (int32 y = 0; y < Height; y++)
		{
			uint8* DestPtr = &DestData[(Height - 1 - y) * DestStride];
			//const FColor* SrcPtr = &FColorData[cy * ex];
			for (int32 x = 0; x < Width; x++)
			{
				//int32 cy = bReverse ? y : (Height - 1 - y);
				//int32 cx = bReverse ? (Width - 1 - x) : x;
				//const FColor* SrcPtr = &FColorData[cy * Width + cx];

				if (rotation >= 270) SrcPtr = SrcData + (x * Height) + y;
				else if (rotation >= 180) SrcPtr = SrcData + (y * Width) + (Width - 1 - x);
				else if (rotation >= 90) SrcPtr = SrcData + ((Width - 1 - x) * Height) + (Height - 1 - y);
				else SrcPtr = SrcData + ((Height - 1 - y) * Width) + x;

				*DestPtr++ = bIsRGB ? SrcPtr->R : SrcPtr->B;
				*DestPtr++ = SrcPtr->G;
				*DestPtr++ = bIsRGB ? SrcPtr->B : SrcPtr->R;
				*DestPtr++ = SrcPtr->A;
				//SrcPtr++;
			}
		}

		RHIUnlockTexture2D(TextureRHI, 0, false, false);
	}

	// Write byte array into a dynamic texture2D
	void WriteToTexture2D_RenderThread(FTexture2DDynamicResource* TextureResource, const TArray<uint8>& RawData, bool bUseSRGB = true, bool bIsRGB = true, int rotation = 0)
	{
		check(IsInRenderingThread());
		FTexture2DRHIParamRef TextureRHI = TextureResource->GetTexture2DRHI();

		int32 Width = TextureRHI->GetSizeX();
		int32 Height = TextureRHI->GetSizeY();

		uint32 DestStride = 0;
		uint8* DestData = reinterpret_cast<uint8*>(RHILockTexture2D(TextureRHI, 0, RLM_WriteOnly, DestStride, false, false));
		const FColor* SrcData = &((FColor*)(RawData.GetData()))[0];
		const FColor* SrcPtr = SrcData;

		while (rotation < 0) rotation += 360;
		while (rotation >= 360) rotation -= 360;

		for (int32 y = 0; y < Height; y++)
		{
			uint8* DestPtr = &DestData[(Height - 1 - y) * DestStride];
			for (int32 x = 0; x < Width; x++)
			{
				if (rotation >= 270) SrcPtr = SrcData + (x * Height) + y;
				else if ( rotation >= 180 ) SrcPtr = SrcData + (y * Width) + (Width - 1 - x);
				else if ( rotation >= 90) SrcPtr = SrcData + ((Width - 1 - x) * Height) + (Height - 1 - y);
				else SrcPtr = SrcData + ((Height - 1 - y) * Width) + x;
				*DestPtr++ = bIsRGB ? SrcPtr->R : SrcPtr->B;
				*DestPtr++ = SrcPtr->G;
				*DestPtr++ = bIsRGB ? SrcPtr->B : SrcPtr->R;
				*DestPtr++ = SrcPtr->A;
				//SrcPtr++;
			}
		}

		RHIUnlockTexture2D(TextureRHI, 0, false, false);
	}

	// Generate a dynamic texture2D from an FColor array
	UTexture2DDynamic *Texture2DFrom(const TArray<FColor> &FColorData, int32 width, int32 height, bool bUseSRGB, bool bIsRGB, int rotation)
	{
		if (FColorData.Num() > 0 && width > 0 && height > 0)
		{
			if (UTexture2DDynamic* Texture = UTexture2DDynamic::Create(width, height))
			{
				Texture->SRGB = bUseSRGB;
				Texture->UpdateResource();

				ENQUEUE_UNIQUE_RENDER_COMMAND_FIVEPARAMETER(
					FWriteRawDataToTexture,
					FTexture2DDynamicResource*, TextureResource, static_cast<FTexture2DDynamicResource*>(Texture->Resource),
					TArray<FColor>, RawFColorData, *&FColorData,
					bool, bUseSRGB, bUseSRGB,
					bool, bIsRGB, bIsRGB,
					int, rotation, rotation,
					{
						WriteToTexture2D_RenderThread(TextureResource, RawFColorData, bUseSRGB, bIsRGB, rotation);
					});
				return Texture;
			}
		}
		return NULL;
	}

	// Generate a dynamic texture2D from an FColor array
	UTexture2DDynamic *Texture2DFrom(const TArray<FColor> &FColorData, int32 width, int32 height)
	{
		return Texture2DFrom(FColorData, width, height, true, true, 0);
	}

	// Generate a dynamic texture2D from a byte array
	UTexture2DDynamic *Texture2DFrom(const TArray<uint8>& RawData, int32 width, int32 height, bool bUseSRGB, bool bIsRGB, int rotation)
	{
		if (RawData.Num() > 0 && width > 0 && height > 0)
		{
			int32 texWidth = (rotation == 90 || rotation == 270) ? height : width;
			int32 texHeight = (rotation == 90 || rotation == 270) ? width : height;

			if (UTexture2DDynamic* Texture = UTexture2DDynamic::Create(texWidth, texHeight))
			{
				Texture->SRGB = bUseSRGB;
				Texture->UpdateResource();

				ENQUEUE_UNIQUE_RENDER_COMMAND_FIVEPARAMETER(
					FWriteRawDataToTexture,
					FTexture2DDynamicResource*, TextureResource, static_cast<FTexture2DDynamicResource*>(Texture->Resource),
					TArray<uint8>, RawFColorData, *&RawData,
					bool, bUseSRGB, bUseSRGB,
					bool, bIsRGB, bIsRGB,
					int, rotation, rotation,
					{
						WriteToTexture2D_RenderThread(TextureResource, RawFColorData, bUseSRGB, bIsRGB, rotation);
					});
				return Texture;
			}
		}
		return NULL;
	}

	// Generate a dynamic texture2D from a byte array
	UTexture2DDynamic *Texture2DFrom(const TArray<uint8>& RawData, int32 width, int32 height)
	{
		return Texture2DFrom(RawData, width, height, true, true, 0);
	}

	// Generate FColor array from a Texture2D
	TArray<FColor> FColorArrayFrom(UTexture2D *texture, int32& width, int32& height)
	{
		TArray<FColor> imageData;
		width = 0;
		height = 0;

		if (texture)
		{
			if (texture->CompressionSettings != TC_VectorDisplacementmap)
			{
				UE_LOG(LogTemp, Log, TEXT("Color Array From UTexture: Texture compression setting is not TC_VectorDisplacementmap"));
			}
			else
			{
				FTexture2DMipMap& texMipMap = texture->PlatformData->Mips[0];
				FByteBulkData* bulkData = &texMipMap.BulkData;
				if (bulkData)
				{
					width = texMipMap.SizeX;
					height = texMipMap.SizeY;
					FColor* colorArray = static_cast<FColor*>(bulkData->Lock(LOCK_READ_ONLY));
					for (int32 x = 0; x < width; x++)
					{
						for (int32 y = 0; y < height; y++)
						{
							imageData.Add(colorArray[x * width + y]);
						}
					}
					bulkData->Unlock();
				}
			}
		}
		return imageData;
	}

	// Generate FColor array from a Texture2D resized to specified width/height
	TArray<FColor> FColorArrayFrom(UTexture2D *texture, int32 outputWidth, int32 outputHeight, int32& width, int32& height)
	{
		TArray<FColor> imageData;
		width = 0;
		height = 0;

		if (texture)
		{
			if (texture->CompressionSettings != TC_VectorDisplacementmap)
			{
				UE_LOG(LogTemp, Log, TEXT("Color Array From UTexture2D: Texture compression setting is not TC_VectorDisplacementmap"));
			}
			else
			{
				FTexture2DMipMap& texMipMap = texture->PlatformData->Mips[0];
				FByteBulkData* bulkData = &texMipMap.BulkData;
				if (bulkData)
				{
					width = texMipMap.SizeX;
					height = texMipMap.SizeY;

					float fwidth = (float)width;
					float fheight = (float)height;
					float fowidth = (float)outputWidth;
					float foheight = (float)outputHeight;

					FColor* colorArray = static_cast<FColor*>(bulkData->Lock(LOCK_READ_ONLY));
					for (float x = 0; x < fowidth; x++)
					{
						for (float y = 0; y < foheight; y++)
						{
							int32 resizedx = FMath::RoundToInt((x / fowidth) * fwidth);
							int32 resizedy = FMath::RoundToInt((y / foheight) * fheight);
							imageData.Add(colorArray[resizedx * width + resizedy]);
						}
					}
					bulkData->Unlock();
				}
			}
		}
		return imageData;
	}

	// Draw FColor array to a Texture2D
	TArray<FColor> drawTexture(const TArray<FColor>& source, int32 sourceWidth, int32 sourceHeight,
		UTexture2D *texture, FVector2D size, FVector2D offset, FVector2D anchor, FVector2D alignment, float opacity = 1)
	{
		TArray<FColor> imageData = source;
		if (texture)
		{
			if (texture->CompressionSettings != TC_VectorDisplacementmap)
			{
				UE_LOG(LogTemp, Log, TEXT("DrawTexture to Color Array: Texture compression setting is not TC_VectorDisplacementmap"));
			}
			else
			{
				FTexture2DMipMap& texMipMap = texture->PlatformData->Mips[0];
				FByteBulkData* bulkData = &texMipMap.BulkData;
				if (bulkData)
				{
					float textureWidth = texMipMap.SizeX;
					float textureHeight = texMipMap.SizeY;
					float w = size.X; // (size.X <= 0) ? watermark.size.width : size.X;
					float h = size.Y; // (size.Y <= 0) ? watermark.size.height : size.Y;

					if (w <= 0 && h <= 0)
					{
						w = textureWidth;
						h = textureHeight;
					}
					else if (w <= 0)
					{
						h = textureHeight;
						w = (h / textureHeight) * textureWidth;
					}
					else if (h <= 0)
					{
						w = textureWidth;
						h = (w / textureWidth) * textureHeight;
					}

					int32 startx = FMath::FloorToInt(offset.X + anchor.X * sourceWidth - alignment.X * w);
					int32 starty = FMath::FloorToInt(offset.Y + anchor.Y * sourceHeight - alignment.Y * h);
					int32 endx = startx + FMath::FloorToInt(w);
					int32 endy = starty + FMath::FloorToInt(h);

					FColor* colorArray = static_cast<FColor*>(bulkData->Lock(LOCK_READ_ONLY));
					for (int32 sx = startx; sx < endx; sx++)
					{
						for (int32 sy = starty; sy < endy; sy++)
						{
							int32 srcindex = sy * sourceWidth + sx;
							if (sx >= startx && sx <= endx && sy >= starty && sy <= endy)
							{
								float x = (float)(sx - startx);
								float y = (float)(sy - starty);

								int32 resizedx = FMath::FloorToInt((x / w) * textureWidth);
								int32 resizedy = FMath::FloorToInt((y / h) * textureHeight);
								int32 targetIndex = resizedy * textureWidth + resizedx;

								FColor newColor = colorArray[targetIndex];
								float alpha = opacity * (newColor.A / 255.0f);
								imageData[srcindex].R = uint8_t(source[srcindex].R / 255.0f * (1 - alpha) * 255.0f) + uint8_t(newColor.R / 255.0f * alpha * 255.0f);
								imageData[srcindex].G = uint8_t(source[srcindex].G / 255.0f * (1 - alpha) * 255.0f) + uint8_t(newColor.G / 255.0f * alpha * 255.0f);
								imageData[srcindex].B = uint8_t(source[srcindex].B / 255.0f * (1 - alpha) * 255.0f) + uint8_t(newColor.B / 255.0f * alpha * 255.0f);
								imageData[srcindex].A = uint8_t(source[srcindex].A / 255.0f * (1 - alpha) * 255.0f) + uint8_t(newColor.A / 255.0f * alpha * 255.0f);
							}
						}
					}
					bulkData->Unlock();
				}
			}
		}
		return imageData;
	}

#if PLATFORM_ANDROID
	// Create AndroidBitmap object
	extern jobject AndroidThunkCpp_CreateBitmap(int width, int height);

	// Generate a Texture2D from an AndroidBitmap object
	UTexture2DDynamic *Texture2DFromAndroidBitmap(const TArray<FColor> &FColorData, int32 width, int32 height)
	{
		if (FColorData.Num() > 0 && width > 0 && height > 0)
		{
			if (UTexture2DDynamic* Texture = UTexture2DDynamic::Create(width, height))
			{
				Texture->SRGB = true;
				Texture->UpdateResource();

				ENQUEUE_UNIQUE_RENDER_COMMAND_TWOPARAMETER(
					FWriteRawDataToTexture,
					FTexture2DDynamicResource*, TextureResource, static_cast<FTexture2DDynamicResource*>(Texture->Resource),
					TArray<FColor>, RawFColorData, *&FColorData,
					{
						WriteToTexture2D_RenderThread(TextureResource, RawFColorData, true, true);
					});
				return Texture;
			}
		}
		return NULL;
	}

	// Generate an AndroidBitmap from FColor array
	jobject AndroidBitmapFrom(const TArray<FColor> &RawColorArray, int32 textureWidth, int32 textureHeight, bool premultiplyAlpha = false, bool bIsRGB = false)
	{
		if (JNIEnv* env = FAndroidApplication::GetJavaEnv())
		{
			jclass java_bitmap_class = (jclass)env->FindClass("android/graphics/Bitmap");
			jmethodID mid = env->GetStaticMethodID(java_bitmap_class, "createBitmap", "(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;");

			//const wchar_t config_name[] = L"ARGB_8888";
			jstring j_config_name = env->NewStringUTF("ARGB_8888"); //env->NewString((const jchar*)config_name, wcslen(config_name));
			jclass bcfg_class = env->FindClass("android/graphics/Bitmap$Config");
			jobject java_bitmap_config = env->CallStaticObjectMethod(bcfg_class, env->GetStaticMethodID(bcfg_class, "valueOf", "(Ljava/lang/String;)Landroid/graphics/Bitmap$Config;"), j_config_name);
			jobject bitmap = env->CallStaticObjectMethod(java_bitmap_class, mid, textureWidth, textureHeight, java_bitmap_config);
			jintArray pixels = env->NewIntArray(textureWidth * textureHeight);

			const size_t Width = textureWidth;
			const size_t Height = textureHeight;
			const size_t Area = Width * Height;
			const size_t ComponentsPerPixel = 4; // rgba
			const size_t NumComponents = Area * ComponentsPerPixel;

			for (size_t y = 0; y < Height; y++)
			{
				for (size_t x = 0; x < Width; x++)
				{
					size_t outputOffset = y * Width + x;
					size_t offset = (Height - 1 - y) * Width + x;
					size_t pixelOffset = offset * ComponentsPerPixel;
					float premultipliedAlpha = premultiplyAlpha ? (RawColorArray[offset].A / 255.0f) : 1.0f;

					uint8 red = uint8_t(RawColorArray[offset].R / 255.0f * premultipliedAlpha * 255.0f);
					uint8 green = uint8_t(RawColorArray[offset].G / 255.0f * premultipliedAlpha * 255.0f);
					uint8 blue = uint8_t(RawColorArray[offset].B / 255.0f * premultipliedAlpha * 255.0f);
					uint8 alpha = RawColorArray[offset].A; // size_t(RawColorArray[offset].A * opacity);
					int32 currentPixel = (alpha << 24) | (red << 16) | (green << 8) | (blue);
					env->SetIntArrayRegion(pixels, outputOffset, 1, &currentPixel);
				}
			}

			jmethodID setPixelsMid = env->GetMethodID(java_bitmap_class, "setPixels", "([IIIIIII)V");
			env->CallVoidMethod(bitmap, setPixelsMid, pixels, 0, textureWidth, 0, 0, textureWidth, textureHeight);
			return bitmap;
		}
		return nullptr;
	}

	// Generate AndroidBitmap from a Texture2D
	jobject AndroidBitmapFrom(UTexture2D *texture, float opacity = 1.0f, bool premultiplyAlpha = true)
	{
		if (texture == nullptr)
		{
			UE_LOG(LogTemp, Log, TEXT("AndroidBitmapFrom from UTexture2D Error: Texture is nil"));
			return nullptr;
		}

		if (!texture->PlatformData)
		{
			UE_LOG(LogTemp, Log, TEXT("AndroidBitmapFrom from UTexture2D Error: PlatformData is nil"));
			return nullptr;
		}

		if (texture->PlatformData->Mips.Num() <= 0)
		{
			UE_LOG(LogTemp, Log, TEXT("AndroidBitmapFrom from UTexture2D Error: MipMaps is nil"));
			return nullptr;
		}

		if (texture->CompressionSettings != TC_VectorDisplacementmap)
		{
			UE_LOG(LogTemp, Log, TEXT("AndroidBitmapFrom from UTexture2D Error: Texture Compression Settings is not VectorDisplacementMap"));
			return nullptr;
		}

		FTexture2DMipMap& textureMipMap = texture->PlatformData->Mips[0];
		int32 textureWidth = textureMipMap.SizeX;
		int32 textureHeight = textureMipMap.SizeY;

		if (textureWidth <= 0 || textureHeight <= 0)
		{
			UE_LOG(LogTemp, Log, TEXT("AndroidBitmapFrom from UTexture2D Error: TextureWidth and / or TextureHeight is 0"));
			return nullptr;
		}
		FByteBulkData* RawImageData = &textureMipMap.BulkData;

		if (!RawImageData)
		{
			UE_LOG(LogTemp, Log, TEXT("AndroidBitmapFrom from UTexture2D Error: Texture BulkData is nil"));
			return nullptr;
		}

		FColor* RawColorArray = static_cast<FColor*>(RawImageData->Lock(LOCK_READ_ONLY));
		if (RawColorArray)
		{
			jobject bitmap = nullptr;
			if (JNIEnv* env = FAndroidApplication::GetJavaEnv())
			{
				jclass java_bitmap_class = (jclass)env->FindClass("android/graphics/Bitmap");
				jmethodID mid = env->GetStaticMethodID(java_bitmap_class, "createBitmap", "(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;");

				//const wchar_t config_name[] = L"ARGB_8888";
				jstring j_config_name = env->NewStringUTF("ARGB_8888"); //env->NewString((const jchar*)config_name, wcslen(config_name));
				jclass bcfg_class = env->FindClass("android/graphics/Bitmap$Config");
				jobject java_bitmap_config = env->CallStaticObjectMethod(bcfg_class, env->GetStaticMethodID(bcfg_class, "valueOf", "(Ljava/lang/String;)Landroid/graphics/Bitmap$Config;"), j_config_name);
				bitmap = env->CallStaticObjectMethod(java_bitmap_class, mid, textureWidth, textureHeight, java_bitmap_config);
				jintArray pixels = env->NewIntArray(textureWidth * textureHeight);

				const size_t Width = textureWidth;
				const size_t Height = textureHeight;
				const size_t Area = Width * Height;
				const size_t ComponentsPerPixel = 4; // rgba
				const size_t NumComponents = Area * ComponentsPerPixel;

				for (size_t y = 0; y < Height; y++)
				{
					for (size_t x = 0; x < Width; x++)
					{
						size_t outputOffset = y * Width + x;
						size_t offset = (Height - 1 - y) * Width + x;
						size_t pixelOffset = offset * ComponentsPerPixel;
						float premultipliedAlpha = premultiplyAlpha ? (RawColorArray[offset].A / 255.0f) : 1.0f;

						uint8 red = uint8_t(RawColorArray[offset].R / 255.0f * premultipliedAlpha * 255.0f);
						uint8 green = uint8_t(RawColorArray[offset].G / 255.0f * premultipliedAlpha * 255.0f);
						uint8 blue = uint8_t(RawColorArray[offset].B / 255.0f * premultipliedAlpha * 255.0f);
						uint8 alpha = RawColorArray[offset].A; // size_t(RawColorArray[offset].A * opacity);
						int32 currentPixel = (alpha << 24) | (red << 16) | (green << 8) | (blue);
						env->SetIntArrayRegion(pixels, outputOffset, 1, &currentPixel);
					}
				}

				jmethodID setPixelsMid = env->GetMethodID(java_bitmap_class, "setPixels", "([IIIIIII)V");
				env->CallVoidMethod(bitmap, setPixelsMid, pixels, 0, textureWidth, 0, 0, textureWidth, textureHeight);
			}
			RawImageData->Unlock();
			return bitmap;
		}

		UE_LOG(LogTemp, Log, TEXT("AndroidBitmapFrom from UTexture2D Error: Unable to retrieve texture data"));
		return nullptr;
	}

	// Generate AndroidBitmap from a RenderTarget2D
	jobject AndroidBitmapFrom(UTextureRenderTarget2D *texture, float opacity = 1.0f, bool premultiplyAlpha = true)
	{
		if (texture == nullptr)
		{
			return nullptr;
		}

		FRenderTarget* RenderTarget = texture->GameThread_GetRenderTargetResource();
		if (!RenderTarget)
		{
			return nullptr;
		}

		int32 textureWidth = texture->SizeX;
		int32 textureHeight = texture->SizeY;
		if (textureWidth <= 0 || textureHeight <= 0)
		{
			return nullptr;
		}

		TArray<FColor> SurfData;
		RenderTarget->ReadPixels(SurfData);
		if (SurfData.Num() <= 0)
		{
			return nullptr;
		}

		return AndroidBitmapFrom(SurfData, textureWidth, textureHeight, premultiplyAlpha, false);
	}

	// Generate AndroidBitmap from a generic texture
	jobject AndroidBitmapFrom(UTexture *texture, float opacity = 1.0f, bool premultiplyAlpha = true)
	{
		if (texture)
		{
			if (texture->IsA(UTexture2D::StaticClass()))
			{
				return AndroidBitmapFrom((UTexture2D*)texture, opacity, premultiplyAlpha);
			}
			else if (texture->IsA(UTextureRenderTarget2D::StaticClass()))
			{
				return AndroidBitmapFrom((UTextureRenderTarget2D*)texture, opacity, premultiplyAlpha);
			}
			return nullptr;
		}
		return nullptr;
	}
#endif


	// Credits to Rama
	// From Rama Victory Plugin: https://github.com/EverNewJoy/VictoryPlugin/blob/master/Source/VictoryBPLibrary/Private/VictoryBPFunctionLibrary.cpp
	// Copyright Permissions: https://forums.unrealengine.com/development-discussion/blueprint-visual-scripting/4014-39-rama-s-extra-blueprint-nodes-for-you-as-a-plugin-no-c-required
	static TSharedPtr<IImageWrapper> GetImageWrapperByExtention(const FString InImagePath)
	{
		IImageWrapperModule& ImageWrapperModule = FModuleManager::LoadModuleChecked<IImageWrapperModule>(FName("ImageWrapper"));
		if (InImagePath.EndsWith(".png"))
		{
			return ImageWrapperModule.CreateImageWrapper(EImageFormat::PNG);
		}
		else if (InImagePath.EndsWith(".jpg") || InImagePath.EndsWith(".jpeg"))
		{
			return ImageWrapperModule.CreateImageWrapper(EImageFormat::JPEG);
		}
		else if (InImagePath.EndsWith(".bmp"))
		{
			return ImageWrapperModule.CreateImageWrapper(EImageFormat::BMP);
		}
		else if (InImagePath.EndsWith(".ico"))
		{
			return ImageWrapperModule.CreateImageWrapper(EImageFormat::ICO);
		}
		else if (InImagePath.EndsWith(".exr"))
		{
			return ImageWrapperModule.CreateImageWrapper(EImageFormat::EXR);
		}
		else if (InImagePath.EndsWith(".icns"))
		{
			return ImageWrapperModule.CreateImageWrapper(EImageFormat::ICNS);
		}

		return nullptr;
	}

	// Credits to Rama
	// From Rama Victory Plugin: https://github.com/EverNewJoy/VictoryPlugin/blob/master/Source/VictoryBPLibrary/Private/VictoryBPFunctionLibrary.cpp
	// Copyright Permissions: https://forums.unrealengine.com/development-discussion/blueprint-visual-scripting/4014-39-rama-s-extra-blueprint-nodes-for-you-as-a-plugin-no-c-required
	UTexture2D* Texture2DFromFile(TSharedPtr<IImageWrapper> ImageWrapper, TArray<uint8> CompressedData,
		const FString& ImagePath, bool& IsValid, int32& OutWidth, int32& OutHeight)
	{
		UTexture2D* Texture = nullptr;

		if (ImageWrapper.IsValid() && ImageWrapper->SetCompressed(CompressedData.GetData(), CompressedData.Num()))
		{
			const TArray<uint8>* UncompressedRGBA = nullptr;

			if (ImageWrapper->GetRaw(ERGBFormat::RGBA, 8, UncompressedRGBA))
			{
				Texture = UTexture2D::CreateTransient(ImageWrapper->GetWidth(), ImageWrapper->GetHeight(), PF_R8G8B8A8);

				if (Texture != nullptr)
				{
					IsValid = true;

					OutWidth = ImageWrapper->GetWidth();
					OutHeight = ImageWrapper->GetHeight();

					void* TextureData = Texture->PlatformData->Mips[0].BulkData.Lock(LOCK_READ_WRITE);
					FMemory::Memcpy(TextureData, UncompressedRGBA->GetData(), UncompressedRGBA->Num());
					Texture->PlatformData->Mips[0].BulkData.Unlock();
					Texture->UpdateResource();
				}
			}
		}

		return Texture;
	}

	// Get Texture2D from an image file on disk
	UTexture2D* Texture2DFromFile(TSharedPtr<IImageWrapper> ImageWrapper, const FString& ImagePath, bool& IsValid, int32& OutWidth, int32& OutHeight)
	{
		//UTexture2D* Texture = nullptr;
		IsValid = false;

		// To avoid log spam, make sure it exists before doing anything else.
		if (!FPlatformFileManager::Get().GetPlatformFile().FileExists(*ImagePath))
		{
			return nullptr;
		}

		TArray<uint8> CompressedData;
		if (!FFileHelper::LoadFileToArray(CompressedData, *ImagePath))
		{
			return nullptr;
		}

		return Texture2DFromFile(ImageWrapper, CompressedData, ImagePath, IsValid, OutWidth, OutHeight);
	}

	// Get Texture2D from an image file on disk
	UTexture2D* Texture2DFromFile(const FString& ImagePath, bool& IsValid, int32& OutWidth, int32& OutHeight)
	{
		TSharedPtr<IImageWrapper> ImageWrapper = GetImageWrapperByExtention(ImagePath);
		return Texture2DFromFile(ImageWrapper, ImagePath, IsValid, OutWidth, OutHeight);
	}

	// Get Texture2DDynamic from an image file on disk
	UTexture2DDynamic* Texture2DFromFileDynamic(TSharedPtr<IImageWrapper> ImageWrapper, const FString& ImagePath, bool& IsValid, int32& OutWidth, int32& OutHeight, int rotation = 0)
	{
		IsValid = false;

		// To avoid log spam, make sure it exists before doing anything else.
		if (!LocalCheckFileExists(ImagePath))
		{
			UE_LOG(LogTemp, Log, TEXT("Texture2DFromFileDynamic from ImageWrapper Error: File does not exist: %s"), *ImagePath);
			return nullptr;
		}

		TArray<uint8> CompressedData;
		if (LocalLoadFileToArray(CompressedData, *ImagePath, 0U))
		{
			if (ImageWrapper.IsValid() && ImageWrapper->SetCompressed(CompressedData.GetData(), CompressedData.Num()))
			{
				const TArray<uint8>* UncompressedRGBA = nullptr;

				if (ImageWrapper->GetRaw(ERGBFormat::RGBA, 8, UncompressedRGBA))
				{
					IsValid = true;
					OutWidth = ImageWrapper->GetWidth();
					OutHeight = ImageWrapper->GetHeight();

					bool bIsRGB = true;
#if PRE_421_INVERT_RGB
					if (ImagePath.EndsWith(".jpg") || ImagePath.EndsWith(".jpeg"))
					{
						bIsRGB = !bIsRGB;
					}
#endif

					TArray<uint8> arrayData = TArray<uint8>(UncompressedRGBA->GetData(), UncompressedRGBA->Num());
					//if (rotation != 0) RotatePixelArray(arrayData, OutWidth, OutHeight, rotation);
					return Texture2DFrom(arrayData, OutWidth, OutHeight, true, bIsRGB, rotation);
				}
				else
				{
					UE_LOG(LogTemp, Log, TEXT("Texture2DFromFileDynamic from ImageWrapper Error: ImageWrapper GetRaw returned error"));
				}
			}
			else
			{
				UE_LOG(LogTemp, Log, TEXT("Texture2DFromFileDynamic from ImageWrapper Error: ImageWrapper is not valid."));
			}
		}
		else
		{
			UE_LOG(LogTemp, Log, TEXT("Texture2DFromFileDynamic from ImageWrapper Error: Unable to load file %s."), *ImagePath);
		}

		return nullptr;
	}

	// Get Texture2DDynamic from an image file on disk
	UTexture2DDynamic* Texture2DFromFileDynamic(const FString& ImagePath, bool& IsValid, int32& OutWidth, int32& OutHeight, int rotation = 0)
	{
		TSharedPtr<IImageWrapper> ImageWrapper = GetImageWrapperByExtention(ImagePath);
		return Texture2DFromFileDynamic(ImageWrapper, ImagePath, IsValid, OutWidth, OutHeight, rotation);
	}

	// Rotate contents of a FColor array
	void RotatePixelArray(TArray<FColor> &bitmap, int &width, int &height, int rotation)
	{
		// Referenced from
		// https://www.geeksforgeeks.org/inplace-m-x-n-size-matrix-transpose/

		int r = width;
		int c = height;
		int size = r * c - 1;
		while (rotation < 0) rotation += 360;
		while (rotation >= 360) rotation -= 360;

		if (rotation == 90 || rotation == 270)
		{
			int next; // location of 't' to be moved
			int cycleBegin; // holds start of cycle
			int i; // iterator
			bool *b = new bool[size]; // hash to mark moved elements
			for (i = 1; i < size - 1; i++) b[i] = 0;
			b[0] = b[size - 1] = 1;
			i = 1; // Note that A[0] and A[size-1] won't move
			while (i < size)
			{
				cycleBegin = i;
				do
				{
					// Input matrix [r x c]
					// Output matrix 
					// i_new = (i*r)%(N-1)
					next = (i*r) % size;
					bitmap.SwapMemory(next, i);
					b[i] = 1;
					i = next;
				} while (i != cycleBegin);

				// Get Next Move (what about querying random location?)
				for (i = 1; i < size && b[i]; i++)
					;
			}
			delete[] b;

			width = c;
			height = r;

			for (int32 y = 0; y < height; y++)
			{
				for (int32 x = 0; x < width / 2; x++)
				{
					//				bitmap.SwapMemory(y * width + x, y * width + width - 1 - x);
					//				bitmap.SwapMemory(y * width + x, (height - 1 - y) * width + (width - 1 - x));
					bitmap.SwapMemory(y * width + x, y * width + (width - 1 - x));
				}
			}
		}

		if (rotation == 180 || rotation == 90)
		{
			for (int32 i = 0; i < (size / 2); i++)
			{
				bitmap.SwapMemory(i, size - 1 - i);
			}
		}
	}

	// Rotate contents of a FColor array (size_t version)
	void RotatePixelArray(TArray<FColor> &bitmap, size_t &width, size_t &height, int rotation)
	{
		// Referenced from
		// https://www.geeksforgeeks.org/inplace-m-x-n-size-matrix-transpose/

		int32 r = width;
		int32 c = height;
		int32 size = r * c - 1;
		while (rotation < 0) rotation += 360;
		while (rotation >= 360) rotation -= 360;

		if (rotation == 90 || rotation == 270)
		{
			int32 next; // location of 't' to be moved
			int32 cycleBegin; // holds start of cycle
			int32 i; // iterator
			bool *b = new bool[size]; // hash to mark moved elements
			for (i = 1; i < size - 1; i++) b[i] = 0;
			b[0] = b[size - 1] = 1;
			i = 1; // Note that A[0] and A[size-1] won't move
			while (i < size)
			{
				cycleBegin = i;
				do
				{
					// Input matrix [r x c]
					// Output matrix 
					// i_new = (i*r)%(N-1)
					next = (i*r) % size;
					bitmap.SwapMemory(next, i);
					b[i] = 1;
					i = next;
				} while (i != cycleBegin);

				// Get Next Move (what about querying random location?)
				for (i = 1; i < size && b[i]; i++)
					;
			}
			delete[] b;

			width = c;
			height = r;

			for (int32 y = 0; y < height; y++)
			{
				for (int32 x = 0; x < width / 2; x++)
				{
					//				bitmap.SwapMemory(y * width + x, y * width + width - 1 - x);
					//				bitmap.SwapMemory(y * width + x, (height - 1 - y) * width + (width - 1 - x));
					bitmap.SwapMemory(y * width + x, y * width + (width - 1 - x));
				}
			}
		}

		if (rotation == 180 || rotation == 90)
		{
			for (int32 i = 0; i < (size / 2); i++)
			{
				bitmap.SwapMemory(i, size - 1 - i);
			}
		}
	}
	
	// Rotate contents of a FColor array (int array version)
	void RotatePixelArray(TArray<uint8> &bitmap, int32 &width, int32 &height, int rotation)
	{
		// Referenced from
		// https://www.geeksforgeeks.org/inplace-m-x-n-size-matrix-transpose/

		int32 r = width;
		int32 c = height;
		int32 size = r * c - 1;
		while (rotation < 0) rotation += 360;
		while (rotation >= 360) rotation -= 360;

		UE_LOG(LogTemp, Log, TEXT("Rotate Pixel: %d x %d = %d -- %d -- %d"), r, c, size, bitmap.Num(), rotation);

		if (rotation == 90 || rotation == 270)
		{
			UE_LOG(LogTemp, Log, TEXT("Rotate Pixel 90: %d x %d = %d -- %d"), r, c, size, rotation);

			int32 next; // location of 't' to be moved
			int32 cycleBegin; // holds start of cycle
			int32 i; // iterator
			bool *b = new bool[size]; // hash to mark moved elements
			for (i = 1; i < size - 1; i++) b[i] = 0;
			b[0] = b[size - 1] = 1;
			i = 1; // Note that A[0] and A[size-1] won't move
			while (i < size)
			{
				cycleBegin = i;
				do
				{
					// Input matrix [r x c]
					// Output matrix 
					// i_new = (i*r)%(N-1)
					next = (i*r) % size;

					UE_LOG(LogTemp, Log, TEXT("Rotate Pixel Swap 1: %d x %d x %d"), next, i, size);
					bitmap.SwapMemory(next * 4 + 0, i * 4 + 0);
					bitmap.SwapMemory(next * 4 + 1, i * 4 + 1);
					bitmap.SwapMemory(next * 4 + 2, i * 4 + 2);
					bitmap.SwapMemory(next * 4 + 3, i * 4 + 3);
					b[i] = 1;
					i = next;
				} while (i != cycleBegin);

				// Get Next Move (what about querying random location?)
				for (i = 1; i < size && b[i]; i++)
					;
			}
			delete[] b;

			width = c;
			height = r;

			for (int y = 0; y < height; y++)
			{
				for (int x = 0; x < width / 2; x++)
				{
					UE_LOG(LogTemp, Log, TEXT("Rotate Pixel Swap 2: %d x %d x %d"), y, x, size);
					bitmap.SwapMemory((y * width + x) * 4 + 0, (y * width + (width - 1 - x)) * 4 + 0);
					bitmap.SwapMemory((y * width + x) * 4 + 1, (y * width + (width - 1 - x)) * 4 + 1);
					bitmap.SwapMemory((y * width + x) * 4 + 2, (y * width + (width - 1 - x)) * 4 + 2);
					bitmap.SwapMemory((y * width + x) * 4 + 3, (y * width + (width - 1 - x)) * 4 + 3);
				}
			}
		}

		if (rotation == 180 || rotation == 90)
		{
			for (int i = 0; i < (size / 2); i++)
			{
				bitmap.SwapMemory(i * 4 + 0, (size - 1 - i) * 4 + 0);
				bitmap.SwapMemory(i * 4 + 1, (size - 1 - i) * 4 + 1);
				bitmap.SwapMemory(i * 4 + 2, (size - 1 - i) * 4 + 2);
				bitmap.SwapMemory(i * 4 + 3, (size - 1 - i) * 4 + 3);
			}
		}
	}

	// Generate UIImage from a Texture
	bool FColorFrom(UTexture2D *texture, TArray<FColor> &Results, uint32 &outWidth, uint32 &outHeight)
	{
		if (texture)
		{
			ENQUEUE_UNIQUE_RENDER_COMMAND_TWOPARAMETER(
				FArrayFromTexture,
				UTexture2D*, texture, texture,
				TArray<FColor>&, Results, Results,
				{
					FTexture2DResource* TextureResource = static_cast<FTexture2DResource*>(texture->Resource);
					FTexture2DRHIParamRef TextureRHI = TextureResource->GetTexture2DRHI();
					int32 textureWidth = TextureRHI->GetSizeX();
					int32 textureHeight = TextureRHI->GetSizeY();
					uint32 DestStride = 0;
					if (textureWidth <= 0 || textureHeight <= 0)
					{
						UE_LOG(LogTemp, Log, TEXT("IOS UIImageFrom Texture Error: TextureWidth and / or TextureHeight is 0"));
						return;
					}
					FColor* RawColorArray = reinterpret_cast<FColor*>(RHILockTexture2D(TextureRHI, 0, RLM_ReadOnly, DestStride, false, false));
					Results.Append(RawColorArray, textureWidth * textureHeight);
					RHIUnlockTexture2D(TextureRHI, 0, false, false);
				});
			return true;
		}
		return false;
	}

	bool FColorFrom(UTextureRenderTarget2D *texture, TArray<FColor> &Results, uint32 &outWidth, uint32 &outHeight)
	{
		if (texture)
		{
			if (FRenderTarget* RenderTarget = texture->GameThread_GetRenderTargetResource())
			{
				int32 textureWidth = texture->SizeX;
				int32 textureHeight = texture->SizeY;
				if (textureWidth <= 0 || textureHeight <= 0)
				{
					UE_LOG(LogTemp, Log, TEXT("IOS UIImage From UTextureRenderTarget2D: Render Target Width and / or Height is 0"));
				}
				else
				{
					RenderTarget->ReadPixels(Results);
					return true;
				}
			}
		}
		return false;
	}

	bool FColorFrom(UTexture2DDynamic *texture, TArray<FColor> &Results, uint32 &outWidth, uint32 &outHeight)
	{
		UE_LOG(LogTemp, Log, TEXT("FColor From 1"));
		if (texture)
		{
			UE_LOG(LogTemp, Log, TEXT("FColor From 2"));
			outWidth = texture->GetSurfaceWidth();
			outHeight = texture->GetSurfaceHeight();

			struct FColorFromContext
			{
				UTexture2DDynamic* texture;
				TArray<FColor>* OutData;
				FIntRect Rect;
				FReadSurfaceDataFlags Flags;
			};
			UE_LOG(LogTemp, Log, TEXT("FColor From 3"));
			Results.Reset();
			FColorFromContext Context =
			{
				texture,
				&Results,
				FIntRect(0, 0, texture->GetSurfaceWidth(), texture->GetSurfaceHeight()),
				FReadSurfaceDataFlags()
			};
		
			UE_LOG(LogTemp, Log, TEXT("FColor From 4"));
			/*
			ENQUEUE_UNIQUE_RENDER_COMMAND_ONEPARAMETER(
				ColorFromCommand,
				FColorFromContext, Context, ColorFromContext,
				{
					UE_LOG(LogTemp, Log, TEXT("FColor From 5"));

					FTexture2DDynamicResource* TextureResource = static_cast<FTexture2DDynamicResource*>(Context.texture->Resource);

					UE_LOG(LogTemp, Log, TEXT("FColor From 6"));

					FTexture2DRHIParamRef TextureRHI = TextureResource->GetTexture2DRHI();

					UE_LOG(LogTemp, Log, TEXT("FColor From 7"));

					RHICmdList.ReadSurfaceData(
						TextureRHI,
						Context.Rect,
						*Context.OutData,
						Context.Flags
					);

					UE_LOG(LogTemp, Log, TEXT("FColor From 8"));

				});
			UE_LOG(LogTemp, Log, TEXT("FColor From 9"));

			FlushRenderingCommands();
			UE_LOG(LogTemp, Log, TEXT("FColor From 10"));
			*/
			texture->UpdateResource();
			ENQUEUE_UNIQUE_RENDER_COMMAND_ONEPARAMETER(
				ColorFromCommand,
				FColorFromContext, Context, Context,
				{
					UE_LOG(LogTemp, Log, TEXT("FColor From 5"));
					FTexture2DDynamicResource* TextureResource = static_cast<FTexture2DDynamicResource*>(Context.texture->Resource);

					UE_LOG(LogTemp, Log, TEXT("FColor From 6"));
					FTexture2DRHIParamRef TextureRHI = TextureResource->GetTexture2DRHI();

					UE_LOG(LogTemp, Log, TEXT("FColor From 7"));
					uint32 textureWidth = TextureRHI->GetSizeX();

					UE_LOG(LogTemp, Log, TEXT("FColor From 8"));
					uint32 textureHeight = TextureRHI->GetSizeY();

					UE_LOG(LogTemp, Log, TEXT("FColor From 9"));
					uint32 DestStride = 0;

					UE_LOG(LogTemp, Log, TEXT("FColor From 10"));
					if (textureWidth <= 0 || textureHeight <= 0)
					{
						UE_LOG(LogTemp, Log, TEXT("IOS UIImageFrom Texture Error: TextureWidth and / or TextureHeight is 0"));
						return;
					}

					UE_LOG(LogTemp, Log, TEXT("FColor From 11"));
					//GRHICommandList.GetImmediateCommandList().BlockUntilGPUIdle();

					UE_LOG(LogTemp, Log, TEXT("FColor From 11a"));
					uint8* RawColorArray = reinterpret_cast<uint8*>(RHILockTexture2D(TextureRHI, 0, RLM_ReadOnly, DestStride, false, true));
					//FColor* RawColorArray = reinterpret_cast<FColor*>(RHILockTexture2D(TextureRHI, 0, RLM_ReadOnly, DestStride, false, false));

					UE_LOG(LogTemp, Log, TEXT("FColor From 12"));
					Context.OutData->Empty();

					UE_LOG(LogTemp, Log, TEXT("FColor From 12a"));
					uint32 Size = textureWidth * textureHeight;

					UE_LOG(LogTemp, Log, TEXT("FColor From 12b"));
					//Context.OutData->AddUninitialized(Size);

					//FMemory::Memcpy(OutData.GetData(), Temp.GetData(), Size * sizeof(FColor));

					UE_LOG(LogTemp, Log, TEXT("FColor From 13"));
					for (uint32 y = 0; y < textureHeight; y++)
					{
						//uint8* DestPtr = &DestData[(Height - 1 - y) * DestStride];
						//const FColor* SrcPtr = &FColorData[cy * ex];
						for(uint32 x = 0; x < textureWidth; x++)
						{
							uint8* DestPtr = &RawColorArray[(textureHeight - 1 - y) * DestStride + (x * 4)];
							Context.OutData->Add(FColor(DestPtr[0], DestPtr[1], DestPtr[2], DestPtr[3]));
							//UE_LOG(LogTemp, Log, TEXT("FColor From 13a: x: %d y: %d r: %d g: %d b: %d a: %d"),
							//	x,y,DestPtr[0], DestPtr[1], DestPtr[2], DestPtr[3]);
						}
					}
					//FMemory::Memcpy(Context.OutData->GetData(), RawColorArray, Size * sizeof(FColor));

					UE_LOG(LogTemp, Log, TEXT("FColor From 14"));
					RHIUnlockTexture2D(TextureRHI, 0, false, true);

					UE_LOG(LogTemp, Log, TEXT("FColor From 15"));
				});
			FlushRenderingCommands();
			UE_LOG(LogTemp, Log, TEXT("FColor From 16"));
			/*

			ENQUEUE_UNIQUE_RENDER_COMMAND_ONEPARAMETER(
				ColorFromCommand,
				FColorFromContext, Context, ColorFromContext,
				{
					FTexture2DDynamicResource* TextureResource = static_cast<FTexture2DDynamicResource*>(Context.texture->Resource);
					FTexture2DRHIParamRef TextureRHI = TextureResource->GetTexture2DRHI();
					uint32 textureWidth = TextureRHI->GetSizeX();
					uint32 textureHeight = TextureRHI->GetSizeY();
					uint32 DestStride = 0;
					if (textureWidth <= 0 || textureHeight <= 0)
					{
						UE_LOG(LogTemp, Log, TEXT("IOS UIImageFrom Texture Error: TextureWidth and / or TextureHeight is 0"));
						return;
					}
					FColor* RawColorArray = reinterpret_cast<FColor*>(RHILockTexture2D(TextureRHI, 0, RLM_ReadOnly, DestStride, false, true));
					if (RawColorArray)
					{
						uint32 Size = textureWidth * textureHeight;
						Context.OutData->Empty();
						Context.OutData->AddUninitialized(Size);
						FMemory::Memcpy(Context.OutData->GetData(), RawColorArray, Size * sizeof(FColor));
					}
					RHIUnlockTexture2D(TextureRHI, 0, false, true);
			});
			FlushRenderingCommands();
			*/
			return true;
		}
		return false;
	}

	bool FColorFrom(UTexture *texture, TArray<FColor> &Results, uint32 &outWidth, uint32 &outHeight)
	{
		if (texture)
		{
			if (texture->IsA(UTexture2D::StaticClass()))
			{
				return FColorFrom((UTexture2D*)texture, Results, outWidth, outHeight);
			}
			else if (texture->IsA(UTextureRenderTarget2D::StaticClass()))
			{
				return FColorFrom((UTextureRenderTarget2D*)texture, Results, outWidth, outHeight);
			}
			else if (texture->IsA(UTexture2DDynamic::StaticClass()))
			{
				return FColorFrom((UTexture2DDynamic*)texture, Results, outWidth, outHeight);
			}
			UE_LOG(LogTemp, Log, TEXT("FColorFrom: Unknown texture type specified"));
		}
		return false;
	}

	bool ByteArrayFrom(UTexture *texture, TArray<uint8> &Results, uint32 &outWidth, uint32 &outHeight)
	{
		if (texture)
		{
			TArray<FColor> *ptr = (TArray<FColor>*)&Results;
			bool bResults = FColorFrom(texture, *ptr, outWidth, outHeight);
			return bResults;
		}
		return false;
	}
}