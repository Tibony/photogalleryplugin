// Copyright 2018 Impreszions Solutions All Rights Reserved.
// Author : MunHon Chan

#include "RaveMobilePhotoGallery.h"

#define LOCTEXT_NAMESPACE "FRaveMobilePhotoGalleryModule"

#ifdef __ANDROID__
// Perform startup operations for Android
extern int SetupJNI_RaveMobilePhotoGallery();
#endif

#ifdef __IOS__
// Perform startup operations for IOS
extern void SetupIOS_RaveMobilePhotoGallery();
#endif

void FRaveMobilePhotoGalleryModule::StartupModule()
{
#ifdef __IOS__
	SetupIOS_RaveMobilePhotoGallery();
#endif

#ifdef __ANDROID__
	SetupJNI_RaveMobilePhotoGallery();
#endif
}

void FRaveMobilePhotoGalleryModule::ShutdownModule()
{
}

#undef LOCTEXT_NAMESPACE
	
IMPLEMENT_MODULE(FRaveMobilePhotoGalleryModule, RaveMobilePhotoGallery)