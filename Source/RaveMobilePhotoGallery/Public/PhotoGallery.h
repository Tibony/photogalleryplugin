// Copyright 2018 Impreszions Solutions All Rights Reserved.
// Author : MunHon Chan

#pragma once

#include "RaveMobilePhotoGalleryEnum.h"
#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "MediaTexture.h"
#include "PhotoGallery.generated.h"


// List of possible errors for photo gallery operations by the Rave Media Gallery plugin.
UENUM(BlueprintType)
enum EPhotoGalleryErrorMessages
{
	EPHOTOGALLERYERRORS_NO_ERROR UMETA(DisplayName = "No errors"),
	EPHOTOGALLERYERRORS_HAS_PENDING_REQUEST UMETA(DisplayName = "Has pending photo gallery request"),
	EPHOTOGALLERYERRORS_NO_MEDIA_TYPE_REQUESTED UMETA(DisplayName = "No media type requested"),
	EPHOTOGALLERYERRORS_INVALID_MEDIA UMETA(DisplayName = "Invalid media selected"),
	EPHOTOGALLERYERRORS_UNSUPPORTED_PLATFORM UMETA(DisplayName = "Unsupported platform"),
	EPHOTOGALLERYERRORS_USER_CANCELLED UMETA(DisplayName = "User cancelled"),
	EPHOTOGALLERYERRORS_NO_MEDIA_PLAYER_SPECIFIED UMETA(DisplayName = "No media player specified"),
	EPHOTOGALLERYERRORS_NO_PERMISSION UMETA(DisplayName = "No permission granted")
};

// List of possible media types returned from the photo gallery by the Rave Media Gallery plugin.
UENUM(BlueprintType)
enum EPhotoGalleryMediaType
{
	EPHOTOGALLERYMEDIATYPE_PHOTO UMETA(DisplayName = "Photo"),
	EPHOTOGALLERYMEDIATYPE_VIDEO UMETA(DisplayName = "Video"),
	EPHOTOGALLERYMEDIATYPE_NONE UMETA(DisplayName = "None"),
};

// Information regarding the returned media from the photo gallery
USTRUCT(BlueprintType, Category = "Photo Gallery")
struct RAVEMOBILEPHOTOGALLERY_API FPhotoGalleryMediaInfo
{
	GENERATED_BODY()

	// The type of media returned by the photo gallery
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Photo Gallery")
		TEnumAsByte<EPhotoGalleryMediaType> MediaType = EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_NONE;

	// The width of the media returned by the photo gallery.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Photo Gallery")
		int Width = 0;

	// The height of the media returned by the photo gallery.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Photo Gallery")
		int Height = 0;

	// The counter-clockwise rotation of the media returned by the photo gallery.
	// For photos, this rotation is already applied to the texture and is used for reference purposes only.
	// For videos, apply this rotation to the material to correct the video playback orientation.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Photo Gallery")
		int Rotation = 0;
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FPhotoGalleryTaskDelegate, UTexture*, Texture, EPhotoGalleryMediaType, MediaType,
	FPhotoGalleryMediaInfo, MediaInfo, EPhotoGalleryErrorMessages, ErrorMessage);

UCLASS(Category = "Media Gallery", meta = (DisplayName = "Photo Gallery"))
class RAVEMOBILEPHOTOGALLERY_API UAsyncPhotoGalleryTask : public UBlueprintAsyncActionBase
{
	GENERATED_UCLASS_BODY()

public:
	// Opens the photo gallery browser window and returns the selected media as a texture.
	//
	// [OUT] Texture:
	// If Media Type is Photo, this will be a Texture2D Dynamic object.
	// If Media Type is Video, this will be a MediaTexture object.
	//
	// [OUT] Media Type:
	// The type of media selected by the user.
	//
	// [OUT] Error Message:
	// Error description on failure events.
	//
	// Supported platforms: IOS, Android
	//
	// @param bShowPhotos Allow photos to be listed in the photo gallery window.
	// @param bShowVideos Allow videos to be listed in the photo gallery window.
	// @param MediaPlayer Media player asset to be used for playing video content selected.
	// @param bAutoplay Should the media player auto play on return
	UFUNCTION(BlueprintCallable, Category = "Photo Gallery", meta = (WorldContext = "WorldContextObject", DisplayName = "Get Texture from Photo Gallery", BlueprintInternalUseOnly = "true"))
		static UAsyncPhotoGalleryTask* GetTextureFromPhotoGallery(const UObject* WorldContextObject,
			bool bShowPhotos = true, bool bShowVideos = true,
			UMediaPlayer *MediaPlayer = nullptr, bool bAutoplay = true);

public:
	// Called when a valid media is returned/selected by the user from the photo gallery.
	//
	// [OUT] Texture:
	// If Media Type is Photo, this will be a Texture2D Dynamic object.
	// If Media Type is Video, this will be a MediaTexture object.
	//
	// [OUT] Media Type:
	// The type of media selected by the user.
	//
	// [OUT] Error Message:
	// Error description on failure events.
	UPROPERTY(BlueprintAssignable)
		FPhotoGalleryTaskDelegate OnSuccess;

	// Called when NO valid media is returned/selected by the user from the photo gallery.
	//
	// [OUT] Texture:
	// If Media Type is Photo, this will be a Texture2D Dynamic object.
	// If Media Type is Video, this will be a MediaTexture object.
	//
	// [OUT] Media Type:
	// The type of media selected by the user.
	//
	// [OUT] Error Message:
	// Error description on failure events.
	UPROPERTY(BlueprintAssignable)
		FPhotoGalleryTaskDelegate OnFailure;

	// UObject overrides
	virtual void Activate() override;
	virtual void BeginDestroy() override;
	// END: UObject overrides

	// Convenience method to broadcast a successful event
	// @param texture The texture to be returned
	// @param MediaType the type of media selected/returned by the photo gallery.
	// @param ErrorMessage Error message to be returned. Usually NO_ERRORS for IsSuccess
	void IsSuccess(UTexture *texture, EPhotoGalleryMediaType MediaType, FPhotoGalleryMediaInfo MediaInfo, EPhotoGalleryErrorMessages ErrorMessage);

	// Convenience method to broadcast a failed event
	// @param texture The texture to be returned. Should be NULL for IsFailure
	// @param MediaType the type of media selected/returned by the photo gallery. Should be NONE for IsFailure
	// @param ErrorMessage Error message to be returned.
	void IsFailure(UTexture *texture, EPhotoGalleryMediaType MediaType, FPhotoGalleryMediaInfo MediaInfo, EPhotoGalleryErrorMessages ErrorMessage);

	// Interal function
	void DoOnAndroidPermissionGranted(const TArray<FString>& Permissions, const TArray<bool>& GrantResults);

public:
	// Internal function.
	// Opens the photo gallery browser window and returns the selected media as a texture.
	void DoGetTextureFromPhotoGallery(bool bShowPhotos, bool bShowVideos, UMediaPlayer *MediaPlayer, bool bAutoplay);

	// Reference to the media player specified to be used to play videos returned by the photo gallery
	UMediaPlayer *useMediaPlayer = nullptr;

	// Determines if media player should autoplay
	bool bAutoplayMedia = false;

protected:
	// Tracks when the component has activated
	bool bIsActivated = false;

	// Tracks when a success/failed call has been made
	bool bHasResults = false;

	// Tracks when a success/failed call has been made
	bool bIsSuccess = false;

	// Tracks the last texture returned
	UTexture *lastTexture = NULL;

	// Tracks the last media info
	FPhotoGalleryMediaInfo lastMediaInfo;

	// Tracks the last media type to be returned
	EPhotoGalleryMediaType lastMediaType = EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_NONE;

	// Tracks the last error message to be returned
	EPhotoGalleryErrorMessages lastErrorMsg = EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_NO_ERROR;
};
