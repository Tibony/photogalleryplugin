// Copyright 2018 Impreszions Solutions All Rights Reserved.
// Author : MunHon Chan

// Reference From: https://raw.githubusercontent.com/mstorsjo/android-decodeencodetest/master/src/com/example/decodeencodetest/ExtractDecodeEditEncodeMuxTest.java

package com.impreszions.ravemediatools;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.app.Activity;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;

import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import android.R;
import android.widget.TextView;
import android.graphics.Bitmap;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.Manifest;
import android.content.pm.PackageManager;
import android.content.pm.ActivityInfo;

import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.view.ViewGroup.MarginLayoutParams;
import android.os.Build;
import android.view.Gravity;
import android.widget.ImageView;
import android.util.DisplayMetrics;

public class ScreenRecorder// extends Thread
{
	public static native boolean nativeStartScreenRecordingResponse(int success);
	public static native boolean nativeStopScreenRecordingResponse(int success, int width, int height, int rotation); 
	public static native boolean nativeSaveScreenRecordingResponse(int success, int width, int height, int rotation);    
	public Activity gameActivity;
	public LinearLayout gameActivityLayout;
	public Bitmap OverlayBitmap;
	public static String CAPTURE_FILENAME = "/sdcard/capture.mp4";
	public boolean SaveToGalleryOnComplete = false;

	public void InitRecording(Activity mainActivity, LinearLayout activityLayout, Bitmap mOverlayBitmap)
	{
		gameActivity = mainActivity;
		gameActivityLayout = activityLayout;
		OverlayBitmap = mOverlayBitmap;
	}

    public void StartRecording(int width, int height, String filename, int rotation, int offsetWidth, int offsetHeight, boolean DoSaveToGalleryOnComplete) //, SurfaceView MySurfaceView)
	{
	}

	public void StopRecording()
	{
	}

	public boolean IsRecording()
	{
		return false;
	}

	public void renderFrame(int mOffscreenTexture, int mSurfaceWidth, int mSurfaceHeight, android.opengl.EGLDisplay display, android.opengl.EGLSurface surface)
	{
	}

	public void gameActivityOnActivityResultAdditions(int requestCode, int resultCode, Intent data)
	{
	}

	public void PauseRecording()
	{
	}

	public void ResumeRecording()
	{
	}

	public void OnAddedToGallery(String path)
	{
		//nativeStopScreenRecordingResponse(1);
	}

	public void AddToGallery()
	{
		MediaScannerConnection.scanFile(gameActivity.getApplicationContext(), new String[] { CAPTURE_FILENAME }, null, new MediaScannerConnection.OnScanCompletedListener()
        {
			public void onScanCompleted(String path, Uri uri)
			{
				OnAddedToGallery(path); //nativeStopScreenRecordingResponse(1);
			}
        });
	}

// SCREEN RECORDER OVERLAY
private PopupWindow screenRecorderWindow;
private LinearLayout screenRecorderLayout;
private int ScreenRecorderGravity = Gravity.TOP;
  
protected void showScreenRecorderOverlay1(Bitmap OverlayBitmap)
{
  if ( screenRecorderWindow == null )
  {
    ImageView iv = new ImageView(gameActivity);
    //ScreenRecorderOverlay iv = new ScreenRecorderOverlay(this, OverlayBitmap);
    iv.setLayoutParams(new LayoutParams(
            LayoutParams.FILL_PARENT,
            LayoutParams.FILL_PARENT));
    iv.setImageBitmap(OverlayBitmap);
    iv.setBackground(null);
    //iv.setBackgroundColor(0x00000000); //.setBackgroundColor(0,0,0,0);
         
    final DisplayMetrics dm = gameActivity.getResources().getDisplayMetrics();
		final float scale = dm.density;
          
    screenRecorderWindow = new PopupWindow(gameActivity);
    //screenRecorderWindow.addFlags(WindowManager.LayoutParams.FLAG_SECURE);

	screenRecorderWindow.setWidth(LayoutParams.FILL_PARENT);
	screenRecorderWindow.setHeight(LayoutParams.FILL_PARENT);
	screenRecorderWindow.setClippingEnabled(false);
    screenRecorderWindow.setTouchable(false);
    screenRecorderWindow.setAttachedInDecor(true);
    screenRecorderWindow.setElevation(0);
    screenRecorderWindow.setBackgroundDrawable(null);
    //screenRecorderWindow.setBackgroundColor(0,0,0,0);
    //screenRecorderWindow.setBackgroundColor(0x00000000);
    //screenRecorderWindow.setInputMethodMode(INPUT_METHOD_NOT_NEEDED);

	screenRecorderLayout = new LinearLayout(gameActivity);

	final int padding = (int)(0*scale);
	screenRecorderLayout.setPadding(padding,padding,padding,padding);

	MarginLayoutParams params = new MarginLayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT);
//screenRecorderLayout.flags = WindowManager.LayoutParams.FLAG_SECURE;
	params.setMargins(0,0,0,0);

	screenRecorderWindow.setContentView(screenRecorderLayout);
	screenRecorderLayout.setOrientation(LinearLayout.VERTICAL);
	screenRecorderLayout.addView(iv, params);
    //screenRecorderLayout.setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
  }

  screenRecorderWindow.showAtLocation(gameActivityLayout, ScreenRecorderGravity, 0, 0);

	// don't call update on 7.0 to work around this issue: https://code.google.com/p/android/issues/detail?id=221001
	if (android.os.Build.VERSION.SDK_INT != 24) {
		screenRecorderWindow.update();
	}
}
           
public void showScreenRecorderOverlay() //final Bitmap OverlayBitmap)
{
	gameActivity.runOnUiThread(new Runnable() {
    public void run() {
		  showScreenRecorderOverlay1(OverlayBitmap);
	    }
    });
}
      
public void hideScreenRecorderOverlay()
{
  if ( screenRecorderWindow != null ) //&& screenRecorderWindow.isShowing() )
  {
      gameActivity.runOnUiThread(new Runnable() {
    	public void run() {
		    screenRecorderWindow.dismiss();
			screenRecorderWindow = null;
	      }
      });
  }
}
}