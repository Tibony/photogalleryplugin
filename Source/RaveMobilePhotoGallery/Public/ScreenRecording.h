// Copyright 2018 Impreszions Solutions All Rights Reserved.
// Author : MunHon Chan

#pragma once

#include "RaveMobilePhotoGalleryEnum.h"
#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "MediaTexture.h"
#include "MediaPlayer.h"
#include "UMG/Public/Blueprint/UserWidget.h"

#if PLATFORM_ANDROID
#include "Android/AndroidApplication.h"
#include "SceneViewExtension.h"
#endif

#include "ScreenRecording.generated.h"

// List of possible errors for screen recording operations by the Rave Media Gallery plugin.
UENUM(Blueprintable)
enum EScreenRecordingErrors
{
	EScreenRecordingSuccess UMETA(DisplayName = "Success"),
	EScreenRecordingUnknownError UMETA(DisplayName = "Unknown Error"),
	EScreenRecordingIsRecording UMETA(DisplayName = "Is already recording"),
	EScreenRecordingNotRecording UMETA(DisplayName = "Was not recording"),
	EScreenRecordingFailedStartRecording UMETA(DisplayName = "Failed to start recording"),
	EScreenRecordingFailedStopRecording UMETA(DisplayName = "Failed to stop recording"),
	EScreenRecordingIncompatibleMovieFile UMETA(DisplayName = "Movie file is incompatible with gallery"),
	EScreenRecordingStartCallbackTimeout UMETA(DisplayName = "Start recording callback has timed out"),
	EScreenRecordingStopCallbackTimeout UMETA(DisplayName = "Stop recording callback has timed out"),
	EScreenRecordingPendingPrevious UMETA(DisplayName = "Pending request is still being processed"),
	EScreenRecordingNoPermission UMETA(DisplayName = "Permission is not granted"),
	EScreenRecordingUnsupportedOS UMETA(DisplayName = "OS version not supported"),
	EScreenRecordingNotAvailable UMETA(DisplayName = "Screen recording functionality not available on this device"),
	EScreenRecordingFileWriteError UMETA(DisplayName = "Unable to create media file")
};

enum EScreenRecordingAndroidMethod
{
	EScreenRecordingAndroidMethod_MediaProjection UMETA(DisplayName = "Media Projection"),
	EScreenRecordingAndroidMethod_FrameBufferObject UMETA(DisplayName = "Frame Buffer (FBO)")
};

// Information regarding the returned media from the photo gallery
USTRUCT(BlueprintType, Category = "Photo Gallery")
struct RAVEMOBILEPHOTOGALLERY_API FScreenRecordingMediaInfo
{
	GENERATED_BODY()

	// The width of the media returned by the photo gallery.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Photo Gallery")
		int Width = 0;

	// The height of the media returned by the photo gallery.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Photo Gallery")
		int Height = 0;

	// The counter-clockwise rotation of the media returned by the photo gallery.
	// For photos, this rotation is already applied to the texture and is used for reference purposes only.
	// For videos, apply this rotation to the material to correct the video playback orientation.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Photo Gallery")
		int Rotation = 0;

	// The full file path to the screenshot
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Photo Gallery")
		FString FilePath = "";
};


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FStartScreenRecordingTaskDelegate, EScreenRecordingErrors, ErrorMessage);


UCLASS(Category = "Photo Gallery", meta = (DisplayName = "Photo Gallery"))
class RAVEMOBILEPHOTOGALLERY_API UAsyncStartRecordingTask : public UBlueprintAsyncActionBase
{
	GENERATED_UCLASS_BODY()

public:

	// Starts recording of the screen, using Replaykit on IOS / MediaProjection on Android.
	//
	// [OUT] Error Message:
	// Error message if the screen recording failed to start. If Successful, this should return No Errors.
	//
	// Supported platforms: IOS, Android
	//
	// @param bSaveRecording Saves the recording to disk to the saved folder.	Android: This will always be enabled regardless of setting, due to the need of a valid file location to record the file to.
	// @param bSaveToGallery Saves the recording to the device photo gallery.	Android: Enabling this will automatically enable Save To Screenshots due to the requirement of gallery contents to be physical files.
	// @param SaveFolder Subfolder to store the recording into.
	// IOS: {Documents}/{SaveFolder}	Android:{Project}/Saved/{SavedFolder}	Windows: {Project}/Saved/{SaveFolder}
	// @param Rotation Rotation to be applied to the recorded output.
	// @param OffsetWidthHeight Number of pixels to be reduced from the actual screen size. This can be used as a way to modify aspect ratio of the recorded output.
	// @param bAndroidSaveToExternal This saves the actual image file to the external storage device instead of the default saved folder.
	// This is required for the image to remain in the gallery when the application is uninstalled.
	// If enabled, screenshot will be saved to {storagedevice}/{SaveFolder}
	UFUNCTION(BlueprintCallable, Category = "Photo Gallery", meta = (WorldContext = "WorldContextObject", DisplayName = "Start Screen Recording", BlueprintInternalUseOnly = "true"))
		static UAsyncStartRecordingTask* StartRecording(const UObject* WorldContextObject,
			bool bSaveRecording = false,
			bool bSaveToGallery = false,
			FString SaveFolder = "", 
			EScreenshotRotation Rotation = EScreenshotRotation::EScreenshotRotation_ROTATE0,
			bool bAndroidSaveToExternal = true);

	// Returns if the plugin is currently recording the screen.
	UFUNCTION(BlueprintPure, Category = "Photo Gallery", meta=(DisplayName = "Is Screen Recording"))
		static bool IsRecording();

	UFUNCTION(BlueprintCallable, Category = "Photo Gallery", meta = (DisplayName = "Stop"))
		static bool Stop();

	UFUNCTION(BlueprintCallable, Category = "Photo Gallery", meta = (DisplayName = "Resume"))
		static bool Resume();

	UFUNCTION(BlueprintCallable, Category = "Photo Gallery", meta = (DisplayName = "Check Screen Recording Permissions"))
		static bool CheckScreenRecordingPermissions();

public:
	// UObject overrides
	virtual void Activate() override;
	// END UObject overrides

	// Convenience method to broadcast a successful event
	void IsSuccess();

	// Convenience method to broadcast a successful event
	// @param errorCode Error mess	age to be returned.
	void IsFailure(EScreenRecordingErrors errorCode = EScreenRecordingErrors::EScreenRecordingUnknownError);

#if PLATFORM_IOS
	// Checks if permission is granted for screen recording
	void IsAccessGranted(bool bGranted);
#endif

	// Called when screen recording has started successfully.
	//
	// [OUT] Error Message:
	// Error message if the screen recording failed to start. If Successful, this should return No Errors.
	UPROPERTY(BlueprintAssignable)
		FStartScreenRecordingTaskDelegate OnSuccess;

	// Called when screen recording cannot be started.
	//
	// [OUT] Error Message:
	// Error message if the screen recording failed to start. If Successful, this should return No Errors.
	UPROPERTY(BlueprintAssignable)
		FStartScreenRecordingTaskDelegate OnFailure;

protected:
	// Tracks when the component has activated
	bool bIsActivated = false;

	// Tracks when a success/failed call has been made
	bool bHasResults = false;

	// Tracks when a success/failed call has been made
	bool bIsSuccess = false;

	// Tracks when a delegate has been broadcast
	bool bHasResponse = false;

	// Tracks the last error message to be broadcast
	EScreenRecordingErrors lastError;

public:
	// Interal function
	void DoStartRecording(const UObject* WorldContextObject,
		bool bSaveToScreenshots, bool bSaveToGallery,
		EScreenshotRotation Rotation, FVector2D OffsetWidthHeight,
		FString SaveFolder, bool bAndroidSaveToExternal, EScreenRecordingAndroidMethod AndroidRecordingMethod);

	// Interal function
	void DoStartRecordingWithOverlayTexture(const UObject* WorldContextObject, 
		bool bSaveToScreenshots, bool bSaveToGallery,
		UTexture* OverlayTexture, EScreenshotRotation Rotation, FVector2D OffsetWidthHeight,
		FString SaveFolder, bool bAndroidSaveToExternal, EScreenRecordingAndroidMethod AndroidRecordingMethod);

	// Interal function
	void DoStartRecordingWithOverlayWidget(const UObject* WorldContextObject, 
		bool bSaveToScreenshots, bool bSaveToGallery,
		UUserWidget* OverlayWidget, EScreenshotRotation Rotation, FVector2D OffsetWidthHeight,
		FString SaveFolder, bool bAndroidSaveToExternal, EScreenRecordingAndroidMethod AndroidRecordingMethod);

	// Interal function
	void DoOnAndroidPermissionGranted(const TArray<FString>& Permissions, const TArray<bool>& GrantResults);
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FStopScreenRecordingTaskDelegate, UMediaTexture*, Texture, 
	FScreenRecordingMediaInfo, MediaInfo,
	EScreenRecordingErrors, ErrorMessage);


UCLASS(Category = "Photo Gallery", meta = (DisplayName = "Photo Gallery"))
class RAVEMOBILEPHOTOGALLERY_API UAsyncStopRecordingTask : public UBlueprintAsyncActionBase
{
	GENERATED_UCLASS_BODY()

public:

	// Stops the current screen recording, if any.
	//
	// [OUT] Texture:
	// MediaTexture to be used to playback the recorded video. Only returned if Get Texture is set to TRUE.
	//
	// [OUT] ErrorMessage:

	// Supported platforms: IOS, Android
	//
	// @param bGetTexture If true, a MediaTexture will be returned.
	// @param mediaPlayer (Optional) Media Player asset to be used to play the recorded video.
	UFUNCTION(BlueprintCallable, Category = "Photo Gallery", meta = (WorldContext = "WorldContextObject", DisplayName = "Stop Screen Recording", BlueprintInternalUseOnly = "true"))
		static UAsyncStopRecordingTask* StopRecording(const UObject* WorldContextObject,
			bool bGetTexture = false,
			UMediaPlayer *mediaPlayer = nullptr,
			bool bAutoPlay = true);

public:
	virtual void Activate() override;
	virtual void BeginDestroy() override;

	bool bIsActivated = false;
	bool bHasResults = false;
	bool bIsSuccess = false;
	bool bHasResponse = false;

	void IsSuccess(UMediaTexture *Texture, FString filePath = "");
	void IsSuccess(FString mediaPath, FString filePath = "");

	void IsFailure(EScreenRecordingErrors errorCode = EScreenRecordingErrors::EScreenRecordingUnknownError);
	void IsSaving();
	UMediaPlayer *useMediaPlayer;

	UPROPERTY(BlueprintAssignable)
		FStopScreenRecordingTaskDelegate OnSuccess;

	UPROPERTY(BlueprintAssignable)
		FStopScreenRecordingTaskDelegate OnFailure;

	UPROPERTY(BlueprintAssignable)
		FStopScreenRecordingTaskDelegate OnSaving;

public:
	void DoStopRecording(const UObject* WorldContextObject,
		bool bGetTexture = false,
		UMediaPlayer *mediaPlayer = nullptr,
		bool bAutoPlay = true);

protected:
	UMediaTexture *lastTexture = nullptr;
	FString lastFilePath = "";
	EScreenRecordingErrors lastError;
	bool bShouldAutoplay = true;
};