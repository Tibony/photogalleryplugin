// Copyright 2018 Impreszions Solutions All Rights Reserved.
// Author : MunHon Chan

using UnrealBuildTool;
using System.IO;
using System;

public class RaveMobilePhotoGallery : ModuleRules
{
	public RaveMobilePhotoGallery(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = ModuleRules.PCHUsageMode.UseExplicitOrSharedPCHs;
		
		PublicDependencyModuleNames.AddRange(
			new string[]
			{
				"Core",
				"Slate",
                "UMG",
				"CoreUObject",
				"Engine",
				"SlateCore",
                "MediaAssets",
                "RHI",
                "RenderCore",
                "SlateRHIRenderer"
				// ... add other public dependencies that you statically link with here ...
			}
			);
			
		
		PrivateDependencyModuleNames.AddRange(
			new string[]
			{
				// ... add private dependencies that you statically link with here ...	
			}
            );
		
		
		DynamicallyLoadedModuleNames.AddRange(
			new string[]
			{
				// ... add any modules that your module loads dynamically here ...
			}
			);

        string SDKDIR = Utils.ResolveEnvironmentVariable("%NVPACK_ROOT%");
        SDKDIR = SDKDIR.Replace("\\", "/");
        string pluginPath = Utils.MakePathRelativeTo(ModuleDirectory, Target.RelativeEnginePath);


        if (Target.Platform == UnrealTargetPlatform.IOS)
        {
            PublicDefinitions.Add("IS_IOS=1");
            PublicDefinitions.Add("IS_ANDROID=0");

            // Photo Gallery
            PublicFrameworks.Add("MobileCoreServices");
            PublicFrameworks.Add("AVFoundation");

            // Screen Recording
            PublicFrameworks.Add("ReplayKit");

            PublicIncludePaths.AddRange(
                new string[] {
                    "Runtime/Core/Private/IOS",
                }
            );

            AdditionalPropertiesForReceipt.Add("IOSPlugin", Path.Combine(pluginPath, "RaveMobilePhotoGallery_APL.xml"));
            AdditionalPropertiesForReceipt.Add("IOSPlugin", Path.Combine(pluginPath, "RaveMobilePhotoGalleryScreenshot_APL.xml"));
            AdditionalPropertiesForReceipt.Add("IOSPlugin", Path.Combine(pluginPath, "RaveMobilePhotoGalleryRecording_APL.xml"));
            AdditionalPropertiesForReceipt.Add("IOSPlugin", Path.Combine(pluginPath, "RaveMobilePhotoGallerySocialMedia_APL.xml"));
        }
        else if (Target.Platform == UnrealTargetPlatform.Android)
        {
            PublicDefinitions.Add("IS_IOS=0");
            PublicDefinitions.Add("IS_ANDROID=1");

            PublicIncludePaths.AddRange(
                new string[] {
                    "Runtime/Core/Public/Android",
                    "Runtime/Launch/Private/Android",
                    "Runtime/Launch/Public/Android",
                    "Runtime/OpenGLDrv/Private/Android",
                    "Runtime/OpenGLDrv/Public",
                    "Runtime/Android/AndroidAudio/Public"
                }
            );

            PrivateIncludePaths.AddRange(
                new string[] {
                }
           );

            PublicDependencyModuleNames.AddRange(
               new string[]
               {
                    "AndroidPermission"
               }
           );

            PrivateDependencyModuleNames.AddRange(
                new string[]
                {
                    "OpenGLDrv",
                    "OpenGL",
                    "AndroidAudio",
                    "AudioMixer",
                }
            );

            AdditionalPropertiesForReceipt.Add("AndroidPlugin", Path.Combine(pluginPath, "RaveMobilePhotoGallery_APL.xml"));
            AdditionalPropertiesForReceipt.Add("AndroidPlugin", Path.Combine(pluginPath, "RaveMobilePhotoGalleryScreenshot_APL.xml"));
            AdditionalPropertiesForReceipt.Add("AndroidPlugin", Path.Combine(pluginPath, "RaveMobilePhotoGalleryRecording_APL.xml"));
            AdditionalPropertiesForReceipt.Add("AndroidPlugin", Path.Combine(pluginPath, "RaveMobilePhotoGallerySocialMedia_APL.xml"));

            AddEngineThirdPartyPrivateStaticDependencies(Target,
                "UEOgg",
                "Vorbis",
                "VorbisFile"
            );
        }
        else
        {
            PublicDefinitions.Add("IS_IOS=0");
            PublicDefinitions.Add("IS_ANDROID=0");
        }
    }
}
