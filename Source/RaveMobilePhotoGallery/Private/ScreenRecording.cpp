// Copyright 2018 Impreszions Solutions All Rights Reserved.
// Author : MunHon Chan

#include "ScreenRecording.h"
#include "AudioDevice.h"
#include "Slate/WidgetRenderer.h"
#include "Framework/Application/SlateApplication.h"
#include "Engine/TextureRenderTarget2D.h"
#include "Async/Async.h"

namespace RaveMobilePhotoGallery
{
	// Generate a filename based on the current time
	extern FString GetTimestampedFilename(FString extension, FString prefix, FString suffix);

	// Get an output folder to save most contents to
	extern FString GetOutputFolderPath(FString fileName, bool bAndroidSaveToExternal, FString AndroidSaveFolder);

	// Creates the folder that will contain the file if it does not currently exist
	extern bool EnsureFileFolderExists(FString File);

	// Copys file from source to destination
	extern bool CopyFile(FString Src, FString Dest);

#if PLATFORM_IOS
	// Generate UIImage from a Texture
	extern UIImage *UIImageFrom(UTexture *texture, float opacity, bool premultiplyAlpha);
#endif

#if PLATFORM_ANDROID
	// Generate an AndroidBitmap from a Texture
	extern jobject AndroidBitmapFrom(UTexture *texture, float opacity, bool premultiplyAlpha);
#endif
}
using namespace RaveMobilePhotoGallery;

static int32 renderedFrameCount = 0;
static FDateTime startRecordingTime;
static bool bIsSaveToScreenshots = false;
static bool bIsSaveToGallery = false;
static FString CurrentSaveFolder = "";
static int32 recordingWidth = 0;
static int32 recordingHeight = 0;
static int32 recordingRotation = 0;

#if PLATFORM_WINDOWS || PLATFORM_MAC
static bool SimulatedIsRecording = false;
#endif

#if PLATFORM_ANDROID
#include "AndroidJNI.h"
#include <jni.h>
#include "AndroidEGL.h"
#include "RHIDefinitions.h"
#include "RHIResources.h"

#include <android/bitmap.h>
#include <EGL/egl.h>
#include <EGL/eglext.h>
#include <GLES2/gl2.h>
#include <GLES3/gl3.h>
#include "OpenGLUtil.h"
#include <android/native_window.h> // requires ndk r5 or newer
#include <android/native_window_jni.h> // requires ndk r5 or newer

#include "AndroidPermissionFunctionLibrary.h"
#include "AndroidPermissionCallbackProxy.h"

static jmethodID AndroidThunkJava_StartScreenCapture;
static jmethodID AndroidThunkJava_StopScreenCapture;
static jmethodID AndroidThunkJava_IsScreenCapturing;
static jmethodID AndroidThunkJava_Pause;
static jmethodID AndroidThunkJava_Resume;
static jmethodID AndroidThunkJava_RenderFrame;

ENGINE_API extern bool GetViewportScreenShot(FViewport* Viewport, TArray<FColor>& Bitmap, const FIntRect& ViewRect /*= FIntRect()*/);

DECLARE_MULTICAST_DELEGATE_TwoParams(FOnStartScreenRecordingResponse, int, EScreenRecordingErrors);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnStopScreenRecordingResponse, int, EScreenRecordingErrors);

static FOnStartScreenRecordingResponse AndroidStartScreenRecordingResponseDelegate;
static FOnStopScreenRecordingResponse AndroidStopScreenRecordingResponseDelegate;
static FDelegateHandle AndroidOnScreenshotDelegate;

static UTexture *LastOverlayTexture = NULL;
static uint8* encoderImageBuffer = NULL;
static int encoderImageBufferSize = 0;
static bool hasEncoderImageBuffer = false;

static EGLSurface renderEGLSurface = EGL_NO_SURFACE;
static EGLContext renderEGLContext = EGL_NO_CONTEXT;
static EGLDisplay renderEGLDisplay = EGL_NO_DISPLAY;
static EGLSurface renderUE4Surface = EGL_NO_SURFACE;

static ANativeWindow *window = 0;
static EGLSurface windowEglSurface;
static EGLint mWindowSurfaceWidth = 0;
static EGLint mWindowSurfaceHeight = 0;
static GLuint mOffscreenTexture = 0;
static bool bHasRenderSurface = false;
static bool bIsRenderingSurface = false;
static FString currentFilePath = "";

struct StartRecordingData
{
	FString filePath;
	uint32 outWidth;
	uint32 outHeight;
	uint32 rotationInt;
	bool bUseFBO;
	bool bLockOrientation;
	FVector2D OffsetWidthHeight;
};
static StartRecordingData lastRecordingData;

bool(*eglPresentationTimeANDROID_)(EGLDisplay dpy, EGLSurface sur,
	khronos_stime_nanoseconds_t time);

// Perform JNI binds for Screen Recording
int SetupJNI_RaveMobilePhotoGallery_ScreenRecording()
{
	JNIEnv* env = FAndroidApplication::GetJavaEnv();
	if (!env) return JNI_ERR;

	JNIEnv* ENV = env;

	UE_LOG(LogTemp, Log, TEXT("CHECKING: RaveMobilePhotoGalleryVideo_StartScreenCapture"));
	AndroidThunkJava_StartScreenCapture = FJavaWrapper::FindMethod(ENV, FJavaWrapper::GameActivityClassID, "RaveMobilePhotoGalleryVideo_StartScreenCapture", "(Ljava/lang/String;Landroid/graphics/Bitmap;IIIZZIIZ)Z", false);
	if (!AndroidThunkJava_StartScreenCapture)
	{
		UE_LOG(LogTemp, Log, TEXT("ERROR: RaveMobilePhotoGalleryVideo_StartScreenCapture Not Found"));
	}

	UE_LOG(LogTemp, Log, TEXT("CHECKING: RaveMobilePhotoGalleryVideo_StopScreenCapture"));
	AndroidThunkJava_StopScreenCapture = FJavaWrapper::FindMethod(ENV, FJavaWrapper::GameActivityClassID, "RaveMobilePhotoGalleryVideo_StopScreenCapture", "()Z", false);
	if (!AndroidThunkJava_StopScreenCapture)
	{
		UE_LOG(LogTemp, Log, TEXT("ERROR: RaveMobilePhotoGalleryVideo_StopScreenCapture Not Found"));
	}

	UE_LOG(LogTemp, Log, TEXT("CHECKING: RaveMobilePhotoGalleryVideo_IsScreenCapturing"));
	AndroidThunkJava_IsScreenCapturing = FJavaWrapper::FindMethod(ENV, FJavaWrapper::GameActivityClassID, "RaveMobilePhotoGalleryVideo_IsScreenCapturing", "()Z", false);
	if (!AndroidThunkJava_IsScreenCapturing)
	{
		UE_LOG(LogTemp, Log, TEXT("ERROR: RaveMobilePhotoGalleryVideo_IsScreenCapturing Not Found"));
	}

	UE_LOG(LogTemp, Log, TEXT("CHECKING: RaveMobilePhotoGalleryVideo_Pause"));
	AndroidThunkJava_Pause = FJavaWrapper::FindMethod(ENV, FJavaWrapper::GameActivityClassID, "RaveMobilePhotoGalleryVideo_Pause", "()Z", false);
	if (!AndroidThunkJava_Pause)
	{
		UE_LOG(LogTemp, Log, TEXT("ERROR: RaveMobilePhotoGalleryVideo_Pause Not Found"));
	}


	UE_LOG(LogTemp, Log, TEXT("CHECKING: RaveMobilePhotoGalleryVideo_Resume"));
	AndroidThunkJava_Resume = FJavaWrapper::FindMethod(ENV, FJavaWrapper::GameActivityClassID, "RaveMobilePhotoGalleryVideo_Resume", "()Z", false);
	if (!AndroidThunkJava_Resume)
	{
		UE_LOG(LogTemp, Log, TEXT("ERROR: RaveMobilePhotoGalleryVideo_Resume Not Found"));
	}
	


	UE_LOG(LogTemp, Log, TEXT("CHECKING: RaveMobilePhotoGalleryVideo_RenderFrame"));
	AndroidThunkJava_RenderFrame = FJavaWrapper::FindMethod(ENV, FJavaWrapper::GameActivityClassID, "RaveMobilePhotoGalleryVideo_RenderFrame", "(IIILandroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)V", false);
	if (!AndroidThunkJava_RenderFrame)
	{
		UE_LOG(LogTemp, Log, TEXT("ERROR: RaveMobilePhotoGalleryVideo_RenderFrame Not Found"));
	}

	eglPresentationTimeANDROID_ = reinterpret_cast<
		bool(*)(EGLDisplay, EGLSurface, khronos_stime_nanoseconds_t)>(
			eglGetProcAddress("eglPresentationTimeANDROID"));
	
	return JNI_OK;
}

// Start screen capture on JNI
bool AndroidThunkCpp_StartScreenCapture(FString imagePath, jobject overlayBitmap, int width, int height, int rotation, bool bUseFBO, bool bLockOrientation,
	int offsetWidth, int offsetHeight, bool bAddToGallery)
{
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		currentFilePath = imagePath;
		jstring Argument = Env->NewStringUTF(TCHAR_TO_UTF8(*imagePath));

		recordingWidth = width;
		recordingHeight = height;

		return FJavaWrapper::CallBooleanMethod(Env, FJavaWrapper::GameActivityThis, AndroidThunkJava_StartScreenCapture, Argument, overlayBitmap, (jint)width, (jint)height,
			(jint)rotation, bUseFBO, bLockOrientation,
			(jint)offsetWidth, (jint)offsetHeight, bAddToGallery);
	}
	return false;
}

// Stops screen capture on JNI
bool AndroidThunkCpp_StopScreenCapture()
{
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		return FJavaWrapper::CallBooleanMethod(Env, FJavaWrapper::GameActivityThis, AndroidThunkJava_StopScreenCapture);
	}
	return false;
}

// Check if the application is currently recording on JNI
bool AndroidThunkCpp_IsScreenCapturing()
{
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		return FJavaWrapper::CallBooleanMethod(Env, FJavaWrapper::GameActivityThis, AndroidThunkJava_IsScreenCapturing);
	}
	return false;
}

bool AndroidThunkCpp_Pause()
{
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		return FJavaWrapper::CallBooleanMethod(Env, FJavaWrapper::GameActivityThis, AndroidThunkJava_Pause);
	}
	return false;
}

bool AndroidThunkCpp_Resume()
{
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		return FJavaWrapper::CallBooleanMethod(Env, FJavaWrapper::GameActivityThis, AndroidThunkJava_Resume);
	}
	return false;
}


// Render a bitmap on JNI. Currently unused
void AndroidThunkCpp_RenderFrame(int mOffscreenTexture1, int mWidth, int mHeight)
{
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		FJavaWrapper::CallVoidMethod(Env, FJavaWrapper::GameActivityThis, AndroidThunkJava_RenderFrame, (jint)mOffscreenTexture1, (jint)mWidth, (jint)mHeight,
			nullptr, nullptr); // , (jint)height);
	}
	//return nullptr;
}

// Response from the JNI for Start Screen Recording
extern "C" bool Java_com_impreszions_ravemediatools_ScreenRecorder_nativeStartScreenRecordingResponse(JNIEnv* LocalJNIEnv, jobject LocalThiz, jint success)
{
	bIsRenderingSurface = false;
	AsyncTask(ENamedThreads::GameThread, [success]() {
		if ((int)success == 2)
		{
			AndroidThunkCpp_RenderFrame(1, mWindowSurfaceWidth, mWindowSurfaceHeight);
		}
		else
		{
			AndroidStartScreenRecordingResponseDelegate.Broadcast((int)success, EScreenRecordingSuccess);
		}
	});
	return JNI_TRUE;
}

// Response from the JNI for Stop Screen Recording
extern "C" bool Java_com_impreszions_ravemediatools_ScreenRecorder_nativeStopScreenRecordingResponse(JNIEnv* LocalJNIEnv, jobject LocalThiz, jint success, jint width, jint height, jint rotation)
{
	bHasRenderSurface = false;
	int bSuccess = (int)success;
	recordingWidth = (int)width;
	recordingHeight = (int)height;
	recordingRotation = (int)rotation;

	AsyncTask(ENamedThreads::GameThread, [bSuccess]() {
		AndroidStopScreenRecordingResponseDelegate.Broadcast(bSuccess, EScreenRecordingSuccess);

		if (renderEGLSurface != EGL_NO_SURFACE )
		{
			eglDestroySurface(renderEGLDisplay, renderEGLSurface);
		}
		renderEGLSurface = EGL_NO_SURFACE;
		renderUE4Surface = EGL_NO_SURFACE;
		renderEGLDisplay = EGL_NO_DISPLAY;
		renderEGLContext = EGL_NO_CONTEXT;
	});

	return JNI_TRUE;
}

// Response from the JNI for Save Screen Recording
extern "C" bool Java_com_impreszions_ravemediatools_ScreenRecorder_nativeSaveScreenRecordingResponse(JNIEnv* LocalJNIEnv, jobject LocalThiz, jint success, jint width, jint height, jint rotation)
{
	bIsRenderingSurface = false;
	recordingWidth = (int)width;
	recordingHeight = (int)height;
	recordingRotation = (int)rotation;

	AsyncTask(ENamedThreads::GameThread, [success]() {
		AndroidStopScreenRecordingResponseDelegate.Broadcast(2, EScreenRecordingSuccess);
	});
	return JNI_TRUE;
}

// Request from the JNI to obtain for the EGL Display (used by FBO only)
extern "C" jint Java_com_impreszions_ravemediatools_ScreenRecorderFBO_nativeGetRenderEGLDisplay(JNIEnv* LocalJNIEnv, jobject LocalThiz)
{
	return (jint)renderEGLDisplay;
}

// Request from the JNI to obtain for the EGL Surface (used by FBO only)
extern "C" jint Java_com_impreszions_ravemediatools_ScreenRecorderFBO_nativeGetRenderEGLSurface(JNIEnv* LocalJNIEnv, jobject LocalThiz)
{
	return (jint)renderEGLSurface;
}

// Display log information about EGL (used by FBO only)
void LogConfig2(EGLDisplay display, EGLConfig EGLConfigInfo)
{
	EGLint ResultValue = 0;
	eglGetConfigAttrib(display, EGLConfigInfo, EGL_RED_SIZE, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_RED_SIZE"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_GREEN_SIZE, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_GREEN_SIZE"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_BLUE_SIZE, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_BLUE_SIZE"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_ALPHA_SIZE, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_ALPHA_SIZE"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_ALPHA_MASK_SIZE, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_ALPHA_MASK_SIZE"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_DEPTH_SIZE, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_DEPTH_SIZE"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_STENCIL_SIZE, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_STENCIL_SIZE"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_SAMPLE_BUFFERS, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_SAMPLE_BUFFERS"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_BIND_TO_TEXTURE_RGB, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_BIND_TO_TEXTURE_RGB"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_SAMPLES, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_SAMPLES"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_COLOR_BUFFER_TYPE, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_COLOR_BUFFER_TYPE"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_CONFIG_CAVEAT, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_CONFIG_CAVEAT"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_CONFIG_ID, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_CONFIG_ID"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_CONFORMANT, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_CONFORMANT"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_LEVEL, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_LEVEL"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_LUMINANCE_SIZE, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_LUMINANCE_SIZE"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_MAX_PBUFFER_WIDTH, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_MAX_PBUFFER_WIDTH"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_MAX_PBUFFER_HEIGHT, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_MAX_PBUFFER_HEIGHT"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_MAX_PBUFFER_PIXELS, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_MAX_PBUFFER_PIXELS"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_NATIVE_RENDERABLE, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_NATIVE_RENDERABLE"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_NATIVE_VISUAL_TYPE, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_NATIVE_VISUAL_TYPE"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_NATIVE_VISUAL_ID, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_NATIVE_VISUAL_ID"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_RENDERABLE_TYPE, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_RENDERABLE_TYPE"), ResultValue);
	
	eglGetConfigAttrib(display, EGLConfigInfo, EGL_SURFACE_TYPE, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_SURFACE_TYPE"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_TRANSPARENT_TYPE, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_TRANSPARENT_TYPE"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_TRANSPARENT_RED_VALUE, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_TRANSPARENT_RED_VALUE"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_TRANSPARENT_GREEN_VALUE, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_TRANSPARENT_GREEN_VALUE"), ResultValue);
	
	eglGetConfigAttrib(display, EGLConfigInfo, EGL_TRANSPARENT_BLUE_VALUE, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_TRANSPARENT_BLUE_VALUE"), ResultValue);

	eglGetConfigAttrib(display, EGLConfigInfo, EGL_RECORDABLE_ANDROID, &ResultValue);
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: %s = %d"), TEXT("EGL_RECORDABLE_ANDROID"), ResultValue);
}

// Get EGL config information (used by FBO only)
EGLConfig getConfig(EGLDisplay display, int flags, int version)
{
	EGLint renderableType = EGL_OPENGL_ES2_BIT;

	// The actual surface is generally RGBA or RGBX, so situationally omitting alpha
	// doesn't really help.  It can also lead to a huge performance hit on glReadPixels()
	// when reading into a GL_RGBA buffer.
	const EGLint attribList[] = {
		EGL_RED_SIZE, 8,
		EGL_GREEN_SIZE, 8,
		EGL_BLUE_SIZE, 8,
		EGL_ALPHA_SIZE, 8,
		EGL_RENDERABLE_TYPE, renderableType,
		EGL_RECORDABLE_ANDROID, EGL_TRUE,      // placeholder for recordable [@-3]
		EGL_NONE
	};
	EGLConfig configs[64];
	EGLint numConfigs = 0;

	if (!eglChooseConfig(display, attribList, configs, 64, &numConfigs))
	{
		UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: ------------ NATIVE SET RENDER SURFACE FAILED TO GET CONFIG!"));
	}
	else
	{
		UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: NUM CONFIGS RETURNED: %d"), numConfigs);
		for (int i = 0; i < numConfigs; i++)
		{
			UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: START ----------------------- CONFIG %d"), i);
			LogConfig2(display, configs[i]);
			UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: LOG CONFIG: END ----------------------- CONFIG %d"), i);
		}
	}

	return configs[0];
}

const  int EGLMinRedBits = 5;
const  int EGLMinGreenBits = 6;
const  int EGLMinBlueBits = 5;
const  int EGLMinAlphaBits = 0;
const  int EGLMinDepthBits = 0;
const  int EGLMinStencilBits = 0;
const  int EGLMinSampleBuffers = 0;
const  int EGLMinSampleSamples = 0;


const EGLint Attributes[] = {
	EGL_RED_SIZE,       EGLMinRedBits,
	EGL_GREEN_SIZE,     EGLMinGreenBits,
	EGL_BLUE_SIZE,      EGLMinBlueBits,
	EGL_ALPHA_SIZE,     EGLMinAlphaBits,
	EGL_DEPTH_SIZE,     EGLMinDepthBits,
	EGL_STENCIL_SIZE,   EGLMinStencilBits,
	EGL_SAMPLE_BUFFERS, EGLMinSampleBuffers,
	EGL_SAMPLES,        EGLMinSampleSamples,
	EGL_RENDERABLE_TYPE,  EGL_OPENGL_ES2_BIT,
	EGL_SURFACE_TYPE, EGL_WINDOW_BIT,
	EGL_CONFIG_CAVEAT,  EGL_NONE,
	EGL_RECORDABLE_ANDROID, EGL_TRUE,
	EGL_NONE
};

// Get EGL config information (used by FBO only)
EGLConfig GetConfig2(EGLDisplay eglDisplay)
{
	EGLConfig eglConfigParam;
	EGLint eglNumConfigs = 0;
	EGLConfig* EGLConfigList = NULL;
	EGLBoolean result = eglChooseConfig(eglDisplay, Attributes, NULL, 0, &eglNumConfigs);
	if (result)
	{
		int NumConfigs = eglNumConfigs;
		EGLConfigList = new EGLConfig[NumConfigs];
		result = eglChooseConfig(eglDisplay, Attributes, EGLConfigList, NumConfigs, &eglNumConfigs);
	}
	if (!result)
	{
		UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: Get Config 2 Start : FAILED!"));
		//return eglConfigParam; // ResetInternal();
	}
	UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: Get Config 2 Start : Num Configs: %d"), eglNumConfigs);

	//checkf(result == EGL_TRUE, TEXT(" eglChooseConfig error: 0x%x"), eglGetError());
	//checkf(PImplData->eglNumConfigs != 0, TEXT(" eglChooseConfig num EGLConfigLists is 0 . error: 0x%x"), eglGetError());

	int ResultValue = 0;
	bool haveConfig = false;
	int64 score = LONG_MAX;


	int sampleBuffers = 0;
	int sampleSamples = 0;
	int redSize = EGLMinRedBits;
	int greenSize = EGLMinGreenBits;
	int blueSize = EGLMinBlueBits;
	int depthSize = EGLMinDepthBits; // 16;
	int stencilSize = EGLMinStencilBits; // 8;
	int alphaSize = EGLMinAlphaBits; // 8;


	for (uint32_t i = 0; i < eglNumConfigs; i++)
	{
		eglGetConfigAttrib(eglDisplay, EGLConfigList[i], EGL_RECORDABLE_ANDROID, &ResultValue);
		if (ResultValue == EGL_TRUE)
		{
			LogConfig2(eglDisplay, EGLConfigList[i]);

			int64 currScore = 0;
			int r, g, b, a, d, s, sb, sc, nvi;
			eglGetConfigAttrib(eglDisplay, EGLConfigList[i], EGL_RED_SIZE, &ResultValue); r = ResultValue;
			eglGetConfigAttrib(eglDisplay, EGLConfigList[i], EGL_GREEN_SIZE, &ResultValue); g = ResultValue;
			eglGetConfigAttrib(eglDisplay, EGLConfigList[i], EGL_BLUE_SIZE, &ResultValue); b = ResultValue;
			eglGetConfigAttrib(eglDisplay, EGLConfigList[i], EGL_ALPHA_SIZE, &ResultValue); a = ResultValue;
			eglGetConfigAttrib(eglDisplay, EGLConfigList[i], EGL_DEPTH_SIZE, &ResultValue); d = ResultValue;
			eglGetConfigAttrib(eglDisplay, EGLConfigList[i], EGL_STENCIL_SIZE, &ResultValue); s = ResultValue;
			eglGetConfigAttrib(eglDisplay, EGLConfigList[i], EGL_SAMPLE_BUFFERS, &ResultValue); sb = ResultValue;
			eglGetConfigAttrib(eglDisplay, EGLConfigList[i], EGL_SAMPLES, &ResultValue); sc = ResultValue;

			// Optional, Tegra-specific non-linear depth buffer, which allows for much better
			// effective depth range in relatively limited bit-depths (e.g. 16-bit)
			int bNonLinearDepth = 0;
			if (eglGetConfigAttrib(eglDisplay, EGLConfigList[i], EGL_DEPTH_ENCODING_NV, &ResultValue))
			{
				bNonLinearDepth = (ResultValue == EGL_DEPTH_ENCODING_NONLINEAR_NV) ? 1 : 0;
			}

			// Favor EGLConfigLists by RGB, then Depth, then Non-linear Depth, then Stencil, then Alpha
			currScore = 0;
			currScore |= ((int64)FPlatformMath::Min(FPlatformMath::Abs(sb - sampleBuffers), 15)) << 29;
			currScore |= ((int64)FPlatformMath::Min(FPlatformMath::Abs(sc - sampleSamples), 31)) << 24;
			currScore |= FPlatformMath::Min(
				FPlatformMath::Abs(r - redSize) +
				FPlatformMath::Abs(g - greenSize) +
				FPlatformMath::Abs(b - blueSize), 127) << 17;
			currScore |= FPlatformMath::Min(FPlatformMath::Abs(d - depthSize), 63) << 11;
			currScore |= FPlatformMath::Min(FPlatformMath::Abs(1 - bNonLinearDepth), 1) << 10;
			currScore |= FPlatformMath::Min(FPlatformMath::Abs(s - stencilSize), 31) << 6;
			currScore |= FPlatformMath::Min(FPlatformMath::Abs(a - alphaSize), 31) << 0;

			if (currScore < score || !haveConfig)
			{
				eglConfigParam = EGLConfigList[i];
				//PImplData->DepthSize = d;		// store depth/stencil sizes
				haveConfig = true;
				score = currScore;
				UE_LOG(LogTemp, Log, TEXT("SET RENDER SURFACE: Get Config 2: Index"));
				//eglGetConfigAttrib(eglDisplay, EGLConfigList[i], EGL_NATIVE_VISUAL_ID, &ResultValue);
				//PImplData->NativeVisualID = ResultValue;
			}
		}
	}
	delete[] EGLConfigList;
	return eglConfigParam;
}

// Set a render surface from JNI (used by FBO only)
extern "C" jint Java_com_impreszions_ravemobiletools_ScreenRecorderFBO_nativeSetRenderSurface(JNIEnv* LocalJNIEnv, jobject LocalThiz, jobject surface)
{
	window = ANativeWindow_fromSurface(LocalJNIEnv, surface);
	int32_t windowFormat = ANativeWindow_getFormat(window);
	//ANativeWindow_setBuffersGeometry(window, 0, 0, WINDOW_FORMAT_RGBA_8888);

	bHasRenderSurface = false;
	
	if ( !window)
	{
		UE_LOG(LogTemp, Log, TEXT("ScreenRecording ERROR: nativeSetRenderSurface has no window!"));
	}

	ENQUEUE_UNIQUE_RENDER_COMMAND(FMobileScreenRecordingRenderSurface,
	{
		renderEGLDisplay = eglGetDisplay(EGL_DEFAULT_DISPLAY);
		if (renderEGLDisplay == EGL_NO_DISPLAY)
		{
			UE_LOG(LogTemp, Log, TEXT("ScreenRecording ERROR: nativeSetRenderSurface has no EGL Display"));
		}

		EGLConfig config = GetConfig2(renderEGLDisplay);
		LogConfig2(renderEGLDisplay, config);

		renderEGLContext = AndroidEGL::GetInstance()->GetCurrentContext();
		if (renderEGLContext == EGL_NO_CONTEXT)
		{
			UE_LOG(LogTemp, Log, TEXT("ScreenRecording ERROR: nativeSetRenderSurface has no EGL Context"));
		}

		renderUE4Surface = AndroidEGL::GetInstance()->GetRenderingContext()->eglSurface;
		if (renderUE4Surface == EGL_NO_SURFACE)
		{
			UE_LOG(LogTemp, Log, TEXT("ScreenRecording ERROR: nativeSetRenderSurface has no EGL Surface"));
		}

		GLenum error;
		while ((error = glGetError()) != GL_NO_ERROR) {
			UE_LOG(LogTemp, Log, TEXT("ScreenRecording ERROR: nativeSetRenderSurface cleanup error: %d"), error);
		}

		uint32 outWidth = 0;
		uint32 outHeight = 0;
		AndroidEGL::GetInstance()->GetDimensions(outWidth, outHeight);
		EGLint format = 0;
		ANativeWindow_setBuffersGeometry(window, outWidth, outHeight, WINDOW_FORMAT_RGBA_8888);
		UE_LOG(LogTemp, Log, TEXT("ScreenRecording: nativeSetRenderSurface set buffers: %d x %d = %d"), outWidth, outHeight, format);

		//GLenum error;
		while ((error = glGetError()) != GL_NO_ERROR) {
			UE_LOG(LogTemp, Log, TEXT("ScreenRecording ERROR: nativeSetRenderSurface cleanup error: %d"), error);
		}

		renderEGLSurface = eglCreateWindowSurface(renderEGLDisplay, config, window, NULL);
		GLenum renderError = glGetError();
		if ( renderError == GL_INVALID_ENUM ) UE_LOG(LogTemp, Log, TEXT("ScreenRecording ERROR: nativeSetRenderSurface GLError: GL_INVALID_ENUM"));
		if ( renderError == GL_INVALID_VALUE ) UE_LOG(LogTemp, Log, TEXT("ScreenRecording ERROR: nativeSetRenderSurface GLError: GL_INVALID_VALUE"));
		if ( renderError == GL_INVALID_OPERATION ) UE_LOG(LogTemp, Log, TEXT("ScreenRecording ERROR: nativeSetRenderSurface GLError: GL_INVALID_OPERATION"));
		if ( renderError == GL_INVALID_FRAMEBUFFER_OPERATION ) UE_LOG(LogTemp, Log, TEXT("ScreenRecording ERROR: nativeSetRenderSurface GLError: GL_INVALID_FRAMEBUFFER_OPERATION"));
		if ( renderError == GL_OUT_OF_MEMORY ) UE_LOG(LogTemp, Log, TEXT("ScreenRecording ERROR: nativeSetRenderSurface GLError: GL_OUT_OF_MEMORY"));
		
		if (renderEGLSurface == EGL_NO_SURFACE)
		{
			UE_LOG(LogTemp, Log, TEXT("ScreenRecording ERROR: nativeSetRenderSurface has no RENDER EGL Surface:"));
		}
		bHasRenderSurface = true;
		glFlush();
		eglMakeCurrent(renderEGLDisplay, renderEGLSurface, renderEGLSurface, renderEGLContext);

		renderError = glGetError();
		if (renderError == GL_INVALID_ENUM) UE_LOG(LogTemp, Log, TEXT("ScreenRecording ERROR: nativeSetRenderSurface GLError: GL_INVALID_ENUM"));
		if (renderError == GL_INVALID_VALUE) UE_LOG(LogTemp, Log, TEXT("ScreenRecording ERROR: nativeSetRenderSurface GLError: GL_INVALID_VALUE"));
		if (renderError == GL_INVALID_OPERATION) UE_LOG(LogTemp, Log, TEXT("ScreenRecording ERROR: nativeSetRenderSurface GLError: GL_INVALID_OPERATION"));
		if (renderError == GL_INVALID_FRAMEBUFFER_OPERATION) UE_LOG(LogTemp, Log, TEXT("ScreenRecording ERROR: nativeSetRenderSurface GLError: GL_INVALID_FRAMEBUFFER_OPERATION"));
		if (renderError == GL_OUT_OF_MEMORY) UE_LOG(LogTemp, Log, TEXT("ScreenRecording ERROR: nativeSetRenderSurface GLError: GL_OUT_OF_MEMORY"));

		eglSwapBuffers(renderEGLDisplay, renderEGLSurface);
		glFlush();
		eglMakeCurrent(renderEGLDisplay, renderUE4Surface, renderUE4Surface, renderEGLContext);
		UE_LOG(LogTemp, Log, TEXT("ScreenRecording: nativeSetRenderSurface completed: renderEGLSurface:%d, renderUE4Surface:%d , renderEGLDisplay:%d , renderEGLContext:%d"), renderEGLSurface, renderUE4Surface, renderEGLDisplay, renderEGLContext);
	}
	);

	mOffscreenTexture = 0;
	return (jint)mOffscreenTexture;
}

// Set a render surface from JNI (used by FBO only)
extern "C" jint Java_com_impreszions_ravemobiletools_ScreenRecorderMediaProjection_nativeSetRenderSurface(JNIEnv* LocalJNIEnv, jobject LocalThiz, jobject surface)
{
	return Java_com_impreszions_ravemobiletools_ScreenRecorderFBO_nativeSetRenderSurface(LocalJNIEnv, LocalThiz, surface);
}

// Set a render surface from JNI (used by FBO only)
extern "C" jint Java_com_impreszions_ravemobiletools_ScreenRecorderFBOThread_nativeSetRenderSurface(JNIEnv* LocalJNIEnv, jobject LocalThiz, jobject surface)
{
	return Java_com_impreszions_ravemobiletools_ScreenRecorderFBO_nativeSetRenderSurface(LocalJNIEnv, LocalThiz, surface);
}
#endif

#if PLATFORM_IOS
#import <UIKit/UIKit.h>
#import <ReplayKit/ReplayKit.h>
#include "IOSAppDelegate.h"
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

#define SCREEN_RECORDER_USE_ASSETWRITER		1
#define SCREEN_RECORDER_USE_CAMERA			0

@interface IOSReplayKitOverlayWindow : UIWindow
{
}
@end

@implementation IOSReplayKitOverlayWindow
@end

// Singleton class for IOS ReplayKit functionality
@interface IOSReplayKit : UIViewController<RPPreviewViewControllerDelegate, RPScreenRecorderDelegate>
{
	UIWindow *overlayWindow;
	UIImageView *overlayImageView;
	UIView *cameraView;
	UAsyncStopRecordingTask *callbackDelegate;

	bool isHideOverlay;
	bool isOverlayShown;
	bool isStopping;
	UIImage *hasOverlayImage;

	RPScreenRecorder *recorder;
	bool hasCompletedStart;
	bool hasCompletedStop;
	
	UAsyncStartRecordingTask *startCallbackDelegate;
	NSString *outputPath;
	AVAssetWriter *assetWriter;
	AVAssetWriterInput  *videoInput;
	AVAssetWriterInput  *audioInput;

	bool isCameraEnabled;
	bool isMicrophoneEnabled;
	float isRotation;

	bool isSaveRecording;
	bool isSaveToGallery;
	bool isGetTexture;

	UIBackgroundTaskIdentifier backgroundTaskID;
	CMTime lastSessionTime;
}
@end

@implementation IOSReplayKit

// Return singleton reference
+ (IOSReplayKit*)GetDelegate
{
	static IOSReplayKit* Singleton = [[IOSReplayKit alloc] init]; 
	return Singleton;
}

- (IOSReplayKit*) init
{
	self = [super init];
	if (self)
	{
		cameraView = nil;
		isHideOverlay = false;
		isOverlayShown = false;
		isStopping = false;
		overlayWindow = nil;
		overlayImageView = nil;
		hasCompletedStart = true;
		hasCompletedStop = true;
		
		isSaveRecording = false;
		isSaveToGallery = false;
		isGetTexture = false;

		isCameraEnabled = false;
		isMicrophoneEnabled = false;
		isRotation = 0;
	}
	return self;
}


-(void)dealloc
{
	startCallbackDelegate = nil;
	callbackDelegate = nil;
	[overlayWindow autorelease];
	[hasOverlayImage autorelease];
	hasOverlayImage = nil;
	//currentSampleBuffer = nil;

	[super dealloc];
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
	if (([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortrait))
	{
		return UIInterfaceOrientationMaskPortrait;
	}
	if (([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationPortraitUpsideDown))
	{
		return UIInterfaceOrientationMaskPortraitUpsideDown;
	}
	if (([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeRight))
	{
		return UIInterfaceOrientationMaskLandscapeRight;
	}
	if (([[UIApplication sharedApplication] statusBarOrientation] == UIInterfaceOrientationLandscapeLeft))
	{
		return UIInterfaceOrientationMaskLandscapeLeft;
	}

	return UIInterfaceOrientationMaskAll;
}

-(BOOL)shouldAutorotate
{
	bool results = isOverlayShown ? NO : YES;
	isOverlayShown = true;
	return NO;
}

-(void)showOverlayWindow:(bool)bCameraEnabled overlayImage:(UIImage *)overlayImage
{
	if (!isOverlayShown)
	{
		isOverlayShown = true;

		[overlayImage retain];
		dispatch_async(dispatch_get_main_queue(), ^{
			if (!overlayWindow)
			{
				overlayWindow = [[IOSReplayKitOverlayWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
				[overlayWindow setUserInteractionEnabled : NO];
				overlayWindow.windowLevel = UIWindowLevelNormal + 1;
				overlayWindow.rootViewController = self;
				overlayWindow.translatesAutoresizingMaskIntoConstraints = false;
			}

			overlayWindow.hidden = NO;
			[overlayWindow setFrame : [[UIScreen mainScreen] bounds]];
			[overlayWindow setAutoresizingMask : UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
			[overlayWindow layoutSubviews];
			[overlayWindow setTransform:[[[UIApplication sharedApplication] keyWindow] transform]];

			if (overlayImage)
			{
				if (!overlayImageView)
				{
					overlayImageView = [[UIImageView alloc] initWithImage:overlayImage];
					[overlayImageView setContentMode : UIViewContentModeScaleAspectFit];
					[overlayImageView setFrame : [[UIScreen mainScreen] bounds]];
					[overlayWindow addSubview : overlayImageView];
					overlayImageView.translatesAutoresizingMaskIntoConstraints = false;
				}
				overlayImageView.hidden = NO;
				[overlayImageView removeFromSuperview];
				[overlayWindow addSubview : overlayImageView];
				[overlayImageView setImage : overlayImage];
			}
			else
			{
				if (overlayImageView)
				{
					overlayImageView.hidden = YES;
					[overlayImageView removeFromSuperview];
				}
			}

#if SCREEN_RECORDER_USE_CAMERA
			if (bCameraEnabled)
			{
				cameraView = [[RPScreenRecorder sharedRecorder] cameraPreviewView];
				if (cameraView)[overlayWindow addSubview : cameraView];
			}
#endif

			[overlayWindow layoutSubviews];
			[overlayImage autorelease];
			[UIViewController attemptRotationToDeviceOrientation];
		});
	}
}

-(void)hideOverlayWindow
{
	if (isOverlayShown)
	{
		isOverlayShown = false;
		dispatch_async(dispatch_get_main_queue(), ^{
			if (cameraView)
			{
				[cameraView removeFromSuperview];
				cameraView = nil;
			}

			if (overlayWindow)
			{
				overlayWindow.rootViewController = nil;
				[overlayWindow removeFromSuperview];
				overlayWindow.hidden = YES;
				isOverlayShown = false;

				[overlayWindow release];
				overlayWindow = nil;
			}

			if (overlayImageView)
			{
				[overlayImageView release];
				overlayImageView = nil;
			}

			self.view = nil;
		});
	}
}

-(bool)saveVideo:(NSString *)videoPath
{
	FString videoPathStr = IOS_TO_UE_STRING(videoPath);
	FString ScreenShotName = CurrentSaveFolder / FPaths::GetBaseFilename(videoPathStr, true);
	if (FPaths::GetExtension(ScreenShotName).IsEmpty()) ScreenShotName += "." + FPaths::GetExtension(videoPathStr);
	EnsureFileFolderExists(ScreenShotName);
	//FString iosPath = RaveMobilePhotoGallery::ConvertToIOSPath(ScreenShotName, true);
	UE_LOG(LogTemp, Log, TEXT("Saving Recording to : %s"), *ScreenShotName);
	return RaveMobilePhotoGallery::CopyFile(*videoPathStr, *ScreenShotName);

	//return [[NSFileManager defaultManager] copyItemAtPath:videoPath toPath : UE_TO_IOS_STRING(iosPath) error : nil];
}

// RPScreenRecorderDelegate
-(void) screenRecorder:(RPScreenRecorder *)screenRecorder
	didStopRecordingWithPreviewViewController : (RPPreviewViewController *)previewViewController
	error : (NSError *)error
{
}

-(void)screenRecorderDidChangeAvailability : (RPScreenRecorder *)screenRecorder
{
}

-(void) video: (NSString *)videoPath didFinishSavingWithError : (NSError *)error contextInfo : (void *)contextInfo
{
	FString videoPathStr = IOS_TO_UE_STRING(videoPath);

	if (isSaveRecording)
	{
		[self saveVideo : videoPath];
	}

	if (callbackDelegate)
	{
		if (error)
		{
			callbackDelegate->IsFailure(EScreenRecordingFileWriteError);
		}
		else
		{
			if (isGetTexture)
			{
				FString destPathUrl = "file://" + videoPathStr;
				callbackDelegate->IsSuccess(destPathUrl, videoPathStr);
			}
			else
			{
				callbackDelegate->IsSuccess(nullptr, videoPathStr);
			}
		}
		callbackDelegate = nil;
	}
	hasCompletedStop = true;
}

- (void)doStartRecording
{
	static bool isFirstTime = false;

	if (@available(iOS 11.0, *))
	{
		dispatch_async(dispatch_get_main_queue(), ^{

			backgroundTaskID = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler : ^{
				[[UIApplication sharedApplication] endBackgroundTask :backgroundTaskID];
				backgroundTaskID = UIBackgroundTaskInvalid;
			}];

			if (isFirstTime)
			{
				
			}

			bool bHideOverlay = isHideOverlay;
			bool bCameraEnabled = isCameraEnabled;
			bool bMicrophoneEnabled = isMicrophoneEnabled;
			float rotation = isRotation;
			NSError *error = nil;

			FString filename = RaveMobilePhotoGallery::GetTimestampedFilename(".mp4","","");
			NSString *tempPath = NSTemporaryDirectory();
			FString destPathStr = IOS_TO_UE_STRING(tempPath);
			destPathStr = destPathStr / "OnDemandResources";
			NSString *dstPath = UE_TO_IOS_STRING(destPathStr);
			bool bCreateDirectory = [[NSFileManager defaultManager] createDirectoryAtPath:dstPath
				withIntermediateDirectories : YES
				attributes : nil
				error : nil];

			NSLog(@"Created Directory : %@ -> %@", dstPath, bCreateDirectory ? @"YES" : @"NO");

			NSString *uniqueFileName = UE_TO_IOS_STRING(filename);
			outputPath = [dstPath stringByAppendingPathComponent: uniqueFileName];
			FString outputPathUE = IOS_TO_UE_STRING(outputPath);
			UE_LOG(LogTemp, Log, TEXT("Asset writer video out path : %s"), *outputPathUE);
			NSLog(@"Asset writer video out path : %@", outputPath);
			
			[videoInput release];
			[audioInput release];
			[assetWriter release];

			audioInput = nil;
			videoInput = nil;
			assetWriter = nil;

			assetWriter = [[AVAssetWriter assetWriterWithURL : [NSURL fileURLWithPath : outputPath] fileType : AVFileTypeMPEG4 error : &error] retain];
			assetWriter.shouldOptimizeForNetworkUse = NO;

			NSDictionary *compressionProperties = @{AVVideoProfileLevelKey: AVVideoProfileLevelH264HighAutoLevel,
				AVVideoH264EntropyModeKey : AVVideoH264EntropyModeCABAC,
				AVVideoAverageBitRateKey : @(1280 * 720 * 11.4),  //@(1920 * 1080 * 11.4),
										   AVVideoMaxKeyFrameIntervalKey  : @30, // @60,
				AVVideoAllowFrameReorderingKey : @NO};

//			float screenWidth = floor([[UIScreen mainScreen] nativeBounds].size.width / 16) * 16.0f;
//			float screenHeight = floor([[UIScreen mainScreen] nativeBounds].size.height / 16) * 16.0f;

			float screenWidth = [[UIScreen mainScreen] nativeBounds].size.width;
			float screenHeight = [[UIScreen mainScreen] nativeBounds].size.height;
			recordingWidth = screenWidth;
			recordingHeight = screenHeight;

			bool isLandscape = UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation);
			NSNumber* width = [NSNumber numberWithFloat : isLandscape ? screenHeight : screenWidth];
			NSNumber* height = [NSNumber numberWithFloat : isLandscape ? screenWidth : screenHeight];

			NSDictionary *videoSettings = @{
				AVVideoCompressionPropertiesKey : compressionProperties,
				AVVideoCodecKey : AVVideoCodecTypeH264,
				AVVideoWidthKey : width,
				AVVideoHeightKey : height};

			videoInput = [[AVAssetWriterInput assetWriterInputWithMediaType : AVMediaTypeVideo outputSettings : videoSettings] retain];
			videoInput.transform = CGAffineTransformMakeRotation(rotation / 180 * M_PI);

			AudioChannelLayout acl;
			bzero(&acl, sizeof(acl));
			acl.mChannelLayoutTag = kAudioChannelLayoutTag_Mono;

			NSDictionary *audioOutputSettings = [NSDictionary dictionaryWithObjectsAndKeys :
			[NSNumber numberWithInt : kAudioFormatMPEG4AAC], AVFormatIDKey,
				[NSNumber numberWithInt : 1], AVNumberOfChannelsKey,
				[NSNumber numberWithFloat : 44100.0], AVSampleRateKey,
				[NSNumber numberWithInt : 64000], AVEncoderBitRateKey,
				[NSData dataWithBytes : &acl length : sizeof(acl)], AVChannelLayoutKey,
				nil];
			audioInput = [[AVAssetWriterInput assetWriterInputWithMediaType : AVMediaTypeAudio outputSettings : audioOutputSettings] retain];

			[assetWriter addInput : videoInput];
			[videoInput setMediaTimeScale : 60];
			[assetWriter setMovieTimeScale : 60];
			[videoInput setExpectsMediaDataInRealTime : YES];
			//[videoInput setExpectsMediaDataInRealTime : NO];
			[audioInput setExpectsMediaDataInRealTime : NO];
			[assetWriter addInput : audioInput];

			recorder = [RPScreenRecorder sharedRecorder];
			recorder.delegate = self;

#if SCREEN_RECORDER_USE_CAMERA
			[recorder setCameraEnabled : bCameraEnabled];
			[recorder setMicrophoneEnabled : bMicrophoneEnabled];
#else
			[recorder setCameraEnabled : false];
			[recorder setMicrophoneEnabled : false];
#endif

			[recorder startCaptureWithHandler : ^ (CMSampleBufferRef  _Nonnull sampleBuffer, RPSampleBufferType bufferType, NSError * _Nullable error1) {
				if (error1)
				{
					if (startCallbackDelegate)
					{
						startCallbackDelegate->IsFailure(EScreenRecordingUnknownError);
						startCallbackDelegate = nil;
						if (backgroundTaskID != UIBackgroundTaskInvalid)
						{
							[[UIApplication sharedApplication] endBackgroundTask:backgroundTaskID];
							backgroundTaskID = UIBackgroundTaskInvalid;
						}
					}
				}
				else
				{
					if (CMSampleBufferDataIsReady(sampleBuffer)) {
						if (assetWriter.status == AVAssetWriterStatusUnknown && bufferType == RPSampleBufferTypeVideo) {
							[assetWriter startWriting];
							[assetWriter startSessionAtSourceTime : CMSampleBufferGetPresentationTimeStamp(sampleBuffer)];
						}

						if (assetWriter.status == AVAssetWriterStatusFailed)
						{
							NSLog(@"Screen Recording: An error occured. %@, %@", assetWriter.outputURL, assetWriter.error.localizedDescription);
							[[RPScreenRecorder sharedRecorder] stopCaptureWithHandler:^ (NSError * _Nullable error2) {
							}];

							if (!hasCompletedStart)
							{
								hasCompletedStart = true;
								NSLog(@"Screen Recording: Unable to write to file. %@", assetWriter.outputURL);
								if (startCallbackDelegate)
								{
									startCallbackDelegate->IsFailure(EScreenRecordingFileWriteError);
									startCallbackDelegate = nil;
								}
								[videoInput release];
								[audioInput release];
								[assetWriter release];

								audioInput = nil;
								videoInput = nil;
								assetWriter = nil;
								recorder = nil;

								if (backgroundTaskID != UIBackgroundTaskInvalid)
								{
									[[UIApplication sharedApplication] endBackgroundTask:backgroundTaskID];
									backgroundTaskID = UIBackgroundTaskInvalid;
								}
							}
							return;
						}

						if (bufferType == RPSampleBufferTypeVideo) {
							if (videoInput.isReadyForMoreMediaData) {
								[videoInput appendSampleBuffer : sampleBuffer];
								lastSessionTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
							}
							else {
								NSLog(@"Screen Recording: Not ready for video");
							}
						}
						else if (bufferType == RPSampleBufferTypeAudioApp) {
							if (audioInput.isReadyForMoreMediaData) {
								[audioInput appendSampleBuffer : sampleBuffer];
							}
							else {
								NSLog(@"Screen Recording: Not ready for audio");
							}
						}
					}
				}
			}

			completionHandler: ^ (NSError * _Nullable error1) {
				if (!hasCompletedStart)
				{
					hasCompletedStart = true;
					if (!error1)
					{
						AVAudioSession *session = [AVAudioSession sharedInstance];
						[session setActive : YES error : nil];

						NSLog(@"Screen Recording: Recording started successfully.");
						if (bHideOverlay)[self hideOverlayWindow];
						else[self showOverlayWindow : bCameraEnabled overlayImage : hasOverlayImage];

						if (startCallbackDelegate)
						{
							startCallbackDelegate->IsSuccess();
							startCallbackDelegate = nil;
						}
					}
					else
					{
						if (startCallbackDelegate)
						{
							startCallbackDelegate->IsFailure(EScreenRecordingUnknownError);
							startCallbackDelegate = nil;

							if (backgroundTaskID != UIBackgroundTaskInvalid)
							{
								[[UIApplication sharedApplication] endBackgroundTask:backgroundTaskID];
								backgroundTaskID = UIBackgroundTaskInvalid;
							}
						}
					}
					[hasOverlayImage autorelease];
					hasOverlayImage = nil;
				}
			}];
		});

	}
	else {
		startCallbackDelegate->IsFailure(EScreenRecordingUnsupportedOS);
		hasCompletedStart = true;
	}
}

-(void)startRecording : (UAsyncStartRecordingTask *)callback
	overlayImage : (UIImage *)overlayImage
	rotation : (float)rotation
{
	static bool isFirstTime = false;
	bool bCameraEnabled = false;
	bool bMicrophoneEnabled = false;
	bool bHideOverlay = true;

	if (hasCompletedStart)
	{
		[overlayImage retain];
		dispatch_async(dispatch_get_main_queue(), ^{

		recorder = [RPScreenRecorder sharedRecorder];
		recorder.delegate = self;

		if ([recorder isAvailable] && ![recorder isRecording])
		{
#if SCREEN_RECORDER_USE_CAMERA
			[recorder setCameraEnabled : bCameraEnabled];
			[recorder setMicrophoneEnabled : bMicrophoneEnabled];
#else
			[recorder setCameraEnabled : false];
			[recorder setMicrophoneEnabled : false];
#endif
			hasCompletedStart = false;

			isHideOverlay = bHideOverlay;
			isCameraEnabled = bCameraEnabled;
			isMicrophoneEnabled = bMicrophoneEnabled;
			isRotation = rotation;
			hasOverlayImage = [overlayImage retain];
			startCallbackDelegate = callback;

			// Source: https://reformatcode.com/code/ios/extract-the-view-from-replaykit-without-presenting-the-view-controller-in-ios-objective-c

			AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType : AVMediaTypeVideo];
			switch (authStatus)
			{
			case AVAuthorizationStatusNotDetermined:
				[AVCaptureDevice requestAccessForMediaType : AVMediaTypeVideo completionHandler : ^ (BOOL granted)
					{
						hasCompletedStart = true;
						if (granted)
						{
							callback->IsAccessGranted(granted);
						}
						else
						{
							callback->IsFailure(EScreenRecordingNoPermission);
							[overlayImage release];

							[videoInput release];
							[audioInput release];
							[assetWriter release];

							audioInput = nil;
							videoInput = nil;
							assetWriter = nil;
							recorder = nil;
							hasCompletedStart = true;
						}
					}];
				break;

				case AVAuthorizationStatusAuthorized:
					[self doStartRecording];
					break;

				case AVAuthorizationStatusRestricted:
				case AVAuthorizationStatusDenied:
				default:
					NSLog(@"Screen Recording: Permission not granted");
					callback->IsFailure(EScreenRecordingNoPermission);
					[overlayImage release];

					[videoInput release];
					[audioInput release];
					[assetWriter release];

					audioInput = nil;
					videoInput = nil;
					assetWriter = nil;
					recorder = nil;
					hasCompletedStart = true;
					break;
				}
			}
			else if (![recorder isAvailable])
			{
				callback->IsFailure(EScreenRecordingNotAvailable);
			}
			else
			{
				callback->IsFailure(EScreenRecordingIsRecording);
			}
		});
	}
	else
	{
		callback->IsFailure(EScreenRecordingPendingPrevious);
		[overlayImage release];
	}
}

-(void)finishRecording
{
	NSLog(@"Screen Recording: Attempt to finish recording...");
	if (assetWriter.status == AVAssetWriterStatusWriting)
	{
		NSLog(@"Screen Recording: Recording stopped successfully.Cleaning up...");
			
		[videoInput markAsFinished];
		[audioInput markAsFinished];
		callbackDelegate->IsSaving();

		[assetWriter endSessionAtSourceTime : lastSessionTime];
		[assetWriter finishWritingWithCompletionHandler : ^{
			FString outputURLString = IOS_TO_UE_STRING([assetWriter.outputURL absoluteString]);
			NSURL *movieURL = [assetWriter.outputURL retain];
			NSString *moviePath = [[movieURL path] retain];
			FString destPathUrl = IOS_TO_UE_STRING([movieURL absoluteString]);
			FString moviePathStr = IOS_TO_UE_STRING(moviePath);

			AVAsset *assetInfo = [AVAsset assetWithURL : movieURL];
			if (assetInfo)
			{
				NSArray *videoTracks = [assetInfo tracksWithMediaType : AVMediaTypeVideo];
				if ([videoTracks count] > 0)
				{
					AVAssetTrack* videoTrack = [videoTracks objectAtIndex : 0];
					CGAffineTransform transform = [videoTrack preferredTransform];
					int videoRotation = (int)(atan2(transform.b, transform.a) * 180 / M_PI);

					recordingWidth = videoTrack.naturalSize.width;
					recordingHeight = videoTrack.naturalSize.height;
					recordingRotation = videoRotation;
				}
			}

			if (isSaveToGallery)
			{
				if (UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(moviePath))
				{
					UISaveVideoAtPathToSavedPhotosAlbum(moviePath, self, @selector(video:didFinishSavingWithError:contextInfo:), nil);
				}
				else
				{
					callbackDelegate->IsFailure(EScreenRecordingIncompatibleMovieFile);
					hasCompletedStop = true;
				}
			}
			else
			{
				if (isSaveRecording)
				{
					[self saveVideo : [movieURL absoluteString]];
				}

				if (isGetTexture) callbackDelegate->IsSuccess(destPathUrl, moviePathStr);
				else callbackDelegate->IsSuccess(nullptr, moviePathStr);
				hasCompletedStop = true;
			}

			[moviePath release];
			[movieURL release];
			[videoInput release];
			[audioInput release];
			[assetWriter release];

			moviePath = nil;
			movieURL = nil;
			audioInput = nil;
			videoInput = nil;
			assetWriter = nil;
			recorder = nil;

			if (backgroundTaskID != UIBackgroundTaskInvalid)
			{
				[[UIApplication sharedApplication] endBackgroundTask:backgroundTaskID];
				backgroundTaskID = UIBackgroundTaskInvalid;
			}
		}];
	}
	else
	{
		NSLog(@"Screen Recording: Recording was not in writing status...");

		callbackDelegate->IsFailure(EScreenRecordingFailedStopRecording);

		[videoInput release];
		[audioInput release];
		[assetWriter release];

		audioInput = nil;
		videoInput = nil;
		assetWriter = nil;
		recorder = nil;

		if (backgroundTaskID != UIBackgroundTaskInvalid)
		{
			[[UIApplication sharedApplication] endBackgroundTask:backgroundTaskID];
			backgroundTaskID = UIBackgroundTaskInvalid;
		}
	}
}

-(void) stopRecording:(UAsyncStopRecordingTask *)callback saveRecording:(bool)bSaveRecording
	saveToGallery:(bool)bSaveToGallery
	getTexture:(bool)bGetTexture
{
	NSLog(@"Screen recording : Stop Recording : %@", hasCompletedStop ? @"YES" : @"NO");
	isSaveRecording = bSaveRecording;
	isSaveToGallery = bSaveToGallery;
	isGetTexture = bGetTexture;

	recorder = [RPScreenRecorder sharedRecorder];
	recorder.delegate = self;

	if (![recorder isRecording])
	{
		callback->IsFailure(EScreenRecordingErrors::EScreenRecordingNotRecording);
		hasCompletedStop = true;
		if (backgroundTaskID != UIBackgroundTaskInvalid)
		{
			[[UIApplication sharedApplication] endBackgroundTask:backgroundTaskID];
			backgroundTaskID = UIBackgroundTaskInvalid;
		}
		return;
	}

	if (hasCompletedStop)
	{
		hasCompletedStop = false;
		[self hideOverlayWindow];
		callbackDelegate = callback;

		if (@available(iOS 11.0, *))
		{
			dispatch_async(dispatch_get_main_queue(), ^{
				[self finishRecording];
				[recorder stopCaptureWithHandler:^ (NSError * _Nullable error) {
					if (!hasCompletedStop)
					{
						if (!error)
						{
							//[self finishRecording];
						}
						else
						{
							callbackDelegate->IsFailure(EScreenRecordingFailedStopRecording);
							hasCompletedStop = true;
							if (backgroundTaskID != UIBackgroundTaskInvalid)
							{
								[[UIApplication sharedApplication] endBackgroundTask:backgroundTaskID];
								backgroundTaskID = UIBackgroundTaskInvalid;
							}
						}
					}
				}];
			});
		}
		else {
			callback->IsFailure(EScreenRecordingErrors::EScreenRecordingUnsupportedOS);
			hasCompletedStop = true;
			if (backgroundTaskID != UIBackgroundTaskInvalid)
			{
				[[UIApplication sharedApplication] endBackgroundTask:backgroundTaskID];
				backgroundTaskID = UIBackgroundTaskInvalid;
			}
		}
	}
	else
	{
		callback->IsFailure(EScreenRecordingErrors::EScreenRecordingPendingPrevious);
		if (backgroundTaskID != UIBackgroundTaskInvalid)
		{
			[[UIApplication sharedApplication] endBackgroundTask:backgroundTaskID];
			backgroundTaskID = UIBackgroundTaskInvalid;
		}
	}
}

- (bool)isRecording
{
	return [[RPScreenRecorder sharedRecorder] isRecording];
}

// RPScreenRecorderDelegate
-(void) previewController : (RPPreviewViewController *)previewController didFinishWithActivityTypes : (NSSet<NSString *> *)activityTypes
{
	[previewController dismissViewControllerAnimated : YES completion : nil];
	if (FAudioDevice *audioDevice = FAudioDevice::GetMainAudioDevice())
	{
		audioDevice->ResumeContext();
	}
}

-(bool)checkPermissions
{
	AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType : AVMediaTypeVideo];
	switch (authStatus)
	{
	case AVAuthorizationStatusNotDetermined:
		dispatch_async(dispatch_get_main_queue(), ^{
			[AVCaptureDevice requestAccessForMediaType : AVMediaTypeVideo completionHandler : ^ (BOOL granted)
		{
			NSLog(@"Check permission granted : %@", granted ? @"YES" : @"NO");
		}];
		});
		return false;
		break;

	case AVAuthorizationStatusAuthorized:
		return true;
		break;

	case AVAuthorizationStatusRestricted:
	case AVAuthorizationStatusDenied:
	default:
		return false;
		break;
	}
}

@end

// Perform functions when access is granted to start revcording
void UAsyncStartRecordingTask::IsAccessGranted(bool bGranted)
{
	if (bGranted)
	{
		AsyncTask(ENamedThreads::GameThread, [this]() {
			[[IOSReplayKit GetDelegate] doStartRecording];
		});
	}
}

#endif // PLATFORM_IOS


void RecordingPause()
{
#if PLATFORM_ANDROID
	AndroidThunkCpp_Pause();
#endif
}

void RecordingResume()
{
#if PLATFORM_ANDROID
	AndroidThunkCpp_Resume();
#endif
}




// Returns if the plugin is currently recording the screen.
bool IsCurrentlyRecording()
{
#if PLATFORM_IOS
	return[[IOSReplayKit GetDelegate] isRecording];
#endif

#if PLATFORM_ANDROID
	return AndroidThunkCpp_IsScreenCapturing();
#endif

#if PLATFORM_WINDOWS || PLATFORM_MAC
	return SimulatedIsRecording;
#endif

	return false;
}

// Returns if the plugin is currently recording the screen.
bool UAsyncStartRecordingTask::IsRecording()
{
	return IsCurrentlyRecording();
}

bool UAsyncStartRecordingTask::Stop()
{
	RecordingPause();
	return false;
}

bool UAsyncStartRecordingTask::Resume()
{
	RecordingResume();
	return false;
}

UAsyncStartRecordingTask::UAsyncStartRecordingTask(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	if (HasAnyFlags(RF_ClassDefaultObject) == false)
	{
		AddToRoot();
	}
	bHasResults = false;
	bIsActivated = false;
	bHasResponse = false;
}

void UAsyncStartRecordingTask::Activate()
{
	Super::Activate();
	bIsActivated = true;
	if (bHasResults)
	{
		if (bIsSuccess) IsSuccess();
		else IsFailure(lastError);
	}
}

void UAsyncStartRecordingTask::IsSuccess()
{
	if (bHasResponse) return;

	bHasResults = true;
	bIsSuccess = true;
	if (bIsActivated)
	{
		AsyncTask(ENamedThreads::GameThread, [this]() {
			OnSuccess.Broadcast(EScreenRecordingSuccess);
			RemoveFromRoot();
			SetReadyToDestroy();
		});
		bHasResponse = true;
	}
}

void UAsyncStartRecordingTask::IsFailure(EScreenRecordingErrors errorCode)
{
	if (bHasResponse) return;

	bHasResults = true;
	bIsSuccess = false;
	lastError = errorCode;
	if (bIsActivated)
	{
		AsyncTask(ENamedThreads::GameThread, [this, errorCode]() {
			OnFailure.Broadcast(errorCode);
			RemoveFromRoot();
			SetReadyToDestroy();
		});
		bHasResponse = true;
	}
}

UAsyncStartRecordingTask* UAsyncStartRecordingTask::StartRecording(const UObject* WorldContextObject,
	bool bSaveToScreenshots, bool bSaveToGallery,
	FString SaveFolder, 
	EScreenshotRotation Rotation,
	bool bAndroidSaveToExternal)
{
	EScreenRecordingAndroidMethod AndroidRecordingMethod = EScreenRecordingAndroidMethod::EScreenRecordingAndroidMethod_MediaProjection;
	UAsyncStartRecordingTask* ScreenshotTask = NewObject<UAsyncStartRecordingTask>();
	FVector2D OffsetWidthHeight = FVector2D(0, 0);
	ScreenshotTask->DoStartRecording(WorldContextObject, bSaveToScreenshots, bSaveToGallery, Rotation, OffsetWidthHeight, SaveFolder, bAndroidSaveToExternal, AndroidRecordingMethod);
	return ScreenshotTask;
}

void UAsyncStartRecordingTask::DoOnAndroidPermissionGranted(const TArray<FString>& Permissions, const TArray<bool>& GrantResults)
{
#if PLATFORM_ANDROID
	if (GrantResults.Num() > 1 && GrantResults[0] && GrantResults[1] )
	{
		StartRecordingData dataRecording = lastRecordingData;
		AndroidThunkCpp_StartScreenCapture(dataRecording.filePath, nullptr,
			dataRecording.outWidth, dataRecording.outHeight, dataRecording.rotationInt,
			dataRecording.bUseFBO, dataRecording.bLockOrientation, dataRecording.OffsetWidthHeight.X, dataRecording.OffsetWidthHeight.Y,
			bIsSaveToGallery);
	}
	else
	{
		IsFailure(EScreenRecordingNoPermission);
	}
#endif
}

void UAsyncStartRecordingTask::DoStartRecordingWithOverlayTexture(const UObject* WorldContextObject,
	bool bSaveToScreenshots, bool bSaveToGallery,
	UTexture* OverlayTexture, EScreenshotRotation Rotation, FVector2D OffsetWidthHeight,
	FString SaveFolder, bool bAndroidSaveToExternal, EScreenRecordingAndroidMethod AndroidRecordingMethod)
{
	if (IsCurrentlyRecording())
	{
		IsFailure(EScreenRecordingIsRecording);
		return;
	}
	bIsSaveToScreenshots = bSaveToScreenshots;
	bIsSaveToGallery = bSaveToGallery;
	CurrentSaveFolder = SaveFolder;

#if PLATFORM_IOS
	//	UIImage *overlayImage = UIImageFrom(OverlayTexture, 1.0f, false);
#ifdef ORIENTATION_SENSOR
		FOrientationSensor::LockDeviceOrientationChanges(true);
#endif
		float rotationInt = 0;
		switch (Rotation)
		{
			case EScreenshotRotation::EScreenshotRotation_ROTATE0:		rotationInt = 0; break;
			case EScreenshotRotation::EScreenshotRotation_ROTATE90:		rotationInt = 270; break;
			case EScreenshotRotation::EScreenshotRotation_ROTATE180:	rotationInt = 180; break;
			case EScreenshotRotation::EScreenshotRotation_ROTATE270:	rotationInt = 90; break;
		}
		recordingRotation = rotationInt;

		__block UIImage *overlayImage = RaveMobilePhotoGallery::UIImageFrom(OverlayTexture, 1.0f, true);
		[overlayImage retain];

		[[IOSReplayKit GetDelegate] startRecording:this
			overlayImage : overlayImage
			rotation : rotationInt];
		[overlayImage autorelease];
		overlayImage = nil;

#endif

#if PLATFORM_ANDROID
	if (AndroidThunkCpp_IsScreenCapturing())
	{
		IsFailure(EScreenRecordingErrors::EScreenRecordingIsRecording);
		return;
	}

	static FDelegateHandle onStartRecordingDelegate;
	static uint bRenderNextFrame = 0;
	onStartRecordingDelegate = AndroidStartScreenRecordingResponseDelegate.AddLambda(
		[this](int bSuccessInt, EScreenRecordingErrors errorMsg)
	{
		bool bSuccess = (bSuccessInt == 1);
		if (bSuccess)
		{
			IsSuccess();
		}
		else
		{
			IsFailure(errorMsg);
		}
		AndroidStartScreenRecordingResponseDelegate.Remove(onStartRecordingDelegate);
		onStartRecordingDelegate.Reset();
	});

	bool bUseFBO = (AndroidRecordingMethod == EScreenRecordingAndroidMethod_FrameBufferObject); 
	jobject overlayBitmap = NULL;

	if (bUseFBO)
	{
		overlayBitmap = RaveMobilePhotoGallery::AndroidBitmapFrom(OverlayTexture, 1.0f, true);
		renderedFrameCount = 0;
		AndroidOnScreenshotDelegate = FSlateApplication::Get().GetRenderer()->OnSlateWindowRendered().AddLambda([this](SWindow& SlateWindow, void* ViewportRHI)
		{
			if (!bIsRenderingSurface && bHasRenderSurface && renderEGLDisplay != EGL_NO_DISPLAY)
			{
				if (renderedFrameCount >= 2)
				{
					bIsRenderingSurface = true;
					startRecordingTime = FDateTime::UtcNow();

					ENQUEUE_UNIQUE_RENDER_COMMAND(FMobileScreenRecordingRenderSurface,
					{
						renderUE4Surface = AndroidEGL::GetInstance()->GetRenderingContext()->eglSurface;
						renderEGLDisplay = AndroidEGL::GetInstance()->GetDisplay();
						renderEGLContext = AndroidEGL::GetInstance()->GetCurrentContext();

						if (renderEGLSurface == EGL_NO_SURFACE)
						{
							UE_LOG(LogTemp, Log, TEXT("Start Recording Error: renderEGLSurface == EGL_NO_SURFACE"));
						}
						if (renderUE4Surface == EGL_NO_SURFACE)
						{
							UE_LOG(LogTemp, Log, TEXT("Start Recording Error: renderUE4Surface == EGL_NO_SURFACE"));
						}
						if (renderEGLContext == EGL_NO_CONTEXT)
						{
							UE_LOG(LogTemp, Log, TEXT("Start Recording Error: renderEGLContext == EGL_NO_CONTEXT"));
						}
						if (renderEGLDisplay == EGL_NO_DISPLAY)
						{
							UE_LOG(LogTemp, Log, TEXT("Start Recording Error: renderEGLDisplay == EGL_NO_DISPLAY"));
						}

						FTimespan timeLapsed = FDateTime::UtcNow() - startRecordingTime;
						float timeLapsedMsec = timeLapsed.GetTotalMilliseconds();
						long timeLapsedMsecLong = (long)timeLapsedMsec;

						eglPresentationTimeANDROID_(renderEGLDisplay, renderEGLSurface, timeLapsedMsecLong);
						eglMakeCurrent(renderEGLDisplay, renderEGLSurface, renderUE4Surface, renderEGLContext);
						AndroidThunkCpp_RenderFrame(mOffscreenTexture, mWindowSurfaceWidth, mWindowSurfaceHeight);
						eglSwapBuffers(renderEGLDisplay, renderEGLSurface);
						glFlush();
						eglMakeCurrent(renderEGLDisplay, renderUE4Surface, renderUE4Surface, renderEGLContext);
						bIsRenderingSurface = false;
					}
					);
				}
				renderedFrameCount++;
			}
		});
	}

	uint32 outWidth = 0;
	uint32 outHeight = 0;
	AndroidEGL::GetInstance()->GetDimensions(outWidth, outHeight);
	mWindowSurfaceWidth = outWidth;
	mWindowSurfaceHeight = outHeight;

	uint32 rotationInt = 0;
	switch (Rotation)
	{
		case EScreenshotRotation::EScreenshotRotation_ROTATE0:	rotationInt = 0; break;
		case EScreenshotRotation::EScreenshotRotation_ROTATE90:	rotationInt = 270; break;
		case EScreenshotRotation::EScreenshotRotation_ROTATE180:	rotationInt = 180; break;
		case EScreenshotRotation::EScreenshotRotation_ROTATE270:	rotationInt = 90; break;
	}
	recordingRotation = rotationInt;
	bool bLockOrientation = false;

	FString filePath = RaveMobilePhotoGallery::GetOutputFolderPath(RaveMobilePhotoGallery::GetTimestampedFilename(".mp4","",""), bAndroidSaveToExternal, SaveFolder);
	EnsureFileFolderExists(filePath);

	StartRecordingData dataRecording;
	dataRecording.filePath = filePath;
	dataRecording.outWidth = outWidth;
	dataRecording.outHeight = outHeight;
	dataRecording.rotationInt = rotationInt;
	dataRecording.bUseFBO = bUseFBO;
	dataRecording.bLockOrientation = bLockOrientation;
	dataRecording.OffsetWidthHeight = OffsetWidthHeight;
	lastRecordingData = dataRecording;

	if (UAndroidPermissionFunctionLibrary::CheckPermission("android.permission.WRITE_EXTERNAL_STORAGE") &&
		UAndroidPermissionFunctionLibrary::CheckPermission("android.permission.RECORD_AUDIO"))
	{
		AndroidThunkCpp_StartScreenCapture(dataRecording.filePath, nullptr,
			dataRecording.outWidth, dataRecording.outHeight, dataRecording.rotationInt,
			dataRecording.bUseFBO, dataRecording.bLockOrientation, dataRecording.OffsetWidthHeight.X, dataRecording.OffsetWidthHeight.Y,
			bIsSaveToGallery);
	}
	else
	{
		TArray<FString> permissions = { "android.permission.WRITE_EXTERNAL_STORAGE", "android.permission.RECORD_AUDIO" };
		UAndroidPermissionCallbackProxy *permissionCallback = UAndroidPermissionFunctionLibrary::AcquirePermissions(permissions);
		permissionCallback->OnPermissionsGrantedDelegate.BindUObject(this, &UAsyncStartRecordingTask::DoOnAndroidPermissionGranted);
	}
	return;
#endif

#if PLATFORM_WINDOWS || PLATFORM_MAC
	SimulatedIsRecording = true;
	IsSuccess();
#endif

	
}

void UAsyncStartRecordingTask::DoStartRecordingWithOverlayWidget(const UObject* WorldContextObject,
	bool bSaveToScreenshots, bool bSaveToGallery,
	UUserWidget* OverlayWidget,
	EScreenshotRotation Rotation, FVector2D OffsetWidthHeight,
	FString SaveFolder, bool bAndroidSaveToExternal, EScreenRecordingAndroidMethod AndroidRecordingMethod)
{

	if (OverlayWidget)
	{
		UTextureRenderTarget2D *WidgetTexture = NULL;
		FVector2D ViewportSize;
		GEngine->GameViewport->GetViewportSize(ViewportSize);
		TSharedPtr<FWidgetRenderer, ESPMode::ThreadSafe> WidgetRenderer = MakeShareable(new FWidgetRenderer());

		WidgetTexture = WidgetRenderer->DrawWidget(OverlayWidget->TakeWidget(), ViewportSize);
		WidgetTexture->UpdateResourceImmediate(false);
		DoStartRecordingWithOverlayTexture(WorldContextObject, bSaveToScreenshots, bSaveToGallery, WidgetTexture, Rotation, OffsetWidthHeight, SaveFolder, bAndroidSaveToExternal, AndroidRecordingMethod);
	}
	else
	{
		DoStartRecordingWithOverlayTexture(WorldContextObject, bSaveToScreenshots, bSaveToGallery, NULL, Rotation, OffsetWidthHeight, SaveFolder, bAndroidSaveToExternal, AndroidRecordingMethod);
	}
}


void UAsyncStartRecordingTask::DoStartRecording(const UObject* WorldContextObject,
	bool bSaveToScreenshots, bool bSaveToGallery,
	EScreenshotRotation Rotation, FVector2D OffsetWidthHeight,
	FString SaveFolder, bool bAndroidSaveToExternal, EScreenRecordingAndroidMethod AndroidRecordingMethod)
{
	DoStartRecordingWithOverlayTexture(WorldContextObject, bSaveToScreenshots, bSaveToGallery, NULL, Rotation, OffsetWidthHeight, SaveFolder, bAndroidSaveToExternal, AndroidRecordingMethod);
}

// ----- STOP RECORDING ----

UAsyncStopRecordingTask::UAsyncStopRecordingTask(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	if (HasAnyFlags(RF_ClassDefaultObject) == false)
	{
		AddToRoot();
	}
	bHasResults = false;
	bIsActivated = false;
	bHasResponse = false;
}

void UAsyncStopRecordingTask::Activate()
{
	Super::Activate();
	bIsActivated = true;
	if (bHasResults)
	{
		if (bIsSuccess) IsSuccess(lastTexture, lastFilePath);
		else IsFailure(lastError);
	}
}

void UAsyncStopRecordingTask::BeginDestroy()
{
	lastTexture = nullptr;
	useMediaPlayer = nullptr;
	Super::BeginDestroy();
}

void UAsyncStopRecordingTask::IsSuccess(UMediaTexture *Texture, FString filePath)
{
	if (bHasResponse) return;

	bHasResults = true;
	bIsSuccess = true;
	lastTexture = Texture;
	lastFilePath = filePath;

	if (bIsActivated)
	{
		AsyncTask(ENamedThreads::GameThread, [this]() {
			FScreenRecordingMediaInfo mediaInfo;
			mediaInfo.Width = recordingWidth;
			mediaInfo.Height = recordingHeight;
			mediaInfo.Rotation = recordingRotation;
			mediaInfo.FilePath = lastFilePath;
			OnSuccess.Broadcast(lastTexture, mediaInfo, EScreenRecordingSuccess);
			RemoveFromRoot();
			SetReadyToDestroy();
		});
		bHasResponse = true;
	}
}

void UAsyncStopRecordingTask::IsSuccess(FString mediaPath, FString filePath)
{
	if (bHasResponse) return;
	AsyncTask(ENamedThreads::GameThread, [this, mediaPath, filePath]() {
		UMediaPlayer *mediaPlayer = useMediaPlayer;
		if (!mediaPlayer)
		{
			//mediaPlayer = NewObject<UMediaPlayer>();
			IsSuccess(nullptr, filePath);
			return;
		}
		
		if (mediaPlayer->OpenUrl(mediaPath))
		{
			UMediaTexture *mediaTexture = NewObject<UMediaTexture>();
			mediaTexture->SetMediaPlayer(mediaPlayer);
			mediaTexture->UpdateResource();
			if (bShouldAutoplay) mediaPlayer->Play();
			IsSuccess(mediaTexture, filePath);
		}
		else
		{
			IsFailure(EScreenRecordingErrors::EScreenRecordingIncompatibleMovieFile);
		}
	});
}


void UAsyncStopRecordingTask::IsFailure(EScreenRecordingErrors errorCode)
{
	if (bHasResponse) return;
	bHasResults = true;
	bIsSuccess = false;
	lastError = errorCode;
	if (bIsActivated)
	{
		AsyncTask(ENamedThreads::GameThread, [this]() {
			FScreenRecordingMediaInfo mediaInfo;
			mediaInfo.Width = recordingWidth;
			mediaInfo.Height = recordingHeight;
			mediaInfo.Rotation = recordingRotation;

			OnFailure.Broadcast(nullptr, mediaInfo, lastError);
			RemoveFromRoot();
			SetReadyToDestroy();
		});
		bHasResponse = true;
	}
}

void UAsyncStopRecordingTask::IsSaving()
{
	if (bIsActivated)
	{
		AsyncTask(ENamedThreads::GameThread, [this]() {
			FScreenRecordingMediaInfo mediaInfo;
			mediaInfo.Width = recordingWidth;
			mediaInfo.Height = recordingHeight;
			mediaInfo.Rotation = recordingRotation;
			OnSaving.Broadcast(nullptr, mediaInfo, EScreenRecordingSuccess);
		});
	}
}

UAsyncStopRecordingTask* UAsyncStopRecordingTask::StopRecording(const UObject* WorldContextObject,
	bool bGetTexture,
	UMediaPlayer *mediaPlayer, bool bAutoPlay)
{
	UAsyncStopRecordingTask* ScreenshotTask = NewObject<UAsyncStopRecordingTask>();
	ScreenshotTask->DoStopRecording(WorldContextObject, bGetTexture, mediaPlayer, bAutoPlay);
	return ScreenshotTask;
}

void UAsyncStopRecordingTask::DoStopRecording(const UObject* WorldContextObject,
	bool bGetTexture,
	UMediaPlayer *mediaPlayer, bool bAutoPlay)
{
	useMediaPlayer = mediaPlayer;
	bool bSaveRecording = bIsSaveToScreenshots;
	bool bSaveToGallery = bIsSaveToGallery;
	bShouldAutoplay = bAutoPlay;

#if PLATFORM_IOS
	[[IOSReplayKit GetDelegate] stopRecording:this
		saveRecording: bSaveRecording
		saveToGallery: bSaveToGallery
		getTexture: bGetTexture];
	return;
#endif

#if PLATFORM_ANDROID
	if (AndroidOnScreenshotDelegate.IsValid())
	{
		FSlateApplication::Get().GetRenderer()->OnSlateWindowRendered().Remove(AndroidOnScreenshotDelegate);
		AndroidOnScreenshotDelegate.Reset();
		hasEncoderImageBuffer = false;
	}

	static FDelegateHandle onStopRecordingDelegate;
	onStopRecordingDelegate = AndroidStopScreenRecordingResponseDelegate.AddLambda(
		[this](int bSuccessInt, EScreenRecordingErrors errorMsg)
	{
		bool bSuccess = (bSuccessInt == 1);
		if (bSuccessInt == 2)
		{
			IsSaving();
		}
		else
		{
			if (bSuccess)
			{
				FString destPathUrl = "file://" + currentFilePath;
				IsSuccess(destPathUrl, currentFilePath);
			}
			else
			{
				IsFailure(errorMsg);
			}
			AndroidStopScreenRecordingResponseDelegate.Remove(onStopRecordingDelegate);
			onStopRecordingDelegate.Reset();
		}
	});

	AndroidThunkCpp_StopScreenCapture();
	return;
#endif

#if PLATFORM_WINDOWS || PLATFORM_MAC
	SimulatedIsRecording = false;
	IsSuccess(nullptr);
#endif
}

bool UAsyncStartRecordingTask::CheckScreenRecordingPermissions()
{
#if PLATFORM_IOS
	return[[IOSReplayKit GetDelegate] checkPermissions];
#endif

	return false;
}