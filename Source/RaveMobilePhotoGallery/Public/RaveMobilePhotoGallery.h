// Copyright 2018 Impreszions Solutions All Rights Reserved.
// Author : MunHon Chan

#pragma once

#include "Modules/ModuleManager.h"

class FRaveMobilePhotoGalleryModule : public IModuleInterface
{
public:

	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;
};
