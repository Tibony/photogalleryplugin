// Copyright 2018 Impreszions Solutions All Rights Reserved.
// Author : MunHon Chan

#include "Screenshot.h"
#include "UnrealClient.h"
#include "HighResScreenshot.h"
#include "ImageUtils.h"
#include "Engine/Engine.h"
#include "Engine/GameViewportClient.h"
#include "SocialMedia.h"

//~~~ Image Wrapper ~~~
#include "ImageUtils.h"
#include "IImageWrapper.h"
#include "IImageWrapperModule.h"
//~~~ Image Wrapper ~~~

namespace RaveMobilePhotoGallery
{
	// Generate a filename based on the current time
	extern FString GetTimestampedFilename(FString extension, FString prefix, FString suffix);

	// Generate a Texture2DDynamic from a FColor array
	extern UTexture2DDynamic *Texture2DFrom(const TArray<FColor> &RawColorArray, int32 textureWidth, int32 textureHeight, bool bUseSRGB, bool bIsRGB, int rotation);

	// Draw FColor array to a Texture2D
	extern TArray<FColor> drawTexture(const TArray<FColor>& source, int32 sourceWidth, int32 sourceHeight,
		UTexture2D *texture, FVector2D size, FVector2D offset, FVector2D anchor, FVector2D alignment, float opacity);

	// Get Texture2D from an image file on disk
	extern UTexture2D* Texture2DFromFile(const FString& ImagePath, bool& IsValid, int32& OutWidth, int32& OutHeight);

	// Get Texture2D from an image file on disk
	extern UTexture2D* Texture2DFromFile(TSharedPtr<IImageWrapper> ImageWrapper, const FString& ImagePath, bool& IsValid, int32& OutWidth, int32& OutHeight);

	// Rotate contents of a FColor array
	extern void RotatePixelArray(TArray<FColor> &bitmap, int &width, int &height, int rotation);

	// Get Texture2DDynamic from an image file on disk
	extern UTexture2DDynamic* Texture2DFromFileDynamic(const FString& ImagePath, bool& IsValid, int32& OutWidth, int32& OutHeight);

	// Get an output folder to save most contents to
	extern FString GetOutputFolderPath(FString fileName, bool bAndroidSaveToExternal, FString AndroidSaveFolder);

	// Creates the folder that will contain the file if it does not currently exist
	extern bool EnsureFileFolderExists(FString File);

	// Convert relative path to full
	extern FString ConvertRelativePathToFull(FString Path);

#if PLATFORM_IOS
	// Generate UIImage from Texture2D
	extern UIImage *UIImageFrom(UTexture2D *texture, float opacity, bool premultiplyAlpha);

	// Generate UIImage from FColor array
	extern UIImage *UIImageFrom(const TArray<FColor> &RawColorArray, int32 textureWidth, int32 textureHeight, bool premultiplyAlpha);

	// Gets the full path for IOS
	extern FString ConvertToIOSPath(const FString& Filename, bool bForWrite);
#endif

#if PLATFORM_ANDROID
	// Save byte array to a file (Android version)
	extern bool AndroidSaveArrayToFile(TArrayView<const uint8> Array, FString dest);

	// Return the temporary directory for Android
	extern FString AndroidTemporaryDirectory();
#endif

}
using namespace RaveMobilePhotoGallery;

#if PLATFORM_ANDROID
#include "HAL/FileManager.h"
#include "AndroidJNI.h"
#include <jni.h>
#include "AndroidPermissionFunctionLibrary.h"
#include "AndroidPermissionCallbackProxy.h"

// JNI method to save image to the gallery
static jmethodID AndroidThunkJava_SaveImageToGallery;

// Perform startup functions for the screenshot function
int SetupJNI_RaveMobilePhotoGallery_Screenshot()
{
	JNIEnv* env = FAndroidApplication::GetJavaEnv();
	if (!env) return JNI_ERR;
	JNIEnv* ENV = env;

	UE_LOG(LogTemp, Log, TEXT("CHECKING: RaveMobilePhotoGallery_SaveImageToGallery"));
	AndroidThunkJava_SaveImageToGallery = FJavaWrapper::FindMethod(ENV, FJavaWrapper::GameActivityClassID, "RaveMobilePhotoGallery_SaveImageToGallery", "(Ljava/lang/String;)Z", false);
	if (!AndroidThunkJava_SaveImageToGallery)
	{
		UE_LOG(LogTemp, Log, TEXT("ERROR: RaveMobilePhotoGallery_SaveImageToGallery Not Found"));
		//return JNI_ERR;
	}

	return JNI_OK;
}

// JNI method to save image to the gallery
bool AndroidThunkCpp_SaveImageToGallery(FString imagePath)
{
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring Argument = Env->NewStringUTF(TCHAR_TO_UTF8(*imagePath));
		return FJavaWrapper::CallBooleanMethod(Env, FJavaWrapper::GameActivityThis, AndroidThunkJava_SaveImageToGallery, Argument);
	}
	return false;
}

#endif

#if PLATFORM_IOS
#import <UIKit/UIKit.h>
#include "IOSAppDelegate.h"
#import <Foundation/Foundation.h>

@interface IOSScreenshot : UIViewController <UINavigationControllerDelegate>
@end
 
@implementation IOSScreenshot
 
+ (IOSScreenshot*)GetDelegate
{
    static IOSScreenshot* Singleton = [[IOSScreenshot alloc] init];
    return Singleton;
}

-(void)image:(UIImage *)image
didFinishSavingWithError : (NSError *)error
	contextInfo : (void *)contextInfo
{
}
 
-(void)saveScreenshot : (UAsyncScreenshotTask *)callback
	image:(UIImage *)sourceimg
	watermark : (UIImage *)watermark
	watermarkdata : (FScreenshotWatermarkTexture)watermarkdata
	returnTexture:(UTexture2DDynamic *)returnTex
{
	[watermark retain];
	[sourceimg retain];
	UIImage *image = sourceimg;

	if (image)
	{
		CGFloat width = image.size.width;
		CGFloat height = image.size.height;

		if ((width > 0) && (height > 0))
		{
			if (watermark)
			{
				CGFloat w = watermarkdata.size.X; // (size.X <= 0) ? watermark.size.width : size.X;
				CGFloat h = watermarkdata.size.Y; // (size.Y <= 0) ? watermark.size.height : size.Y;

				if (w <= 0 && h <= 0)
				{
					w = watermark.size.width;
					h = watermark.size.height;
				}
				else if (w <= 0)
				{
					h = watermark.size.height;
					w = (h / watermark.size.height) * watermark.size.width;
				}
				else if (h <= 0)
				{
					w = watermark.size.width;
					h = (w / watermark.size.width) * watermark.size.height;
				}

				CGFloat x = watermarkdata.offset.X + watermarkdata.anchor.X * width - watermarkdata.alignment.X * w;
				CGFloat y = watermarkdata.offset.Y + watermarkdata.anchor.Y * height - watermarkdata.alignment.Y * h;

				UIGraphicsBeginImageContextWithOptions(CGSizeMake(width, height), false, 0.0f);
				[image drawInRect : CGRectMake(0, 0, width, height)];
				[watermark drawInRect : CGRectMake(x, y, w, h) blendMode : kCGBlendModeNormal alpha : watermarkdata.opacity];
				image = UIGraphicsGetImageFromCurrentImageContext();
				UIGraphicsEndImageContext();
			}

			if (image)
			{
				UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
				callback->IsSuccess(returnTex);
			}
			else
			{
				callback->IsFailure(returnTex, EScreenshotErrors::EScreenshotFailedToGenerateScreenshot);
			}
		}
		else // width/height = 0
		{
			callback->IsFailure(NULL, EScreenshotErrors::EScreenshotInvalidSize);
		}
	}
	else
	{
		callback->IsFailure(NULL, EScreenshotErrors::EScreenshotFailedToGenerateScreenshot);
	}

	[sourceimg release];
	[watermark release];
}
 
@end
#endif

#if PLATFORM_ANDROID
static FScreenshotWatermarkTexture lastWatermark;
static bool bLastShowUI = false;
static bool bLastSaveScreenshot = false;
static FString lastSaveFolder;
static EScreenshotRotation lastRotation;
static bool bLastAddToGallery = false;
static bool bLastAndroidSaveToExternal = false;
static bool bLastShareToSocialMedia = false;
static FString lastShareCaption = "";
static const UObject* lastWorldContextObject = nullptr;
#endif


UAsyncScreenshotTask::UAsyncScreenshotTask(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	if (HasAnyFlags(RF_ClassDefaultObject) == false)
	{
		AddToRoot();
	}
}

void UAsyncScreenshotTask::Activate()
{
	Super::Activate();
	bIsActivated = true;
	if (bHasResults)
	{
		if (bIsSuccess) IsSuccess(lastTexture, lastFilepath);
		else IsFailure(lastTexture, lastErrorMessage);
	}
}

void UAsyncScreenshotTask::IsSuccess(UTexture2DDynamic *texture, FString filePath)
{
	bHasResults = true;
	bIsSuccess = true;
	if (bIsActivated)
	{
		FScreenshotMediaInfo mediaInfo;
		mediaInfo.Width = texture->SizeX;
		mediaInfo.Height = texture->SizeY;
		mediaInfo.FilePath = filePath;

		OnSuccess.Broadcast(texture, mediaInfo, EScreenshotErrors::EScreenshotSuccess);
		RemoveFromRoot();
		SetReadyToDestroy();
		lastTexture = NULL;
#if PLATFORM_ANDROID
		lastWorldContextObject = nullptr;
#endif

	}
	else
	{
		lastTexture = texture;
		lastFilepath = filePath;
	}
}

void UAsyncScreenshotTask::IsFailure(UTexture2DDynamic *texture, EScreenshotErrors errorMessage)
{
	bHasResults = true;
	bIsSuccess = false;
	if (bIsActivated)
	{
		FScreenshotMediaInfo mediaInfo;

		OnFailure.Broadcast(texture, mediaInfo, errorMessage);
		RemoveFromRoot();
		SetReadyToDestroy();
		lastTexture = NULL;
#if PLATFORM_ANDROID
		lastWorldContextObject = nullptr;
#endif
	}
	else
	{
		lastTexture = texture;
		lastErrorMessage = errorMessage;
	}
}

UAsyncScreenshotTask* UAsyncScreenshotTask::GetScreenshot(const UObject* WorldContextObject, 
	bool bShowUI,
	bool bSaveScreenshot, bool bAddToGallery,
	FString SaveFolder, EScreenshotRotation Rotation,
	bool bAndroidSaveToExternal)
{
	FScreenshotWatermarkTexture watermark;
	watermark.texture = NULL;
	UAsyncScreenshotTask* ScreenshotTask = NewObject<UAsyncScreenshotTask>();
	ScreenshotTask->DoSaveScreenshot(WorldContextObject, watermark, 
		bShowUI, bSaveScreenshot, SaveFolder, Rotation,
		bAddToGallery, bAndroidSaveToExternal);
	return ScreenshotTask;
}

UAsyncScreenshotTask* UAsyncScreenshotTask::GetScreenshotWithWatermark(const UObject* WorldContextObject, 
	FScreenshotWatermarkTexture watermark,
	bool bShowUI, bool bSaveScreenshot, bool bAddToGallery,
	FString SaveFolder, EScreenshotRotation Rotation,
	bool bAndroidSaveToExternal)
{
	UAsyncScreenshotTask* ScreenshotTask = NewObject<UAsyncScreenshotTask>();
	ScreenshotTask->DoSaveScreenshot(WorldContextObject, watermark,
		bShowUI, bSaveScreenshot, SaveFolder, Rotation,
		bAddToGallery, bAndroidSaveToExternal);
	return ScreenshotTask;
}

void UAsyncScreenshotTask::DoSaveScreenshot(const UObject* WorldContextObject, FScreenshotWatermarkTexture watermark,
	bool bShowUI, bool bSaveScreenshot, FString SaveFolder,
	EScreenshotRotation Rotation,
	bool bAddToGallery, bool bAndroidSaveToExternal)
{
	bool bShareToSocialMedia = false;
	FString ShareCaption = "";

#if PLATFORM_ANDROID
	if ( !UAndroidPermissionFunctionLibrary::CheckPermission("android.permission.WRITE_EXTERNAL_STORAGE"))
	{
		lastWatermark = watermark;
		bLastShowUI = bShowUI;
		bLastSaveScreenshot = bSaveScreenshot;
		lastSaveFolder = SaveFolder;
		lastRotation = Rotation;
		bLastAddToGallery = bAddToGallery;
		bLastAndroidSaveToExternal = bAndroidSaveToExternal;
		bLastShareToSocialMedia = bShareToSocialMedia;
		lastShareCaption = ShareCaption;
		lastWorldContextObject = WorldContextObject;

		TArray<FString> permissions = { "android.permission.WRITE_EXTERNAL_STORAGE" };
		UAndroidPermissionCallbackProxy *permissionCallback = UAndroidPermissionFunctionLibrary::AcquirePermissions(permissions);
		permissionCallback->OnPermissionsGrantedDelegate.BindUObject(this, &UAsyncScreenshotTask::DoOnAndroidPermissionGranted);
		return;
	}
#endif

	bool bHideUI = !bShowUI;
	FString filename = GetTimestampedFilename(".png","","");
	FScreenshotRequest::RequestScreenshot(filename, !bHideUI, false);

#if PLATFORM_IOS
	UIImage *watermarkImage = nil;
#else
	int watermarkImage = 0;
#endif

	bool bReturnTexture2D = true;
	static FDelegateHandle onScreenshotDelegate;
	onScreenshotDelegate = GEngine->GameViewport->OnScreenshotCaptured().AddLambda(
		[this, watermark, bSaveScreenshot, bReturnTexture2D, bAndroidSaveToExternal, watermarkImage, Rotation, SaveFolder, bAddToGallery,
		bShareToSocialMedia, ShareCaption, WorldContextObject]
	(int32 width, int32 height, const TArray<FColor>&bitmap1)
	{
		if (width > 0 && height > 0 && bitmap1.Num() > 0)
		{
			UTexture2DDynamic *returnTex = NULL;
			TArray<FColor> bitmap(bitmap1);

			if (watermark.texture)
			{
				bitmap = drawTexture(bitmap1, width, height,
					watermark.texture, watermark.size, watermark.offset, watermark.anchor, watermark.alignment, watermark.opacity);
			}

			int rotationInt = 0;
			switch (Rotation)
			{
				case EScreenshotRotation::EScreenshotRotation_ROTATE0:		rotationInt = 0; break;
				case EScreenshotRotation::EScreenshotRotation_ROTATE90:		rotationInt = 90; break;
				case EScreenshotRotation::EScreenshotRotation_ROTATE180:	rotationInt = 180; break;
				case EScreenshotRotation::EScreenshotRotation_ROTATE270:	rotationInt = 270; break;
			}
			RotatePixelArray(bitmap, width, height, rotationInt);
			if (bShareToSocialMedia || bReturnTexture2D) returnTex = Texture2DFrom(bitmap, width, height, true, false, 0);

#if PLATFORM_IOS
			FString ScreenShotPath = "";

			FString ScreenShotName = SaveFolder / FPaths::GetBaseFilename(FScreenshotRequest::GetFilename(), true);
			if (FPaths::GetExtension(ScreenShotName).IsEmpty()) ScreenShotName += TEXT(".png");
			EnsureFileFolderExists(ScreenShotName);
			UE_LOG(LogTemp, Log, TEXT("Saving Screenshot to gallery: %s"), *ScreenShotName);

			TArray<uint8> CompressedBitmap;
			FImageUtils::CompressImageArray(width, height, bitmap, CompressedBitmap);
			FFileHelper::SaveArrayToFile(CompressedBitmap, *ScreenShotName);
			ScreenShotPath = RaveMobilePhotoGallery::ConvertToIOSPath(ScreenShotName, true);

			if ( !bSaveScreenshot )
			{
				NSString *tempPath = NSTemporaryDirectory();
				FString destPathStr = IOS_TO_UE_STRING(tempPath);
				destPathStr = destPathStr / "OnDemandResources";
				NSString *dstPath = UE_TO_IOS_STRING(destPathStr);
				bool bCreateDirectory = [[NSFileManager defaultManager] createDirectoryAtPath:dstPath
					withIntermediateDirectories : YES
					attributes : nil
					error : nil];
				NSLog(@"Created Directory : %@ -> %@", dstPath, bCreateDirectory ? @"YES" : @"NO");

				FString TempScreenShotName = destPathStr / FPaths::GetBaseFilename(ScreenShotName, true);
				NSString *srcFile = UE_TO_IOS_STRING(ScreenShotPath);
				NSString *destFile = UE_TO_IOS_STRING(TempScreenShotName);
				bool bCopyItem = [[NSFileManager defaultManager] moveItemAtPath:srcFile toPath:destFile error : nil];
				NSLog(@"Moved File : %@ -> %@ = %@", srcFile, destFile, bCopyItem ? @"YES" : @"NO");
				UE_LOG(LogTemp, Log, TEXT("Saving Screenshot to gallery: %s"), *ScreenShotName);
				ScreenShotPath = TempScreenShotName;
			}

			if (bAddToGallery)
			{
				UIImage *image = RaveMobilePhotoGallery::UIImageFrom(bitmap, width, height, true);
				[image retain];
				UIImageWriteToSavedPhotosAlbum(image, [IOSScreenshot GetDelegate], @selector(image:didFinishSavingWithError:contextInfo:), nil);
				//[[IOSScreenshot GetDelegate] saveScreenshot:this image : image watermark : watermarkImage watermarkdata : watermark returnTexture : returnTex];
				[image release];
			}

			IsSuccess(returnTex, ScreenShotPath);

#elif PLATFORM_ANDROID

			//if (bSaveScreenshot || bAddToGallery || bShareToSocialMedia)
			if ( true )
			{
				FString ScreenShotName = FScreenshotRequest::GetFilename();
				bool results = false;
				FString storagePath = GetOutputFolderPath(ScreenShotName, bAndroidSaveToExternal, SaveFolder);
				if (!bSaveScreenshot && !bAddToGallery)
				{
					storagePath = AndroidTemporaryDirectory() / FPaths::GetCleanFilename(ScreenShotName);
				}
				RaveMobilePhotoGallery::EnsureFileFolderExists(storagePath);

				TArray<uint8> CompressedBitmap;
				FImageUtils::CompressImageArray(width, height, bitmap, CompressedBitmap);
				results = RaveMobilePhotoGallery::AndroidSaveArrayToFile(CompressedBitmap, storagePath);
				UE_LOG(LogTemp, Log, TEXT("Saving Screenshot to gallery: %s"), *storagePath);

				if (results)
				{
					if (bAddToGallery)
					{
						results = AndroidThunkCpp_SaveImageToGallery(storagePath);
					}
				}

				if (results)
				{
					IsSuccess(returnTex, storagePath);
				}
				else
				{
					IsFailure(returnTex, EScreenshotErrors::EScreenshotFailedToAddToGallery);
				}
			}
			else
			{
				IsSuccess(returnTex, "");
			}

#else // OTHER_PLATFORMS
			FString ScreenShotName = "";
			if (bSaveScreenshot)
			{
				ScreenShotName = FScreenshotRequest::GetFilename();

				if (!FPaths::GetExtension(ScreenShotName).IsEmpty())
				{
					ScreenShotName = FPaths::GetBaseFilename(ScreenShotName, false);
					ScreenShotName += TEXT(".png");
				}

				ScreenShotName = SaveFolder / ScreenShotName;
				RaveMobilePhotoGallery::EnsureFileFolderExists(ScreenShotName);
				UE_LOG(LogTemp, Log, TEXT("Saving Screenshot to gallery: %s"), *ScreenShotName);

				// Save the contents of the array to a png file.
				TArray<uint8> CompressedBitmap;
				FImageUtils::CompressImageArray(width, height, bitmap, CompressedBitmap);
				FFileHelper::SaveArrayToFile(CompressedBitmap, *ScreenShotName);
			}
			IsSuccess(returnTex, ScreenShotName);
#endif

		}
		else
		{
			IsFailure(NULL, EScreenshotErrors::EScreenshotFailedToGenerateScreenshot);
		}
		GEngine->GameViewport->OnScreenshotCaptured().Remove(onScreenshotDelegate);
		onScreenshotDelegate.Reset();
	});
}

void UAsyncScreenshotTask::DoOnAndroidPermissionGranted(const TArray<FString>& Permissions, const TArray<bool>& GrantResults)
{
#if PLATFORM_ANDROID
	if (GrantResults.Num() > 0 && GrantResults[0])
	{
		DoSaveScreenshot(lastWorldContextObject, lastWatermark, bLastShowUI, bLastSaveScreenshot,
			lastSaveFolder, lastRotation, bLastAddToGallery, bLastAndroidSaveToExternal);
	}
	else
	{
		IsFailure(NULL, EScreenshotErrors::EScreenshotNoPermission);
	}
#endif
}