// Copyright 2018 Impreszions Solutions All Rights Reserved.
// Author : MunHon Chan

#pragma once

#include "RaveMobilePhotoGalleryEnum.h"
#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "Screenshot.generated.h"

// Information regarding the returned media from the photo gallery
USTRUCT(BlueprintType, Category = "Photo Gallery")
struct RAVEMOBILEPHOTOGALLERY_API FScreenshotMediaInfo
{
	GENERATED_BODY()

	// The width of the media returned by the photo gallery.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Photo Gallery")
		int Width = 0;

	// The height of the media returned by the photo gallery.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Photo Gallery")
		int Height = 0;

	// The full file path to the screenshot
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Photo Gallery")
		FString FilePath = "";
};

// List of possible errors for screen recording operations by the Rave Media Gallery plugin.
UENUM(Blueprintable)
enum EScreenshotErrors
{
	EScreenshotSuccess UMETA(DisplayName = "Success"),
	EScreenshotUnknownError UMETA(DisplayName = "Unknown Error"),
	EScreenshotFailedToGenerateScreenshot UMETA(DisplayName = "Failed to generate screenshot"),
	EScreenshotFailedToAddToGallery UMETA(DisplayName = "Failed to add screenshot to gallery"),
	EScreenshotInvalidSize UMETA(DisplayName = "Invalid screenshot width/height"),
	EScreenshotNoPermission UMETA(DisplayName = "Permission is not granted")
};


DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FScreenshotTaskDelegate, UTexture2DDynamic*, Texture, FScreenshotMediaInfo, MediaInfo, EScreenshotErrors, ErrorMessage);

// Watermark texture to be applied with the screenshot function for Rave Media Gallery
USTRUCT(BlueprintType, Category = "Photo Gallery")
struct FScreenshotWatermarkTexture
{
	GENERATED_BODY()

		// The texture to be used as watermark. Please ensure the following settings are set for the texture used:
		//	Mip Gen Settings -> NoMipmaps
		//	sRGB -> false
		//	Compression Settings->TC Vector Displacementmap
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Watermark")
		UTexture2D *texture = NULL;

	// Size of the output. Use 0 for actual image size
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Watermark")
		FVector2D size;

	// The position offset based on the size of the container (i.e. the original source image)
	// This is a range of 0..1
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Watermark")
		FVector2D anchor;
	
	// The position offset to be added to the final position.
	// This is in pixels.
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Watermark")
		FVector2D offset;

	// The position offset based on the size of the watermark
	// This is a range of 0..1 and it uses the final size output as specified with the size parameter above
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Watermark")
		FVector2D alignment;

	// The opacity multiplier
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Watermark")
		float opacity = 1;
};

// Takes a screenshot and optionally saves it to the photo gallery.
UCLASS(Category = "Photo Gallery", meta = (DisplayName = "Photo Gallery"))
class RAVEMOBILEPHOTOGALLERY_API UAsyncScreenshotTask : public UBlueprintAsyncActionBase
{
	GENERATED_UCLASS_BODY()

public:

	// Takes a screenshot and optionally saves it to the photo gallery.
	// Supported Platforms: Windows, IOS, Android
	//
	// @param bShowUI Decides if the UMG is captured.
	// @param bSaveToScreenshots Saves the screenshot to disk to the saved folder.
	// @param bSaveToGallery Saves the screenshot to the device photo gallery.	Android: Enabling this will automatically enable Save To Screenshots due to the requirement of gallery contents to be physical files.
	// @param SaveFolder Subfolder to store the screeenshot into.	IOS: {Documents}/{SaveFolder}	Android:{Project}/Saved/{SavedFolder}	Windows: {Project}/Saved/{SaveFolder}
	// @param Rotation Rotation to be applied to the screenshot
	// @param bAndroidSaveToExternal This saves the actual image file to the external storage device instead of the default saved folder.
	// This is required for the image to remain in the gallery when the application is uninstalled.
	// If enabled, screenshot will be saved to {storagedevice}/{SaveFolder}
	//
	UFUNCTION(BlueprintCallable, Category = "Photo Gallery", meta = (WorldContext = "WorldContextObject", DisplayName = "Take Screenshot", BlueprintInternalUseOnly = "true"))
		static UAsyncScreenshotTask* GetScreenshot(
			const UObject* WorldContextObject,
			bool bShowUI = true,
			bool bSaveToScreenshots = false,
			bool bSaveToGallery = false,
			FString SaveFolder = "",
			EScreenshotRotation Rotation = EScreenshotRotation::EScreenshotRotation_ROTATE0,
			bool bAndroidSaveToExternal = true);

	// Takes a screenshot and optionally saves it to the photo gallery.
	// 
	// Supported Platforms: Windows, IOS, Android
	//
	// @param watermark For Watermark Textures please ensure the following settings are set for the texture used:
	//	Mip Gen Settings -> NoMipmaps
	//	sRGB -> false
	//	Compression Settings->TC Vector Displacementmap
	// @param bShowUI Decides if the UMG is captured.
	// @param bSaveToScreenshots Saves the screenshot to disk to the saved folder.
	// @param bSaveToGallery Saves the screenshot to the device photo gallery.	Android: Enabling this will automatically enable Save To Screenshots due to the requirement of gallery contents to be physical files.
	// @param Rotation Rotation to be applied to the screenshot
	// @param SaveFolder Subfolder to store the screeenshot into.	IOS: {Documents}/{SaveFolder}	Android:{Project}/Saved/{SavedFolder}	Windows: {Project}/Saved/{SaveFolder}
	// @param bAndroidSaveToExternal This saves the actual image file to the external storage device instead of the default saved folder.
	// This is required for the image to remain in the gallery when the application is uninstalled.
	// If enabled, screenshot will be saved to {storagedevice}/{SaveFolder}
	UFUNCTION(BlueprintCallable, Category = "Photo Gallery", meta = (WorldContext = "WorldContextObject", DisplayName = "Take Screenshot with Watermark", BlueprintInternalUseOnly = "true"))
		static UAsyncScreenshotTask* GetScreenshotWithWatermark(
			const UObject* WorldContextObject,
			FScreenshotWatermarkTexture watermark, 
			bool bShowUI = true,
			bool bSaveToScreenshots = false,
			bool bSaveToGallery = false,
			FString SaveFolder = "",
			EScreenshotRotation Rotation = EScreenshotRotation::EScreenshotRotation_ROTATE0,
			bool bAndroidSaveToExternal = true);

public:
	// When screenshot is successfully taken
	//
	// [OUT] Texture: The texture of the screenshot. This is a Texture2DDynamic texture.
	UPROPERTY(BlueprintAssignable)
		FScreenshotTaskDelegate OnSuccess;

	// When screenshot failed to be taken
	//
	// [OUT] Texture: This should be empty on failure.
	UPROPERTY(BlueprintAssignable)
		FScreenshotTaskDelegate OnFailure;

	// UObject overrides
	virtual void Activate() override;
	// END UObject overrides

	// Convenience function to broadcast a success event
	void IsSuccess(UTexture2DDynamic *texture, FString filePath = "");

	// Convenience function to broadcast a failure event
	void IsFailure(UTexture2DDynamic *texture, EScreenshotErrors ErrorMessage = EScreenshotErrors::EScreenshotUnknownError);

	// Interal function
	void DoOnAndroidPermissionGranted(const TArray<FString>& Permissions, const TArray<bool>& GrantResults);

protected:
	// Tracks when the component has activated
	bool bIsActivated = false;

	// Tracks when a success/failed call has been made
	bool bHasResults = false;

	// Tracks when a success/failed call has been made
	bool bIsSuccess = false;

	// Texture to be broadcasted
	UTexture2DDynamic *lastTexture = NULL;

	// File path to be broadcasted
	FString lastFilepath = "";

	// Last error message encountered
	EScreenshotErrors lastErrorMessage = EScreenshotErrors::EScreenshotUnknownError;

	// Internal function
	void DoSaveScreenshot(const UObject* WorldContextObject,
		FScreenshotWatermarkTexture watermark,
		bool bShowUI = true,
		bool bSaveScreenshot = false, FString SaveFolder = "",
		EScreenshotRotation Rotation = EScreenshotRotation::EScreenshotRotation_ROTATE0,
		bool bAddToGallery = false, bool bAndroidSaveToExternal = true);
};
