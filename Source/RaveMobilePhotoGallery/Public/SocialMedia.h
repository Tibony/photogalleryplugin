// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Kismet/BlueprintAsyncActionBase.h"
#include "SocialMedia.generated.h"

// List of possible errors for photo gallery operations by the Rave Media Gallery plugin.
UENUM(BlueprintType)
enum ESocialMediaErrorMessages
{
	ESOCIALMEDIAERRORS_NO_ERRORS UMETA(DisplayName = "No errors"),
	ESOCIALMEDIAERRORS_NOT_INSTALLED UMETA(DisplayName = "Instragram not installed"),
	ESOCIALMEDIAERRORS_FILE_NOT_FOUND UMETA(DisplayName = "File not found"),
	ESOCIALMEDIAERRORS_UNSUPPORTED_TEXTURE_TYPE UMETA(DisplayName = "Unsupported texture type"),
	ESOCIALMEDIAERRORS_NO_PERMISSION UMETA(DisplayName = "No permission granted"),
	ESOCIALMEDIAERRORS_NO_TEXTURE_SPECIFIED UMETA(DisplayName = "No texture specified")
};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FInstagramTaskDelete, ESocialMediaErrorMessages, ErrorMessage);

UCLASS()
class UAsyncSocialMediaTask : public UBlueprintAsyncActionBase
{
	GENERATED_UCLASS_BODY()

	// Uploads an image/video file specified by path to the device's registered share destinations (i.e. Instagram, Twitter, Whatsapp)
	//
	// [OUT] Error Message:
	// Error message if the file could not be uploaded. If Successful, this should return No Errors.
	//
	// Supported platforms: IOS, Android
	//
	// @param imagePath Specifies the file path to the image to be uploaded. This must be a full file path even on mobile devices.
	// This can be obtained from the Media Info variable returned when exporting screenshots / video recordings.
	// @param caption Caption to be added to the share popup dialog if supported
	// Supported targets: Facebook, Twitter
	// Non-supported targets: Whatsapp, Instagram
	UFUNCTION(BlueprintCallable, Category = "Photo Gallery", meta = (WorldContext = "WorldContextObject", DisplayName = "Share Media File to Social Media", BlueprintInternalUseOnly = "true"))
		static UAsyncSocialMediaTask* UploadFileToSocialMedia(const UObject* WorldContextObject, FString mediaPath, FString caption = "");

public:

	// Called when media is uploaded successfully.
	// NOTE: OnSuccess is called when the device shows the dialog prompt to choose where to share the media to.
	// It does not track if the user has successfully completed the upload to the social media.
	//
	// [OUT] Error Message:
	// Error message if the file could not be uploaded. If Successful, this should return No Errors.
	UPROPERTY(BlueprintAssignable)
		FInstagramTaskDelete OnSuccess;


	// Called when media upload has failed.
	//
	// [OUT] Error Message:
	// Error message if the file could not be uploaded. If Successful, this should return No Errors.
	UPROPERTY(BlueprintAssignable)
		FInstagramTaskDelete OnFailure;

	// UObject overrides
	virtual void Activate() override;
	virtual void BeginDestroy() override;
	// END: UObject overrides

	// Convenience method to broadcast a successful event
	void IsSuccess();

	// Convenience method to broadcast a successful event
	// @param errorCode Error message to be returned.
	void IsFailure(ESocialMediaErrorMessages ErrorMessage);

	// Internal function
	bool DoUploadFileToSocialMedia(FString imagePath, FString caption = "", bool bAndroidSaveToExternal = true, FString AndroidSaveFolder = "");

	// Interal function
	void DoOnAndroidPermissionGranted(const TArray<FString>& Permissions, const TArray<bool>& GrantResults);

protected:
	// Tracks when the component has activated
	bool bIsActivated = false;

	// Tracks when a success/failed call has been made
	bool bHasResults = false;

	// Tracks when a success/failed call has been made
	bool bIsSuccess = false;

	// Tracks the last error message to be returned
	ESocialMediaErrorMessages lastErrorMsg = ESocialMediaErrorMessages::ESOCIALMEDIAERRORS_NO_ERRORS;
};
