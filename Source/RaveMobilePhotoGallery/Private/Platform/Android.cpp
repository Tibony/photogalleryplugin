// Copyright 2018 Impreszions Solutions All Rights Reserved.
// Author : MunHon Chan

#pragma once

#ifdef __ANDROID__

#include "CoreMinimal.h"
#include "Android/AndroidApplication.h"
#include "AndroidJNI.h"

#include <string.h>
#include <jni.h>
#include <stdio.h>

// Bind JNI methods for the export screenshot function
extern int SetupJNI_RaveMobilePhotoGallery_Screenshot();

// Bind JNI methods for the photo gallery import function
extern int SetupJNI_RaveMobilePhotoGallery_PhotoGallery();

// Bind JNI methods for the video recording function
extern int SetupJNI_RaveMobilePhotoGallery_ScreenRecording();

// Bind JNI methods for the social media functions
extern int SetupJNI_RaveMobilePhotoGallery_SocialMedia();


// Called during module startup for Android
int SetupJNI_RaveMobilePhotoGallery()
{
	JNIEnv* env = FAndroidApplication::GetJavaEnv();
	if (!env) return JNI_ERR;

	JNIEnv* ENV = env;

	SetupJNI_RaveMobilePhotoGallery_Screenshot();
	SetupJNI_RaveMobilePhotoGallery_PhotoGallery();
	SetupJNI_RaveMobilePhotoGallery_ScreenRecording();
	SetupJNI_RaveMobilePhotoGallery_SocialMedia();

	return JNI_OK;
}

#endif
