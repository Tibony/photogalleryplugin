// Copyright 2018 Impreszions Solutions All Rights Reserved.
// Author : MunHon Chan

#if PLATFORM_IOS

#include "CoreMinimal.h"
#include "RHIDefinitions.h"
#include "RHIResources.h"
#include "Engine/TextureRenderTarget2D.h"
#include "Engine/Texture2DDynamic.h"

#import <Foundation/Foundation.h>

namespace RaveMobilePhotoGallery
{
	// Write FColor array into a dynamic texture2D
	extern void WriteToTexture2D_RenderThread(FTexture2DDynamicResource* TextureResource, const TArray<FColor>& FColorData, bool bUseSRGB, bool bIsRGB, int rotation);

	// Write a byte array into a dynamic texture2D
	extern void WriteToTexture2D_RenderThread(FTexture2DDynamicResource* TextureResource, const TArray<uint8>& RawData, bool bUseSRGB, bool bIsRGB, int rotation);

	// Generate a dynamic texture2D from an FColor array
	extern UTexture2DDynamic *Texture2DFrom(const TArray<FColor> &FColorData, int32 width, int32 height);

	// Rotate contents of a FColor array (size_t version)
	extern void RotatePixelArray(TArray<FColor> &bitmap, size_t &width, size_t &height, int rotation);

	// Generate FColor array from an UIImage
	TArray<FColor> FColorArrayFrom(UIImage *image)
	{
		TArray<FColor> imageData;
		if (image)
		{
			size_t Width = image.size.width;
			size_t Height = image.size.height;
			size_t Area = Width * Height;
			size_t ComponentsPerPixel = 4; // rgba
			size_t NumComponents = Area * ComponentsPerPixel;

			imageData.SetNumZeroed(Width * Height);
			uint8_t *imageDataPtr = (uint8_t*)(imageData.GetData());
			if (imageDataPtr == nil)
			{
				UE_LOG(LogTemp, Log, TEXT("IOS FColorArrayFrom UIImage Error: ImageData is nil"));
				return imageData;
			}

			CGAffineTransform transform = CGAffineTransformIdentity;

			switch(image.imageOrientation)
			{
				case UIImageOrientationDown:
				case UIImageOrientationDownMirrored:
					transform = CGAffineTransformTranslate(transform, Width, Height);
					transform = CGAffineTransformRotate(transform, CGFloat(M_PI));
					break;

				case UIImageOrientationLeft:
				case UIImageOrientationLeftMirrored:
					transform = CGAffineTransformTranslate(transform, Width, 0);
					transform = CGAffineTransformRotate(transform, CGFloat(M_PI_2));
					break;

				case UIImageOrientationRight:
				case UIImageOrientationRightMirrored:
					transform = CGAffineTransformTranslate(transform, 0, Height);
					transform = CGAffineTransformRotate(transform, CGFloat(-M_PI_2));
					break;

				case UIImageOrientationUp:
				case UIImageOrientationUpMirrored:
					break;
			}

			switch(image.imageOrientation)
			{
				case UIImageOrientationUpMirrored:
				case UIImageOrientationDownMirrored:
					CGAffineTransformTranslate(transform, Width, 0);
					CGAffineTransformScale(transform, -1, 1);
					break;

				case UIImageOrientationLeftMirrored:
				case UIImageOrientationRightMirrored:
					CGAffineTransformTranslate(transform, Height, 0);
					CGAffineTransformScale(transform, -1, 1);
					break;

				case UIImageOrientationUp:
				case UIImageOrientationDown:
				case UIImageOrientationLeft:
				case UIImageOrientationRight:
					break;
			}

			size_t drawWidth = image.size.width;
			size_t drawHeight = image.size.height;
			switch (image.imageOrientation)
			{
				case UIImageOrientationLeft:
				case UIImageOrientationLeftMirrored:
				case UIImageOrientationRight:
				case UIImageOrientationRightMirrored:
					drawWidth = image.size.height;
					drawHeight = image.size.width;
					break;

				default:
					break;
			}

			CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
			CGImageRef imageRef = [image CGImage];
			CGContextRef bitmap = CGBitmapContextCreate(imageDataPtr,
				Width, Height, 8, Width * ComponentsPerPixel, colorSpace,
				kCGImageAlphaPremultipliedLast);
			CGContextConcatCTM(bitmap, transform);
			CGContextDrawImage(bitmap, CGRectMake(0, 0, drawWidth, drawHeight), imageRef);
			CGContextRelease(bitmap);
			CGColorSpaceRelease(colorSpace);
		}
		return imageData;
	}

	// Generate UIImage from Texture2D
	UIImage *UIImageFrom(UTexture2D *texture, float opacity = 1.0f, bool premultiplyAlpha = true)
	{
		if (texture == nil)
		{
			UE_LOG(LogTemp, Log, TEXT("IOS UIImageFrom Texture Error: Texture is nil"));
			return nil;
		}

		if (!texture->PlatformData)
		{
			UE_LOG(LogTemp, Log, TEXT("IOS UIImageFrom Texture Error: PlatformData is nil"));
			return nil;
		}

		if (texture->PlatformData->Mips.Num() <= 0)
		{
			UE_LOG(LogTemp, Log, TEXT("IOS UIImageFrom Texture Error: MipMaps is nil"));
			return nil;
		}

		if (texture->CompressionSettings != TC_VectorDisplacementmap)
		{
			UE_LOG(LogTemp, Log, TEXT("IOS UIImageFrom Texture Error: Texture Compression Settings is not VectorDisplacementMap"));
			return nil;
		}

		FTexture2DMipMap& textureMipMap = texture->PlatformData->Mips[0];
		int32 textureWidth = textureMipMap.SizeX;
		int32 textureHeight = textureMipMap.SizeY;

		if (textureWidth <= 0 || textureHeight <= 0)
		{
			UE_LOG(LogTemp, Log, TEXT("IOS UIImageFrom Texture Error: TextureWidth and / or TextureHeight is 0"));
			return nil;
		}
		FByteBulkData* RawImageData = &textureMipMap.BulkData;

		if (!RawImageData)
		{
			UE_LOG(LogTemp, Log, TEXT("IOS UIImageFrom Texture Error: Texture BulkData is nil"));
			return nil;
		}

		UIImage *uiimage = nil;
		FColor* RawColorArray = static_cast<FColor*>(RawImageData->Lock(LOCK_READ_ONLY));

		if (RawColorArray)
		{
			// Generate UIImage
			const size_t Width = textureWidth;
			const size_t Height = textureHeight;
			const size_t Area = Width * Height;
			const size_t ComponentsPerPixel = 4; // rgba
			const size_t NumComponents = Area * ComponentsPerPixel;

			uint8_t *pixelData = new uint8_t[Width * Height * ComponentsPerPixel];

			// fill the pixels with a lovely opaque blue gradient:
			const size_t BitsPerComponent = 8;
			const size_t BytesPerRow = Width * ComponentsPerPixel;
			CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
			CGContextRef gtx = CGBitmapContextCreate(&pixelData[0], Width, Height, BitsPerComponent, BytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast);

			for (size_t y = 0; y < Height; y++)
			{
				for (size_t x = 0; x < Width; x++)
				{
					size_t offset = y * Width + x;
					size_t pixelOffset = offset * ComponentsPerPixel;
					float premultipliedAlpha = RawColorArray[offset].A / 255.0f;

					pixelData[pixelOffset] = uint8_t(RawColorArray[offset].R / 255.0f * premultipliedAlpha * 255.0f);
					pixelData[pixelOffset + 1] = uint8_t(RawColorArray[offset].G / 255.0f * premultipliedAlpha * 255.0f);
					pixelData[pixelOffset + 2] = uint8_t(RawColorArray[offset].B / 255.0f * premultipliedAlpha * 255.0f);
					pixelData[pixelOffset + 3] = RawColorArray[offset].A; // size_t(RawColorArray[offset].A * opacity);
				}
			}
			RawImageData->Unlock();

			CGImageRef toCGImage = CGBitmapContextCreateImage(gtx);
			uiimage = [[UIImage alloc] initWithCGImage:toCGImage];
			CGImageRelease(toCGImage);
			delete[] pixelData;
			return[uiimage autorelease];
		}
		UE_LOG(LogTemp, Log, TEXT("IOS UIImageFrom Texture Error: Unable to retrieve texture data."));
		return nil;
	}

	// Generate UIImage from FColor array
	UIImage *UIImageFrom(const TArray<FColor> &RawColorArray, int32 textureWidth, int32 textureHeight, bool premultiplyAlpha = true)
	{
		if (RawColorArray.Num() > 0 && textureWidth > 0 && textureHeight > 0)
		{
			// Generate UIImage
			const size_t Width = textureWidth;
			const size_t Height = textureHeight;
			const size_t Area = Width * Height;
			const size_t ComponentsPerPixel = 4; // rgba
			const size_t NumComponents = Area * ComponentsPerPixel;

			uint8_t *pixelData = new uint8_t[Width * Height * ComponentsPerPixel];

			const size_t BitsPerComponent = 8;
			const size_t BytesPerRow = Width * ComponentsPerPixel;
			CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
			CGContextRef gtx = CGBitmapContextCreate(&pixelData[0], Width, Height, BitsPerComponent, BytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast);

			for (size_t y = 0; y < Height; y++)
			{
				for (size_t x = 0; x < Width; x++)
				{
					size_t offset = y * Width + x;
					size_t pixelOffset = offset * ComponentsPerPixel;
					float premultipliedAlpha = premultiplyAlpha ? (RawColorArray[offset].A / 255.0f) : 1.0f;

					pixelData[pixelOffset] = uint8_t(RawColorArray[offset].R / 255.0f * premultipliedAlpha * 255.0f);
					pixelData[pixelOffset + 1] = uint8_t(RawColorArray[offset].G / 255.0f * premultipliedAlpha * 255.0f);
					pixelData[pixelOffset + 2] = uint8_t(RawColorArray[offset].B / 255.0f * premultipliedAlpha * 255.0f);
					pixelData[pixelOffset + 3] = RawColorArray[offset].A; // size_t(RawColorArray[offset].A * opacity);
				}
			}
			//RawImageData->Unlock();

			CGImageRef toCGImage = CGBitmapContextCreateImage(gtx);
			UIImage *image = [[UIImage alloc] initWithCGImage:toCGImage];
			CGImageRelease(toCGImage);

			delete[] pixelData;
			return[image autorelease];
		}
		UE_LOG(LogTemp, Log, TEXT("IOS UIImageFrom ColorArray Error: Array Size or TextureWidth or TextureHeight is 0."));
		return nil;
	}

	// Generate Texture2DDynamic from an UIImage
	UTexture2DDynamic *Texture2DFrom(UIImage *image)
	{
		if (image)
		{
			size_t width = image.size.width;
			size_t height = image.size.height;
			if (width > 0 && height > 0)
			{
				TArray<FColor> FColorData = FColorArrayFrom(image);
				if (UTexture2DDynamic* Texture = UTexture2DDynamic::Create(width, height))
				{
					UE_LOG(LogTemp, Log, TEXT("IOS Texture2D From UIImage: Created dynamic texture size %d x %d"), width, height);
					Texture->SRGB = true;
					Texture->UpdateResource();

					ENQUEUE_UNIQUE_RENDER_COMMAND_TWOPARAMETER(
						FWriteRawDataToTexture,
						FTexture2DDynamicResource*, TextureResource, static_cast<FTexture2DDynamicResource*>(Texture->Resource),
						TArray<FColor>, RawFColorData, *&FColorData,
						{
							WriteToTexture2D_RenderThread(TextureResource, RawFColorData, true, true, 0);
						});
					return Texture;
				}
				else
				{
					return nil;
				}
			}
			else
			{
				return nil;
			}
		}
		return nil;
	}

	// Generate UIImage from RenderTarget2D
	UIImage* UIImageFrom(UTextureRenderTarget2D *texture, float opacity = 1.0f, bool premultiplyAlpha = true)
	{
		if (texture == nil)
		{
			UE_LOG(LogTemp, Log, TEXT("IOS UIImage From UTextureRenderTarget2D: Render Target is nil"));
			return nil;
		}

		FRenderTarget* RenderTarget = texture->GameThread_GetRenderTargetResource();
		if (!RenderTarget)
		{
			UE_LOG(LogTemp, Log, TEXT("IOS UIImage From UTextureRenderTarget2D: Render Target GetRenderTargetResource is nil"));
			return nil;
		}

		int32 textureWidth = texture->SizeX;
		int32 textureHeight = texture->SizeY;
		if (textureWidth <= 0 || textureHeight <= 0)
		{
			UE_LOG(LogTemp, Log, TEXT("IOS UIImage From UTextureRenderTarget2D: Render Target Width and / or Height is 0"));
			return nil;
		}

		TArray<FColor> SurfData;
		RenderTarget->ReadPixels(SurfData);
		if (SurfData.Num() <= 0)
		{
			UE_LOG(LogTemp, Log, TEXT("IOS UIImage From UTextureRenderTarget2D: Render Target Pixel Data Size is 0"));
			return nil;
		}

		return UIImageFrom(SurfData, textureWidth, textureHeight, premultiplyAlpha);
	}

	// Generate UIImage from Texture2D
	UIImage *UIImageFrom(UTexture2DDynamic *texture, float opacity = 1.0f, bool premultiplyAlpha = true)
	{
		if (texture)
		{
			UIImage *uiimage = [UIImage alloc];
			ENQUEUE_UNIQUE_RENDER_COMMAND_FOURPARAMETER(
				FUIImageFromTextureDynamic,
				UTexture2DDynamic*, texture, texture,
				float, opacity, opacity,
				bool, premultiplyAlpha, premultiplyAlpha,
				UIImage*, uiimage, uiimage,
			{
				if (texture == nil)
				{
					UE_LOG(LogTemp, Log, TEXT("IOS UIImageFrom Texture Error: Texture is nil"));
					return;
				}
				FTexture2DDynamicResource* TextureResource = static_cast<FTexture2DDynamicResource*>(texture->Resource);
				FTexture2DRHIParamRef TextureRHI = TextureResource->GetTexture2DRHI();
				int32 textureWidth = TextureRHI->GetSizeX();
				int32 textureHeight = TextureRHI->GetSizeY();
				uint32 DestStride = 0;
				if (textureWidth <= 0 || textureHeight <= 0)
				{
					UE_LOG(LogTemp, Log, TEXT("IOS UIImageFrom Texture Error: TextureWidth and / or TextureHeight is 0"));
					return;
				}

				//		uint8* DestData = reinterpret_cast<uint8*>(RHILockTexture2D(TextureRHI, 0, RLM_ReadOnly, DestStride, false, false));
				//		FColor* RawColorArray = static_cast<FColor*>(RawImageData->Lock(LOCK_READ_ONLY));
				FColor* RawColorArray = reinterpret_cast<FColor*>(RHILockTexture2D(TextureRHI, 0, RLM_ReadOnly, DestStride, false, false));

				if (RawColorArray)
				{
					// Generate UIImage
					const size_t Width = textureWidth;
					const size_t Height = textureHeight;
					const size_t Area = Width * Height;
					const size_t ComponentsPerPixel = 4; // rgba
					const size_t NumComponents = Area * ComponentsPerPixel;

					uint8_t *pixelData = new uint8_t[Width * Height * ComponentsPerPixel];
					// fill the pixels with a lovely opaque blue gradient:
					const size_t BitsPerComponent = 8;
					const size_t BytesPerRow = Width * ComponentsPerPixel;
					CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
					CGContextRef gtx = CGBitmapContextCreate(&pixelData[0], Width, Height, BitsPerComponent, BytesPerRow, colorSpace, kCGImageAlphaPremultipliedLast);

					for (size_t y = 0; y < Height; y++)
					{
						for (size_t x = 0; x < Width; x++)
						{
							size_t offset = y * Width + x;
							size_t pixelOffset = offset * ComponentsPerPixel;
							float premultipliedAlpha = RawColorArray[offset].A / 255.0f;

							pixelData[pixelOffset] = uint8_t(RawColorArray[offset].R / 255.0f * premultipliedAlpha * 255.0f);
							pixelData[pixelOffset + 1] = uint8_t(RawColorArray[offset].G / 255.0f * premultipliedAlpha * 255.0f);
							pixelData[pixelOffset + 2] = uint8_t(RawColorArray[offset].B / 255.0f * premultipliedAlpha * 255.0f);
							pixelData[pixelOffset + 3] = RawColorArray[offset].A; // size_t(RawColorArray[offset].A * opacity);
						}
					}
					RHIUnlockTexture2D(TextureRHI, 0, false, false);
					CGImageRef toCGImage = CGBitmapContextCreateImage(gtx);
					//uiimage = [[UIImage alloc] initWithCGImage:toCGImage];
					[uiimage initWithCGImage : toCGImage];
					CGImageRelease(toCGImage);
					delete[] pixelData;
				}
			});
			return[uiimage autorelease];
		}
		UE_LOG(LogTemp, Log, TEXT("IOS UIImageFrom TextureDynamic Error: Unable to retrieve texture data."));
		return nil;
	}

	// Generate UIImage from a Texture
	UIImage *UIImageFrom(UTexture *texture, float opacity = 1.0f, bool premultiplyAlpha = true)
	{
		if (texture)
		{
			if (texture->IsA(UTexture2D::StaticClass()))
			{
				return UIImageFrom((UTexture2D*)texture, opacity, premultiplyAlpha);
			}
			else if (texture->IsA(UTextureRenderTarget2D::StaticClass()))
			{
				return UIImageFrom((UTextureRenderTarget2D*)texture, opacity, premultiplyAlpha);
			}
			else if (texture->IsA(UTexture2DDynamic::StaticClass()))
			{
				return UIImageFrom((UTexture2DDynamic*)texture, opacity, premultiplyAlpha);
			}
			UE_LOG(LogTemp, Log, TEXT("IOS UIImage From UTexture: Unknown texture type specified"));
			return nullptr;
		}
		UE_LOG(LogTemp, Log, TEXT("IOS UIImage From UTexture: Texture is nil"));
		return nullptr;
	}
}

// Perform startup setup functions for IOS
// Currently there is nothing that needs to be setup for IOS
void SetupIOS_RaveMobilePhotoGallery()
{
	// Do nothing for IOS Setup
	NSString *tempPath = NSTemporaryDirectory();
	FString destPathStr = IOS_TO_UE_STRING(tempPath);
	destPathStr = destPathStr / "OnDemandResources";
	NSString *dstPath = UE_TO_IOS_STRING(destPathStr);
	bool bCreateDirectory = [[NSFileManager defaultManager] createDirectoryAtPath:dstPath
		withIntermediateDirectories : YES
		attributes : nil
		error : nil];

	NSLog(@"Created Directory : %@ -> %@", dstPath, bCreateDirectory ? @"YES" : @"NO");

}

#endif