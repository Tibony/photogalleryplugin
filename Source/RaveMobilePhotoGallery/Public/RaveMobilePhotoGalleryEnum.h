// Copyright 2018 Impreszions Solutions All Rights Reserved.
// Author : MunHon Chan

#pragma once

#include "CoreMinimal.h"
#include "RaveMobilePhotoGalleryEnum.generated.h"

#if UE_BUILD_SHIPPING
#define DebugLog(x) //
#define DebugLog2(x) //
#else
#define DebugLog(x) if(GEngine){GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT(x));}
#define DebugLog2(x) if(GEngine){GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, x);}
#endif

#if PLATFORM_IOS
#define UE_TO_IOS_STRING(string)	[NSString stringWithUTF8String : TCHAR_TO_UTF8(*string)]
#define IOS_TO_UE_STRING(string)	[string UTF8String];
#endif

// Rotation to be applied with the screenshot function for Rave Media Gallery
UENUM(BlueprintType, Category = "Photo Gallery")
enum EScreenshotRotation
{
	EScreenshotRotation_ROTATE0 UMETA(DisplayName = "Rotate CCW 0"),
	EScreenshotRotation_ROTATE90 UMETA(DisplayName = "Rotate CCW 90"),
	EScreenshotRotation_ROTATE180 UMETA(DisplayName = "Rotate CCW 180"),
	EScreenshotRotation_ROTATE270 UMETA(DisplayName = "Rotate CCW 270")
};