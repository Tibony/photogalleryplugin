// Copyright 2018 Impreszions Solutions All Rights Reserved.
// Author : MunHon Chan

// Reference From: https://raw.githubusercontent.com/mstorsjo/android-decodeencodetest/master/src/com/example/decodeencodetest/ExtractDecodeEditEncodeMuxTest.java

package com.impreszions.ravemediatools;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.content.Context;
import android.content.Intent;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.media.MediaRecorder;
import android.media.projection.MediaProjection;
import android.media.projection.MediaProjectionManager;
import android.media.projection.MediaProjection.Callback;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.media.CamcorderProfile;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.AudioManager;

import android.media.MediaScannerConnection;
import android.content.Context;
import android.net.Uri;
import java.io.File;
import java.io.IOException;
import android.os.FileObserver;
      
import android.view.ViewGroup.LayoutParams;
import android.widget.RelativeLayout;
import android.R;
import android.widget.TextView;
import android.graphics.Bitmap;
import android.view.WindowManager;
import android.view.Surface;

import android.opengl.EGL14;
import android.opengl.EGLSurface;
import android.opengl.GLES20;
import android.opengl.GLES30;
import android.os.Looper;
import java.lang.Thread;

public class ScreenRecorderMediaProjection extends ScreenRecorder
{
	public native int nativeSetRenderSurface(Surface surface);

	private static final String TAG = "ScreenRecorderMediaProjection";

	private static final int SCREEN_CAPTURE_PERMISSION_CODE = 61510;
    private static final int RECORD_AUDIO_PERMISSION_CODE = 61511;
    private int mScreenDensity;
    private MediaProjectionManager mProjectionManager;
    private static int DISPLAY_WIDTH = 640;
    private static int DISPLAY_HEIGHT = 480;
    private MediaProjection mMediaProjection;
    private VirtualDisplay mVirtualDisplay;
    private MediaProjectionCallback mMediaProjectionCallback;
    private MediaRecorder mMediaRecorder;
    private boolean hasInitScreenCapture = false;
    private boolean isRecording = false;
    private boolean willBeRecording = false;
	private static int DISPLAY_ROTATION = 0;
	private static int DISPLAY_OFFSETWIDTH = 0;
	private static int DISPLAY_OFFSETHEIGHT = 0;

	private int SAVE_VIDEO_WIDTH = 0;
	private int SAVE_VIDEO_HEIGHT = 0;
	private int SAVE_VIDEO_ROTATION = 0;

    //private static String CAPTURE_FILENAME = "/sdcard/capture.mp4";
	private MPRenderThread renderThread;
	private AudioTrack audioTrack;

	@Override
	public void StartRecording(int width, int height, String filename, int rotation, int offsetWidth, int offsetHeight, boolean DoSaveToGalleryOnComplete) //, SurfaceView MySurfaceView)
	{

		if ( !isRecording )
		{			
	/*		//if ( !hasInitScreenCapture )
			{
				CAPTURE_FILENAME = filename;
				DISPLAY_WIDTH = width;
				DISPLAY_HEIGHT = height;
				DISPLAY_ROTATION = rotation;
				DISPLAY_OFFSETWIDTH = offsetWidth;
				DISPLAY_OFFSETHEIGHT = offsetHeight;
				SaveToGalleryOnComplete = DoSaveToGalleryOnComplete;

				DisplayMetrics metrics = new DisplayMetrics();
				gameActivity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
				mScreenDensity = metrics.densityDpi;

				if ( mMediaRecorder == null ) mMediaRecorder = new MediaRecorder();
				if ( mProjectionManager == null ) mProjectionManager = (MediaProjectionManager)gameActivity.getSystemService(Context.MEDIA_PROJECTION_SERVICE);
				if ( mMediaProjectionCallback == null ) mMediaProjectionCallback = new MediaProjectionCallback();

				initRecorder();
//				prepareRecorder();
				hasInitScreenCapture = true;
			}
			willBeRecording = true;
			renderThread = new MPRenderThread(this);
			renderThread.start();

	*/

			RecordThread rec = new RecordThread();
			rec.start();



		}
		else
		{
			Log.d(TAG,"Unable to create directory: " + filename);
		}
	}

	public void run()
	{
		try {
			mIntentData = null;
			prepareRecorder();
			startRecorder();
		}
		catch(Exception e)
		{

		}
	}

	public void stop()
	{
		if ( isRecording )
		{
			if ( mMediaProjection != null )
			{
				mMediaProjection.stop();
			}
			else
			{
				DoStopRecording();
			}
		}
	}

	@Override
	public void StopRecording()
	{
		Log.d(TAG,"muxer: Stopping Recording");
		renderThread.prepareStop();
		renderThread = null;
		willBeRecording = false;
	}

	@Override
	public boolean IsRecording()
	{
		return willBeRecording;
	}

	@Override
	public void OnAddedToGallery(String path)
	{
		nativeStopScreenRecordingResponse(1, SAVE_VIDEO_WIDTH, SAVE_VIDEO_HEIGHT, SAVE_VIDEO_ROTATION);
	}

	public class MPRenderThread extends Thread
	{
		private ScreenRecorderMediaProjection mp;

		public MPRenderThread(ScreenRecorderMediaProjection _mp)
		{
			mp = _mp;
		}

		@Override
		public void run() {
			Looper.prepare();
			mp.run();
		}

		public void prepareStop()
		{
			mp.stop();
			mp = null;
		}
	}



    class RecordThread extends Thread{
		static final int frequency = 44100;
		static final int channelConfiguration = AudioFormat.CHANNEL_CONFIGURATION_MONO;
		static final int audioEncoding = AudioFormat.ENCODING_PCM_16BIT;
		@Override
		public void run() {
			// TODO Auto-generated method stub
			int recBufSize = AudioRecord.getMinBufferSize(frequency,channelConfiguration, audioEncoding)*2;
			int plyBufSize = AudioTrack.getMinBufferSize(frequency,channelConfiguration, audioEncoding)*2;
			AudioRecord audioRecord = new AudioRecord(MediaRecorder.AudioSource.MIC, frequency,channelConfiguration, audioEncoding, recBufSize);
			audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, frequency,channelConfiguration, audioEncoding, plyBufSize, AudioTrack.MODE_STREAM);
			byte[] recBuf = new byte[recBufSize];
			audioRecord.startRecording();
			audioTrack.play();
			while(true){
			int readLen = audioRecord.read(recBuf, 0, recBufSize);
			audioTrack.write(recBuf, 0, readLen);
			}
			//audioTrack.stop();
			//audioRecord.stop();
		}
    }







	private static Intent mIntentData = null;

	protected Intent GetIntentData()
	{
		return (Intent)mIntentData.clone();
	}

	@Override
	public void gameActivityOnActivityResultAdditions(int requestCode, int resultCode, Intent data)
	{
		if (requestCode == SCREEN_CAPTURE_PERMISSION_CODE)
		{
			if (resultCode == Activity.RESULT_OK)
			{
				mIntentData = (Intent) data.clone();
				Log.d(TAG,"Starting recording");
				startRecorder();
			}
			else
			{
				isRecording = false;
				//hideScreenRecorderOverlay();
				nativeStartScreenRecordingResponse(0);
				Log.d(TAG,"Permission denied");
			}
		}
	}

	@Override
	public void PauseRecording()
	{
		//if ( mMediaRecorder != null && isRecording ) mMediaRecorder.pause();
		audioTrack.stop();
	}

	@Override
	public void ResumeRecording()
	{
		//if ( mMediaRecorder != null && isRecording ) mMediaRecorder.resume();
		audioTrack.play();
	}

// Recorder
	private void initRecorder()
    {
		mMediaRecorder.reset();
		mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
//		mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
		mMediaRecorder.setVideoSource(MediaRecorder.VideoSource.SURFACE);
//		mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP); //MPEG_4);
		mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
		mMediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
//		mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
		mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);

		Context context = gameActivity.getApplicationContext();

		WindowManager windowManager = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE));
		int rotation = windowManager.getDefaultDisplay().getRotation();
		DisplayMetrics dm = new DisplayMetrics();
		windowManager.getDefaultDisplay().getMetrics(dm);
		int wWidth = dm.widthPixels;
		int wHeight = dm.heightPixels;

		mMediaRecorder.setVideoSize(wWidth, wHeight);
		mMediaRecorder.setVideoEncodingBitRate(16 * 512 * 1024);
		mMediaRecorder.setVideoFrameRate(30);
		mMediaRecorder.setOrientationHint(DISPLAY_ROTATION);
		mMediaRecorder.setOutputFile(CAPTURE_FILENAME);

		SAVE_VIDEO_WIDTH = wWidth;
		SAVE_VIDEO_HEIGHT = wHeight;
		SAVE_VIDEO_ROTATION = DISPLAY_ROTATION;
    }

	private void prepareRecorder()
      {
        try {
          mMediaRecorder.prepare();
        }
        catch (IllegalStateException e)
        {
          e.printStackTrace();
//          finish();
        }
        catch (IOException e)
        {
          e.printStackTrace();
//          finish();
        }
      }

      private void startRecorder()
      {
		if ( mIntentData == null )
		{
			gameActivity.startActivityForResult(mProjectionManager.createScreenCaptureIntent(), SCREEN_CAPTURE_PERMISSION_CODE);
			return;
		}
		else
		{
  			mMediaProjection = mProjectionManager.getMediaProjection(Activity.RESULT_OK, GetIntentData());
			mMediaProjectionCallback.MP = this;
			mMediaProjection.registerCallback(mMediaProjectionCallback, null);
			mVirtualDisplay = createVirtualDisplay();
			nativeStartScreenRecordingResponse(2);
		}
      }

      private void stopRecorder()
      {
        //hideScreenRecorderOverlay();
        if (mVirtualDisplay != null)
        {
          mVirtualDisplay.release();
		  mVirtualDisplay = null;
        }
        else
        {
			nativeSaveScreenRecordingResponse(1, SAVE_VIDEO_WIDTH, SAVE_VIDEO_HEIGHT, SAVE_VIDEO_ROTATION);
			nativeStopScreenRecordingResponse(1, SAVE_VIDEO_WIDTH, SAVE_VIDEO_HEIGHT, SAVE_VIDEO_ROTATION);
        }
      }

      private VirtualDisplay createVirtualDisplay()
      {
        Context context = gameActivity.getApplicationContext();

		WindowManager windowManager = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE));
		int rotation = windowManager.getDefaultDisplay().getRotation();
		DisplayMetrics dm = new DisplayMetrics();
		windowManager.getDefaultDisplay().getMetrics(dm);
		int wWidth = dm.widthPixels;
		int wHeight = dm.heightPixels;

        return mMediaProjection.createVirtualDisplay("Recording Display",
          wWidth, wHeight, mScreenDensity,
          DisplayManager.VIRTUAL_DISPLAY_FLAG_AUTO_MIRROR,
          mMediaRecorder.getSurface(), null, null);
      }

	  public void DoStopRecording()
	  {
		if ( mMediaRecorder != null )
		{
			mMediaRecorder.stop();
			mMediaRecorder.reset();
			mMediaRecorder.release();
			mMediaRecorder = null;
		}

		hasInitScreenCapture = false;
		mMediaProjection = null;

		if (mVirtualDisplay != null )
		{
			mVirtualDisplay.release();
			mVirtualDisplay = null;
		}
		isRecording = false;

		nativeSaveScreenRecordingResponse(1, SAVE_VIDEO_WIDTH, SAVE_VIDEO_HEIGHT, SAVE_VIDEO_ROTATION);

		if ( SaveToGalleryOnComplete )
		{
			AddToGallery();
		}
		else
		{
			nativeStopScreenRecordingResponse(1, SAVE_VIDEO_WIDTH, SAVE_VIDEO_HEIGHT, SAVE_VIDEO_ROTATION);
		}
	 }

	@Override
	public void renderFrame(int mOffscreenTexture, int mSurfaceWidth, int mSurfaceHeight, android.opengl.EGLDisplay display, android.opengl.EGLSurface surface)
	{
		Log.d(TAG,"muxer: Render Frame Start Recording");
		mMediaRecorder.start();
		isRecording = true;
		nativeStartScreenRecordingResponse(1);
	}

      
      private class MediaProjectionCallback extends MediaProjection.Callback
      {
		public ScreenRecorderMediaProjection MP = null;

        @Override
        public void onStop()
        {
			if ( MP != null ) MP.DoStopRecording();
        }
      }

      private class MediaRecorderFileObserver extends FileObserver
      {
        public MediaRecorderFileObserver(String path, int mask)
        {
          super(path, mask);
        }

        public void onEvent(int event, String path)
        {
          MediaScannerConnection.scanFile(gameActivity.getApplicationContext(), new String[] { path }, null, new MediaScannerConnection.OnScanCompletedListener()
          {
            public void onScanCompleted(String path, Uri uri)
            {
              Log.d(TAG,"Finished scanning " + path);
            }
          });
          this.stopWatching();
        }
      }
}