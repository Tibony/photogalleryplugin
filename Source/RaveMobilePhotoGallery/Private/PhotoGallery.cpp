// Copyright 2018 Impreszions Solutions All Rights Reserved.
// Author : MunHon Chan

#include "PhotoGallery.h"
#include "MediaTexture.h"
#include "MediaPlayer.h"
#include "Engine/Texture2DDynamic.h"
#include "Async/Async.h"
#include "Engine/Engine.h"
#include "Engine/GameViewportClient.h"

namespace RaveMobilePhotoGallery
{
	// Generate a filename based on the current time
	extern FString GetTimestampedFilename(FString extension, FString prefix, FString suffix);

	// Generate a dynamic texture2D from an FColor array
	extern UTexture2DDynamic *Texture2DFrom(const TArray<FColor> &RawColorArray, int32 textureWidth, int32 textureHeight);

	// Rotate contents of a FColor array
	extern void RotatePixelArray(TArray<FColor> &bitmap, int &width, int &height, int rotation);

	// Rotate contents of a FColor array (int version)
	extern void RotatePixelArray(TArray<uint8> &bitmap, int &width, int &height, int rotation);

#if PLATFORM_IOS
	// Create a Texture2DDynamic from an IOS UIImage
	extern UTexture2DDynamic *Texture2DFrom(UIImage *image);
#endif

#if PLATFORM_ANDROID
	// Create a Texture2DDynamic from an Android Bitmap array from JNI
	extern UTexture2DDynamic *Texture2DFromAndroidBitmap(const TArray<FColor> &FColorData, int32 width, int32 height);

	// Get Texture2DDynamic from an image file on disk
	extern UTexture2DDynamic* Texture2DFromFileDynamic(const FString& ImagePath, bool& IsValid, int32& OutWidth, int32& OutHeight, int rotation);
#endif
}

using namespace RaveMobilePhotoGallery;
static bool hasPendingRequest = false;

#if PLATFORM_ANDROID
#include "AndroidJNI.h"
#include "Android/AndroidApplication.h"
#include <jni.h>
#include "AndroidPermissionFunctionLibrary.h"
#include "AndroidPermissionCallbackProxy.h"

static jmethodID AndroidThunkJava_ShowPhotoGallery;
static UAsyncPhotoGalleryTask* AndroidLastPhotoGalleryTask = nullptr;

static FString lastStoragePath;
static bool lastShowPhotos = false;
static bool lastShowVideos = false;

// Setup variables for JNI calls related to photo gallery
int SetupJNI_RaveMobilePhotoGallery_PhotoGallery()
{
	JNIEnv* env = FAndroidApplication::GetJavaEnv();
	if (!env) return JNI_ERR;
	JNIEnv* ENV = env;

	UE_LOG(LogTemp, Log, TEXT("CHECKING: RaveMobilePhotoGallery_ShowPhotoGallery"));
	AndroidThunkJava_ShowPhotoGallery = FJavaWrapper::FindMethod(ENV, FJavaWrapper::GameActivityClassID, "RaveMobilePhotoGallery_ShowPhotoGallery", "(Ljava/lang/String;ZZ)Z", false);
	if (!AndroidThunkJava_ShowPhotoGallery)
	{
		UE_LOG(LogTemp, Log, TEXT("ERROR: RaveMobilePhotoGallery_ShowPhotoGallery Not Found"));
	}

	return JNI_OK;
}

// Calls JNI function to open the photo gallery window
bool AndroidThunkCpp_ShowPhotoGallery(FString showFolder = "", bool bShowPhotos = true, bool bShowVideos = true)
{
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		jstring Argument = Env->NewStringUTF(TCHAR_TO_UTF8(*showFolder));
		return FJavaWrapper::CallBooleanMethod(Env, FJavaWrapper::GameActivityThis, AndroidThunkJava_ShowPhotoGallery, Argument, bShowPhotos, bShowVideos);
	}
	return false;
}

// Return call from JNI when an image media is selected
extern "C" bool Java_com_epicgames_ue4_GameActivity_nativeRaveMobilePhotoGalleryShowPhotoGalleryResponseImage(JNIEnv* LocalJNIEnv, jobject LocalThiz, jstring imagePath, jint rotation)
{
	const char *cstr = LocalJNIEnv->GetStringUTFChars(imagePath, NULL);
	FString UE4ImagePath(cstr);
	int thisRotation = (int)rotation;
	LocalJNIEnv->ReleaseStringUTFChars(imagePath, cstr);

	if (UE4ImagePath.IsEmpty())
	{
		AndroidLastPhotoGalleryTask->IsFailure(nullptr, EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_NONE, FPhotoGalleryMediaInfo(), EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_INVALID_MEDIA);
		AndroidLastPhotoGalleryTask = nullptr;
		hasPendingRequest = false;
	}
	else
	{
		AsyncTask(ENamedThreads::GameThread, [UE4ImagePath, thisRotation]() {
			bool bIsValid = false;
			int32 width = 0;
			int32 height = 0;
			UTexture2DDynamic* tex2D = Texture2DFromFileDynamic(UE4ImagePath, bIsValid, width, height, thisRotation);
			FPhotoGalleryMediaInfo mediaInfo;

			if (bIsValid && tex2D)
			{
				bool bIsSwappedSize = (thisRotation == 90) || (thisRotation == 270);
				mediaInfo.MediaType = EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_PHOTO;
				mediaInfo.Width = bIsSwappedSize ? height : width;
				mediaInfo.Height = bIsSwappedSize ? width : height;
				mediaInfo.Rotation = thisRotation;

				AndroidLastPhotoGalleryTask->IsSuccess(tex2D, EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_PHOTO, mediaInfo, EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_NO_ERROR);
				AndroidLastPhotoGalleryTask = nullptr;
				hasPendingRequest = false;
			}
			else
			{
				AndroidLastPhotoGalleryTask->IsFailure(nullptr, EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_NONE, mediaInfo, EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_INVALID_MEDIA);
				AndroidLastPhotoGalleryTask = nullptr;
				hasPendingRequest = false;
			}
		});
	}
	return JNI_TRUE;
}

// Return call from JNI when a video media is selected
extern "C" bool Java_com_epicgames_ue4_GameActivity_nativeRaveMobilePhotoGalleryShowPhotoGalleryResponseVideo(JNIEnv* LocalJNIEnv, jobject LocalThiz, jstring videoPath,
	jint width, jint height, jint rotation)
{
	const char *cstr = LocalJNIEnv->GetStringUTFChars(videoPath, NULL);
	FString UE4VideoPath(cstr);
	FString VideoURL(FString(TEXT("file://")) + UE4VideoPath);
	LocalJNIEnv->ReleaseStringUTFChars(videoPath, cstr);

	int videoWidth = (int)width;
	int videoHeight = (int)height;
	int videoRotation = (int)rotation;

	ENQUEUE_UNIQUE_RENDER_COMMAND_FOURPARAMETER(FShowPhotoGalleryVideo, FString, VideoURL, VideoURL,
		int, videoWidth, videoWidth,
		int, videoHeight, videoHeight,
		int, videoRotation, videoRotation,
		{
			UMediaPlayer *mediaPlayer = AndroidLastPhotoGalleryTask->useMediaPlayer; // NewObject<UMediaPlayer>();
			bool bAutoplay = AndroidLastPhotoGalleryTask->bAutoplayMedia;
	
			if (!mediaPlayer)
			{
				AndroidLastPhotoGalleryTask->IsFailure(nullptr, EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_NONE, FPhotoGalleryMediaInfo(), EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_NO_MEDIA_PLAYER_SPECIFIED);
				AndroidLastPhotoGalleryTask = nullptr;
				hasPendingRequest = false;
				//mediaPlayer = NewObject<UMediaPlayer>();
			}

			UMediaTexture *mediaTexture = NewObject<UMediaTexture>();
			mediaTexture->SetMediaPlayer(mediaPlayer);
			mediaTexture->UpdateResource();
			FPhotoGalleryMediaInfo mediaInfo;

			if (mediaPlayer->OpenUrl(VideoURL))
			{
				FIntPoint dimensions = mediaPlayer->GetVideoTrackDimensions(0, 0);
				mediaInfo.MediaType = EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_VIDEO;
				mediaInfo.Width = videoWidth; // dimensions.X;
				mediaInfo.Height = videoHeight; // dimensions.Y;

				if ( bAutoplay ) mediaPlayer->Play();
				AndroidLastPhotoGalleryTask->IsSuccess(mediaTexture, EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_VIDEO, mediaInfo, EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_NO_ERROR);
				AndroidLastPhotoGalleryTask = nullptr;
				hasPendingRequest = false;
			}
			else
			{
				AndroidLastPhotoGalleryTask->IsFailure(nullptr, EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_NONE, mediaInfo, EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_INVALID_MEDIA);
				AndroidLastPhotoGalleryTask = nullptr;
				hasPendingRequest = false;
			}
		});
	return JNI_TRUE;
}
#endif

#if PLATFORM_IOS
#import <UIKit/UIKit.h>
#include "IOSAppDelegate.h"
#import <Foundation/Foundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <AVFoundation/AVFoundation.h>

// IOS Interface Class to handle photo gallery functions
@interface IOSPhotoGallery : UIImagePickerController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>
{
	UAsyncPhotoGalleryTask *owningTask;
	bool showBrowser;
	bool returnTexture;
}
@end

@implementation IOSPhotoGallery

// Create an IOSPhotoGallery object
+ (IOSPhotoGallery*) GetDelegate
{
	return [[IOSPhotoGallery alloc] init];
}

-(id)init
{
	if (self == [super init])
	{
		self.delegate = self;
		showBrowser = false;
		returnTexture = false;
	}
	return self;
}

-(void)dealloc
{
	owningTask = nil;
	[super dealloc];
}

-(bool)shouldAutorotate
{
	return YES;
}

-(NSUInteger)supportedInterfaceOrientations {
	return UIInterfaceOrientationMaskAll; // UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscape;
}

// Called by the UObject class to open the IOS native photo gallery window to allow selection of media
- (void) showPhotoGallery:(UAsyncPhotoGalleryTask *)Callback
	showPhotos:(bool)showPhotos showVideos:(bool)showVideos
	showBrowser:(bool)bShowBrowser returnTexture:(bool)bGetTexture
{
	if ([UIImagePickerController isSourceTypeAvailable : UIImagePickerControllerSourceTypeSavedPhotosAlbum])
	{
		owningTask = Callback;
		showBrowser = bShowBrowser;
		returnTexture = bGetTexture;
		self.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;

		if (showPhotos && showVideos) self.mediaTypes = @[(NSString *)kUTTypeImage, (NSString *)kUTTypeMovie];
		else if ( showPhotos ) self.mediaTypes = @[(NSString *)kUTTypeImage];
		else if ( showVideos ) self.mediaTypes = @[(NSString *)kUTTypeMovie];
		self.allowsEditing = NO;
		self.modalPresentationStyle = UIModalPresentationCurrentContext;
		//self.allowsEditing = YES;
		[[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:self animated : YES completion : nil];
	}
	else
	{
		if (Callback) Callback->IsFailure(nullptr, EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_NONE, FPhotoGalleryMediaInfo(), EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_UNSUPPORTED_PLATFORM);
		hasPendingRequest = false;
	}
}

// UIImagePickerControllerDelegates
- (void)imagePickerController:(UIImagePickerController *)picker
	didFinishPickingMediaWithInfo : (NSDictionary<NSString *, id> *)info
{
	NSString *mediaType = [info objectForKey : UIImagePickerControllerMediaType];
	bool isImage = UTTypeConformsTo((__bridge CFStringRef)mediaType, kUTTypeImage) != 0;
	bool isMovie = UTTypeConformsTo((__bridge CFStringRef)mediaType, kUTTypeMovie) != 0;

	if (isMovie)
	{
		NSLog(@"Picked a movie at URL %@",[info objectForKey : UIImagePickerControllerMediaURL]);
		NSURL *url = [info objectForKey : UIImagePickerControllerMediaURL];
		NSLog(@"> %@",[url absoluteString]);

		[url retain];

		AsyncTask(ENamedThreads::GameThread, [self, url]() {
			UMediaPlayer *mediaPlayer = owningTask->useMediaPlayer;
			bool bAutoplay = owningTask->bAutoplayMedia;

			if (!mediaPlayer)
			{
				//mediaPlayer = NewObject<UMediaPlayer>();
				if (owningTask) owningTask->IsFailure(nullptr, EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_NONE, FPhotoGalleryMediaInfo(), EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_NO_MEDIA_PLAYER_SPECIFIED);
			}
			
			if ( mediaPlayer )
			{
				FString urlString = IOS_TO_UE_STRING([url absoluteString]);

				FString copyString = IOS_TO_UE_STRING([url path]);;
				FPaths::MakeStandardFilename(copyString);
				copyString = "file://" + (FPaths::ProjectContentDir() / copyString);

				NSString *srcPath = [url path];
				NSString *tempPath = NSTemporaryDirectory();
				FString destPathStr = IOS_TO_UE_STRING(tempPath);
				destPathStr = destPathStr / "OnDemandResources";

				FPaths::GetCleanFilename(copyString);
				NSString *dstPath = UE_TO_IOS_STRING(destPathStr);

				bool bCreateDirectory = [[NSFileManager defaultManager] createDirectoryAtPath:dstPath
					withIntermediateDirectories : YES
					attributes : nil
					error : nil];

				destPathStr = destPathStr / FPaths::GetCleanFilename(copyString);
				dstPath = UE_TO_IOS_STRING(destPathStr);

				FPhotoGalleryMediaInfo mediaInfo;
				mediaInfo.MediaType = EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_VIDEO;
				AVAsset *assetInfo = [AVAsset assetWithURL:url];
				if (assetInfo)
				{
					NSArray *videoTracks = [assetInfo tracksWithMediaType:AVMediaTypeVideo];
					if ([videoTracks count] > 0)
					{
						AVAssetTrack* videoTrack = [videoTracks objectAtIndex : 0];
						CGAffineTransform transform = [videoTrack preferredTransform];
						int videoRotation = (int)(atan2(transform.b, transform.a) * 180 / M_PI);

						mediaInfo.Width = videoTrack.naturalSize.width;
						mediaInfo.Height = videoTrack.naturalSize.height;
						mediaInfo.Rotation = videoRotation;
					}
				}

				bool bCopyItem = [[NSFileManager defaultManager] moveItemAtPath:srcPath toPath : dstPath error : nil];
				FString destPathUrl = "file://" + destPathStr;

				if (mediaPlayer->OpenUrl(destPathUrl))
				{
					UMediaTexture *mediaTexture = NewObject<UMediaTexture>();
					mediaTexture->SetMediaPlayer(mediaPlayer);
					mediaTexture->UpdateResource();
					if (bAutoplay) mediaPlayer->Play();
					if (owningTask) owningTask->IsSuccess(mediaTexture, EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_VIDEO, mediaInfo, EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_NO_ERROR);
				}
				else
				{
					if (owningTask) owningTask->IsFailure(nullptr, EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_NONE, mediaInfo, EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_INVALID_MEDIA);
				}
			}
			[url release];
		});

	}
	else if (isImage)
	{
		UIImage *pickedImage = [info objectForKey : UIImagePickerControllerEditedImage];
		if (pickedImage == nil) pickedImage = [info objectForKey : UIImagePickerControllerOriginalImage];
		if (pickedImage == nil) pickedImage = [info objectForKey : UIImagePickerControllerCropRect];

		[pickedImage retain];
		AsyncTask(ENamedThreads::GameThread, [self, pickedImage]() {
			UTexture2DDynamic *imageTexture = RaveMobilePhotoGallery::Texture2DFrom(pickedImage);
			FPhotoGalleryMediaInfo mediaInfo;
			mediaInfo.MediaType = EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_PHOTO;
			mediaInfo.Width = imageTexture->SizeX;
			mediaInfo.Height = imageTexture->SizeY;

			switch (pickedImage.imageOrientation)
			{
			case UIImageOrientationDown:
			case UIImageOrientationDownMirrored:
				mediaInfo.Rotation = 180;
				break;

			case UIImageOrientationLeft:
			case UIImageOrientationLeftMirrored:
				mediaInfo.Rotation = 90;
				break;

			case UIImageOrientationRight:
			case UIImageOrientationRightMirrored:
				mediaInfo.Rotation = 270;
				break;

			case UIImageOrientationUp:
			case UIImageOrientationUpMirrored:
				mediaInfo.Rotation = 0;
				break;
			}

			if (owningTask) owningTask->IsSuccess(imageTexture, EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_PHOTO, mediaInfo, EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_NO_ERROR);
			UE_LOG(LogTemp, Log, TEXT("Rave Media Tools - Photo Gallery: Return Texture Size %d x %d"), imageTexture->SizeX, imageTexture->SizeY);
			[pickedImage release];
		});
	}
	else
	{
		if (owningTask) owningTask->IsFailure(nullptr, EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_NONE, FPhotoGalleryMediaInfo(), EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_INVALID_MEDIA);
	}
	[[[[UIApplication sharedApplication] keyWindow] rootViewController] dismissViewControllerAnimated:YES completion : nil];
	hasPendingRequest = false;
}

// UIImagePickerControllerDelegates
-(void)imagePickerControllerDidCancel : (UIImagePickerController *)picker
{
	if (owningTask) owningTask->IsFailure(nullptr, EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_NONE, FPhotoGalleryMediaInfo(), EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_USER_CANCELLED);
	[[[[UIApplication sharedApplication] keyWindow] rootViewController] dismissViewControllerAnimated:YES completion : nil]; 
	hasPendingRequest = false;
}

@end
#endif

// Constructor
UAsyncPhotoGalleryTask::UAsyncPhotoGalleryTask(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	if (HasAnyFlags(RF_ClassDefaultObject) == false)
	{
		AddToRoot();
	}
}

// Activate
void UAsyncPhotoGalleryTask::Activate()
{
	Super::Activate();
	bIsActivated = true;
	if (bHasResults)
	{
		if (bIsSuccess) IsSuccess(lastTexture, lastMediaType, lastMediaInfo, lastErrorMsg);
		else IsFailure(lastTexture, lastMediaType, lastMediaInfo, lastErrorMsg);
	}
}

// Begin Destroy. Cleanup 
void UAsyncPhotoGalleryTask::BeginDestroy()
{
	useMediaPlayer = nullptr;
	lastTexture = nullptr;
	Super::BeginDestroy();
}

// Convenience method to broadcast a successful event
void UAsyncPhotoGalleryTask::IsSuccess(UTexture *texture, EPhotoGalleryMediaType MediaType, FPhotoGalleryMediaInfo MediaInfo,
	EPhotoGalleryErrorMessages ErrorMessage = EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_NO_ERROR)
{
	bHasResults = true;
	bIsSuccess = true;
	if (bIsActivated)
	{
		OnSuccess.Broadcast(texture, MediaType, MediaInfo, ErrorMessage);
		RemoveFromRoot();
		SetReadyToDestroy();
		lastTexture = NULL;
	}
	else
	{
		lastTexture = texture;
		lastMediaType = MediaType;
		lastErrorMsg = ErrorMessage;
		lastMediaInfo = MediaInfo;
	}
}

// Convenience method to broadcast a failed event
void UAsyncPhotoGalleryTask::IsFailure(UTexture *texture, EPhotoGalleryMediaType MediaType, FPhotoGalleryMediaInfo MediaInfo,
	EPhotoGalleryErrorMessages ErrorMessage = EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_NO_ERROR)
{
	bHasResults = true;
	bIsSuccess = false;
	if (bIsActivated)
	{
		OnFailure.Broadcast(texture, MediaType, MediaInfo, ErrorMessage);
		RemoveFromRoot();
		SetReadyToDestroy();
		lastTexture = NULL;
	}
	else
	{
		lastTexture = texture;
		lastMediaType = MediaType;
		lastErrorMsg = ErrorMessage;
		lastMediaInfo = MediaInfo;
	}
}


// Opens the photo gallery browser window and returns the selected media as a texture.
//
// Show Photos:
// Allow photos to be listed in the photo gallery window.
//
// Show Videos:
// Allow videos to be listed in the photo gallery window.
//
// Media Player:
// Optional media player object to be used for playing video content selected.
// If no media player object is specified, a new media player will be created internally, however this might incur memory / performance issues over time.
//
// [OUTPUT] Texture:
// If Media Type is Photo, this will be a Texture2D Dynamic object.
// If Media Type is Video, this will be a MediaTexture object.
//
// [OUTPUT] Media Type:
// The type of media selected by the user.
//
// Supported platforms: IOS, Android
UAsyncPhotoGalleryTask* UAsyncPhotoGalleryTask::GetTextureFromPhotoGallery(const UObject* WorldContextObject, bool bShowPhotos, bool bShowVideos, UMediaPlayer *MediaPlayer, bool bAutoplay)
{
	UAsyncPhotoGalleryTask* Task = NewObject<UAsyncPhotoGalleryTask>();
	Task->DoGetTextureFromPhotoGallery(bShowPhotos, bShowVideos, MediaPlayer, bAutoplay);
	return Task;
}



// Opens the photo gallery browser window and returns the selected media as a texture.
//
// Show Photos:
// Allow photos to be listed in the photo gallery window.
//
// Show Videos:
// Allow videos to be listed in the photo gallery window.
//
// Media Player:
// Optional media player object to be used for playing video content selected.
// If no media player object is specified, a new media player will be created internally, however this might incur memory / performance issues over time.
//
// [OUTPUT] Texture:
// If Media Type is Photo, this will be a Texture2D Dynamic object.
// If Media Type is Video, this will be a MediaTexture object.
//
// [OUTPUT] Media Type:
// The type of media selected by the user.
//
// Supported platforms: IOS, Android
void UAsyncPhotoGalleryTask::DoGetTextureFromPhotoGallery(bool bShowPhotos, bool bShowVideos, UMediaPlayer *MediaPlayer, bool bAutoplay)
{
	if (hasPendingRequest)
	{
		IsFailure(nullptr, EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_NONE, FPhotoGalleryMediaInfo(), EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_HAS_PENDING_REQUEST);
		return;
	}
	if (!bShowPhotos && !bShowVideos)
	{
		IsFailure(NULL, EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_NONE, FPhotoGalleryMediaInfo(), EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_NO_MEDIA_TYPE_REQUESTED);
		return;
	}

	hasPendingRequest = true;
	bool bShowBrowser = true;
	bool bGetTexture = true;
	useMediaPlayer = MediaPlayer;
	bAutoplayMedia = bAutoplay;
	if (!useMediaPlayer) bShowVideos = false;

#if PLATFORM_IOS
	[[IOSPhotoGallery GetDelegate] showPhotoGallery:this showPhotos:bShowPhotos showVideos:bShowVideos showBrowser:bShowBrowser returnTexture:bGetTexture];
#endif

#if PLATFORM_ANDROID
	AndroidLastPhotoGalleryTask = this;
	FString storagePath = FPaths::ProjectSavedDir();

	if (UAndroidPermissionFunctionLibrary::CheckPermission("android.permission.READ_EXTERNAL_STORAGE"))
	{
		AndroidThunkCpp_ShowPhotoGallery(storagePath, bShowPhotos, bShowVideos);
	}
	else
	{
		lastStoragePath = storagePath;
		lastShowPhotos = bShowPhotos;
		lastShowVideos = bShowVideos;

		TArray<FString> permissions = { "android.permission.READ_EXTERNAL_STORAGE" };
		UAndroidPermissionCallbackProxy *permissionCallback = UAndroidPermissionFunctionLibrary::AcquirePermissions(permissions);
		permissionCallback->OnPermissionsGrantedDelegate.BindUObject(this, &UAsyncPhotoGalleryTask::DoOnAndroidPermissionGranted);
	}

	
#endif

#if PLATFORM_WINDOWS || PLATFORM_MAC
	FString filename = GetTimestampedFilename(".png", "", "");
	FScreenshotRequest::RequestScreenshot(filename, true, false);

	static FDelegateHandle onScreenshotDelegate;
	onScreenshotDelegate = GEngine->GameViewport->OnScreenshotCaptured().AddLambda(
		[this]
	(int32 width, int32 height, const TArray<FColor >&bitmap)
	{
		if (width > 0 && height > 0 && bitmap.Num() > 0)
		{
			UTexture2DDynamic *returnTex = Texture2DFrom(bitmap, width, height);
			FPhotoGalleryMediaInfo mediaInfo;
			mediaInfo.MediaType = EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_PHOTO;
			mediaInfo.Width = width;
			mediaInfo.Height = height;
			IsSuccess(returnTex, EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_PHOTO, mediaInfo, EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_NO_ERROR);
		}
		else
		{
			IsFailure(NULL, EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_NONE, FPhotoGalleryMediaInfo(), EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_INVALID_MEDIA);
		}
		GEngine->GameViewport->OnScreenshotCaptured().Remove(onScreenshotDelegate);
		onScreenshotDelegate.Reset();
		hasPendingRequest = false;
	});
#endif
}

void UAsyncPhotoGalleryTask::DoOnAndroidPermissionGranted(const TArray<FString>& Permissions, const TArray<bool>& GrantResults)
{
#if PLATFORM_ANDROID
	if (GrantResults.Num() > 0 && GrantResults[0])
	{
		AndroidThunkCpp_ShowPhotoGallery(lastStoragePath, lastShowPhotos, lastShowVideos);
	}
	else
	{
		IsFailure(NULL, EPhotoGalleryMediaType::EPHOTOGALLERYMEDIATYPE_NONE, FPhotoGalleryMediaInfo(), EPhotoGalleryErrorMessages::EPHOTOGALLERYERRORS_NO_PERMISSION);
		hasPendingRequest = false;
	}
#endif
}