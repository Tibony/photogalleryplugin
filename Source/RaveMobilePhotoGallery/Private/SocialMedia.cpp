// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "SocialMedia.h"
#include "RaveMobilePhotoGalleryEnum.h"

//~~~ Image Wrapper ~~~
#include "ImageUtils.h"
#include "IImageWrapper.h"
#include "IImageWrapperModule.h"
//~~~ Image Wrapper ~~~

namespace RaveMobilePhotoGallery
{
	extern bool FColorFrom(UTexture *texture, TArray<FColor> &Results, uint32 &outWidth, uint32 &outHeight);
	extern bool ByteArrayFrom(UTexture *texture, TArray<uint8> &Results, uint32 &outWidth, uint32 &outHeight);

#if PLATFORM_IOS
	// Generate UIImage from a Texture
	extern UIImage *UIImageFrom(UTexture *texture, float opacity, bool premultiplyAlpha);
#endif

#if PLATFORM_ANDROID
	// Copys file from source to destination
	extern bool CopyFile(FString Src, FString Dest);
	extern bool EnsureFileFolderExists(FString File);
	extern FString GetOutputFolderPath(FString fileName, bool bAndroidSaveToExternal, FString AndroidSaveFolder);
	extern FString AndroidTemporaryDirectory();

	// Save byte array to a file (Android version)
	extern bool AndroidSaveArrayToFile(TArrayView<const uint8> Array, FString dest);
#endif

}
using namespace RaveMobilePhotoGallery;

#if PLATFORM_IOS
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SocialMediaText : NSObject<UIActivityItemSource>
@property (nonatomic, copy) NSString *text;
@end

@implementation SocialMediaText
@synthesize text;
- (id) initWithText:(NSString *)aText
{
	if (self == [self init])
	{
		self.text = aText;
	}
	return self;
}

-(void)dealloc
{
	self.text = nil;
	[super dealloc];
}

-(id)activityViewControllerPlaceholderItem : (UIActivityViewController *)activityViewController
{
	return [[NSObject alloc] init];
}

-(id)activityViewController : (UIActivityViewController *)activityViewController
	itemForActivityType : (UIActivityType)activityType
{
	if ( activityType == UIActivityTypePostToTwitter || activityType == UIActivityTypePostToFacebook )
	{
		return self.text;
	}
	return nil;
}

@end

@interface SocialMediaImage : NSObject<UIActivityItemSource>
@property (nonatomic, retain) UIImage *image;
@end

@implementation SocialMediaImage
@synthesize image;

-(id)initWithImage:(UIImage *)aImage
{
	if (self == [self init])
	{
		self.image = aImage;
	}
	return self;
}

-(void)dealloc
{
	self.image = nil;
	[super dealloc];
}

-(id)activityViewControllerPlaceholderItem : (UIActivityViewController *)activityViewController
{
	return self.image;
}

-(id)activityViewController : (UIActivityViewController *)activityViewController
itemForActivityType : (UIActivityType)activityType
{
	return self.image;
}
@end


@interface IOSSocialMedia : NSObject
{
	UIActivityViewController *activityController;
}
@end

@implementation IOSSocialMedia

+ (IOSSocialMedia*)GetDelegate
{
	static IOSSocialMedia* Singleton = [[IOSSocialMedia alloc] init];
	return Singleton;
	//return [[[IOSInstagram alloc] init] autorelease];
}

-(void)dealloc
{
	if (activityController)
	{
		[activityController release];
		activityController = nil;
	}
	[super dealloc];
}

-(void)setupExcludedActivityTypes:(UIActivityViewController *)activityController1
{
	if (activityController1)
	{
		activityController1.excludedActivityTypes = @[UIActivityTypeAddToReadingList,
			UIActivityTypeAirDrop,
			UIActivityTypeAssignToContact,
			UIActivityTypeCopyToPasteboard,
			UIActivityTypeMail,
			UIActivityTypeMessage,
			UIActivityTypeOpenInIBooks,
			//			UIActivityTypePostToFacebook,
			//			UIActivityTypePostToFlickr,
			//			UIActivityTypePostToTencentWeibo,
			//			UIActivityTypePostToTwitter,
			//			UIActivityTypePostToVimeo,
			//			UIActivityTypePostToWeibo,
			UIActivityTypePrint,
			UIActivityTypeSaveToCameraRoll,
			UIActivityTypeMarkupAsPDF,
			@"com.apple.mobilenotes.SharingExtension",
			@"com.apple.reminders.RemindersEditorExtension",
			@"com.google.Drive.ShareExtension",
			@"com.apple.mobileslideshow.StreamShareService"
		];
	}
}

- (bool)UploadImageToSocialMedia:(UAsyncSocialMediaTask *)owner
	image: (UIImage *)srcImage
	caption: (FString)caption
{
	if (srcImage)
	{
		NSString *srcCaption = UE_TO_IOS_STRING(caption);
		SocialMediaImage *image = [[SocialMediaImage alloc] initWithImage:srcImage];
		SocialMediaText *text = [[SocialMediaText alloc] initWithText:srcCaption];
		NSArray *activityItems = @[image, text];

		if (activityController)
		{
			[activityController autorelease];
			activityController = nil;
		}

		activityController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities : nil];
		[self setupExcludedActivityTypes : activityController];

		dispatch_async(dispatch_get_main_queue(), ^{
			[activityController setCompletionWithItemsHandler : ^ (UIActivityType activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
				owner->IsSuccess();
				[activityController autorelease];
				activityController = nil;
			}];
		});
		[[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:activityController animated : YES completion : nil];
		[image autorelease];
		[text autorelease];
		
		return true;
	}
	owner->IsFailure(ESocialMediaErrorMessages::ESOCIALMEDIAERRORS_FILE_NOT_FOUND);
	return false;
}

-(bool)UploadFileToSocialMedia:(UAsyncSocialMediaTask *)owner
	path : (NSString *)srcPath
	caption: (FString)caption
{
	if (![[srcPath pathExtension] isEqualToString:@"mp4"])
	{
		UIImage *srcImage = [UIImage imageWithContentsOfFile : srcPath];
		return[self UploadImageToSocialMedia : owner image : srcImage caption : caption];
	}
	
	NSString *srcCaption = UE_TO_IOS_STRING(caption);
	SocialMediaText *text = [[SocialMediaText alloc] initWithText:srcCaption];
	NSArray *activityItems = @[[NSURL fileURLWithPath:srcPath], text];

	if (activityController)
	{
		[activityController autorelease];
		activityController = nil;
	}

	activityController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities : nil];
	[self setupExcludedActivityTypes : activityController];

	dispatch_async(dispatch_get_main_queue(), ^{
		[activityController setCompletionWithItemsHandler : ^ (UIActivityType activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
			owner->IsSuccess();
			[activityController autorelease];
			activityController = nil;
		}];
	});
	[[[[UIApplication sharedApplication] keyWindow] rootViewController] presentViewController:activityController animated : YES completion : nil];
	//[image autorelease];
	[text autorelease];

	return true;
	owner->IsFailure(ESocialMediaErrorMessages::ESOCIALMEDIAERRORS_FILE_NOT_FOUND);
	return false;
}

@end

#endif

#if PLATFORM_ANDROID
#include "HAL/FileManager.h"
#include "AndroidJNI.h"
#include <jni.h>
#include "AndroidPermissionFunctionLibrary.h"
#include "AndroidPermissionCallbackProxy.h"

// JNI method to save image to the gallery
static jmethodID AndroidThunkJava_ShareToSocialMedia;

static UAsyncSocialMediaTask* AndroidLastSocialMediaTask = nullptr;

static FString lastImagePath;
static FString lastCaption;
static bool lastAndroidSaveToExternal;
static FString lastAndroidSaveFolder;
static UTexture *lastTexture;

// Perform startup functions for the screenshot function
int SetupJNI_RaveMobilePhotoGallery_SocialMedia()
{
	JNIEnv* env = FAndroidApplication::GetJavaEnv();
	if (!env) return JNI_ERR;
	JNIEnv* ENV = env;

	UE_LOG(LogTemp, Log, TEXT("CHECKING: RaveMobilePhotoGallery_ShareToSocialMedia"));
	AndroidThunkJava_ShareToSocialMedia = FJavaWrapper::FindMethod(ENV, FJavaWrapper::GameActivityClassID, "RaveMobilePhotoGallery_ShareToSocialMedia", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z", false);
	if (!AndroidThunkJava_ShareToSocialMedia)
	{
		UE_LOG(LogTemp, Log, TEXT("ERROR: RaveMobilePhotoGallery_ShareToSocialMedia Not Found"));
	}
	
	return JNI_OK;
}

// JNI method to share image to social media
bool AndroidThunkCpp_ShareToSocialMedia(FString imagePath, FString caption, bool bAndroidSaveToExternal, FString AndroidSaveFolder)
{
	if (JNIEnv* Env = FAndroidApplication::GetJavaEnv())
	{
		FString windowTitle = "Share to...";

		jstring Argument1 = Env->NewStringUTF(TCHAR_TO_UTF8(*imagePath));
		jstring Argument2 = Env->NewStringUTF(TCHAR_TO_UTF8(*caption));
		jstring Argument3 = Env->NewStringUTF(TCHAR_TO_UTF8(*windowTitle));
		return FJavaWrapper::CallBooleanMethod(Env, FJavaWrapper::GameActivityThis, AndroidThunkJava_ShareToSocialMedia, Argument1, Argument2, Argument3);
	}
	return false;
}

// Response from the JNI for Start Screen Recording
extern "C" bool Java_com_epicgames_ue4_GameActivity_nativeRaveMobilePhotoGalleryOnShareToSocialMedia(JNIEnv* LocalJNIEnv, jobject LocalThiz, jint success)
{
	int bSuccess = (int)success;
	UE_LOG(LogTemp, Log, TEXT("CHECKING: Java_com_epicgames_ue4_GameActivity_nativeRaveMobilePhotoGalleryOnShareToSocialMedia: %d"), bSuccess);

	if (AndroidLastSocialMediaTask != nullptr)
	{
		if (bSuccess == 1)
		{
			AndroidLastSocialMediaTask->IsSuccess();
		}
		else
		{
			AndroidLastSocialMediaTask->IsFailure(ESOCIALMEDIAERRORS_FILE_NOT_FOUND);
		}
		AndroidLastSocialMediaTask = nullptr;
	}
	return true;
}

#endif


// Constructor
UAsyncSocialMediaTask::UAsyncSocialMediaTask(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
	if (HasAnyFlags(RF_ClassDefaultObject) == false)
	{
		AddToRoot();
	}
}

// Activate
void UAsyncSocialMediaTask::Activate()
{
	Super::Activate();
	bIsActivated = true;
	if (bHasResults)
	{
		if (bIsSuccess) IsSuccess();
		else IsFailure(lastErrorMsg);
	}
}

// Begin Destroy. Cleanup 
void UAsyncSocialMediaTask::BeginDestroy()
{
	Super::BeginDestroy();
}

// Convenience method to broadcast a successful event
void UAsyncSocialMediaTask::IsSuccess()
{
	bHasResults = true;
	bIsSuccess = true;
	if (bIsActivated)
	{
		AsyncTask(ENamedThreads::GameThread, [this]() {
			OnSuccess.Broadcast(ESocialMediaErrorMessages::ESOCIALMEDIAERRORS_NO_ERRORS);
			RemoveFromRoot();
			SetReadyToDestroy();
		});
#if PLATFORM_ANDROID
		lastTexture = nullptr;
#endif
	}
}

// Convenience method to broadcast a failed event
void UAsyncSocialMediaTask::IsFailure(ESocialMediaErrorMessages ErrorMessage = ESocialMediaErrorMessages::ESOCIALMEDIAERRORS_NO_ERRORS)
{
	bHasResults = true;
	bIsSuccess = false;
	lastErrorMsg = ErrorMessage;

	if (bIsActivated)
	{
		AsyncTask(ENamedThreads::GameThread, [this]() {
			OnFailure.Broadcast(lastErrorMsg);
			RemoveFromRoot();
			SetReadyToDestroy();
		});
#if PLATFORM_ANDROID
		lastTexture = nullptr;
#endif
	}
}

UAsyncSocialMediaTask* UAsyncSocialMediaTask::UploadFileToSocialMedia(const UObject* WorldContextObject, FString imagePath, FString caption)
{
	bool bAndroidSaveToExternal = false;
	FString AndroidSaveFolder = "";

	UAsyncSocialMediaTask* Task = NewObject<UAsyncSocialMediaTask>();
	Task->DoUploadFileToSocialMedia(imagePath, caption, bAndroidSaveToExternal, AndroidSaveFolder);
	return Task;
}

bool UAsyncSocialMediaTask::DoUploadFileToSocialMedia(FString imagePath, FString caption, bool bAndroidSaveToExternal, FString AndroidSaveFolder)
{
#if PLATFORM_IOS
//	NSString *srcPath = UE_TO_IOS_STRING(imagePath);
//	UIImage *srcImage = [UIImage imageWithContentsOfFile : srcPath];
//	return [[IOSSocialMedia GetDelegate] UploadFileToSocialMedia:this image:srcImage caption : caption];

	NSString *srcPath = UE_TO_IOS_STRING(imagePath);
	return [[IOSSocialMedia GetDelegate] UploadFileToSocialMedia:this path:srcPath caption : caption];
#endif

#if PLATFORM_ANDROID
	AndroidLastSocialMediaTask = this;
	lastImagePath = imagePath;
	lastCaption = caption;
	lastAndroidSaveToExternal = bAndroidSaveToExternal;
	lastAndroidSaveFolder = AndroidSaveFolder;
	lastTexture = nullptr;

	if (UAndroidPermissionFunctionLibrary::CheckPermission("android.permission.READ_EXTERNAL_STORAGE") &&
		UAndroidPermissionFunctionLibrary::CheckPermission("android.permission.WRITE_EXTERNAL_STORAGE"))
	{
		FString imageExt = FPaths::GetExtension(lastImagePath, true);
		FString tempDir = RaveMobilePhotoGallery::AndroidTemporaryDirectory(); // RaveMobilePhotoGallery::GetOutputFolderPath("", bAndroidSaveToExternal, AndroidSaveFolder);
		FString tempFile = FPaths::CreateTempFilename(*tempDir, TEXT(""), *imageExt);

		bool bEnsureFolder = RaveMobilePhotoGallery::EnsureFileFolderExists(tempFile);
		bool bCopyFile = RaveMobilePhotoGallery::CopyFile(lastImagePath, tempFile);
		return AndroidThunkCpp_ShareToSocialMedia(tempFile, lastCaption, lastAndroidSaveToExternal, lastAndroidSaveFolder);
	}
	else
	{
		TArray<FString> permissions = { "android.permission.READ_EXTERNAL_STORAGE", "android.permission.WRITE_EXTERNAL_STORAGE" };
		UAndroidPermissionCallbackProxy *permissionCallback = UAndroidPermissionFunctionLibrary::AcquirePermissions(permissions);
		permissionCallback->OnPermissionsGrantedDelegate.BindUObject(this, &UAsyncSocialMediaTask::DoOnAndroidPermissionGranted);
	}
	return false;
#endif

#if PLATFORM_WINDOWS

#endif

	IsFailure(ESocialMediaErrorMessages::ESOCIALMEDIAERRORS_NOT_INSTALLED);
	return false;
}

void UAsyncSocialMediaTask::DoOnAndroidPermissionGranted(const TArray<FString>& Permissions, const TArray<bool>& GrantResults)
{
#if PLATFORM_ANDROID
	if (GrantResults.Num() > 0 && GrantResults[0])
	{
		DoUploadFileToSocialMedia(lastImagePath, lastCaption, lastAndroidSaveToExternal, lastAndroidSaveFolder);
	}
	else
	{
		IsFailure(ESOCIALMEDIAERRORS_NO_PERMISSION);
	}
#endif
}