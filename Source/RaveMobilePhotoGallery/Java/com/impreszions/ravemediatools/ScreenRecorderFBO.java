// Copyright 2018 Impreszions Solutions All Rights Reserved.
// Author : MunHon Chan

// Reference From: https://raw.githubusercontent.com/mstorsjo/android-decodeencodetest/master/src/com/example/decodeencodetest/ExtractDecodeEditEncodeMuxTest.java

package com.impreszions.ravemediatools;

import android.media.AudioFormat; 
import android.media.AudioRecord; 
import android.media.MediaCodec; 
import android.media.MediaCodecInfo; 
import android.media.MediaFormat; 
import android.media.MediaMuxer; 
import android.media.MediaRecorder; 
import android.media.MediaExtractor;
import android.media.MediaCodecList;
import android.media.MediaCodec.BufferInfo;

import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.graphics.Bitmap;

import android.util.Log;
import android.view.Surface;
import android.app.Activity;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicReference;
import java.util.LinkedList;
import java.util.Arrays;
import android.opengl.EGL14;
import android.opengl.EGLSurface;
import android.opengl.GLES20;
import android.opengl.GLES30;
import android.graphics.Rect;
import android.opengl.EGLConfig;
import android.opengl.EGLContext;
import android.opengl.EGLDisplay;
import android.opengl.EGLExt;
import android.opengl.EGLSurface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.graphics.Canvas;

import java.nio.FloatBuffer;
import android.graphics.SurfaceTexture;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.Matrix;
import java.nio.ByteOrder;
import android.os.Bundle;

public class ScreenRecorderFBO extends ScreenRecorder
{
	public native int nativeSetRenderSurface(Surface surface);
	public native android.opengl.EGLDisplay nativeGetRenderEGLDisplay();
	public native int nativeGetRenderEGLSurface();

    private static final String TAG = "ScreenRecorder";
    private static final boolean VERBOSE = true; // lots of logging
    private static final int TIMEOUT_USEC = 1000;

	// parameters for the encoder
    private static final String MIME_TYPE = "video/avc";    // H.264 Advanced Video Coding
    private static final int FRAME_RATE = 30;               // 15fps
    private static final int IFRAME_INTERVAL = 10;          // 10 seconds between I-frames

    // parameters for the audio encoder
    private static final String OUTPUT_AUDIO_MIME_TYPE = "audio/mp4a-latm"; // Advanced Audio Coding
    private static final int OUTPUT_AUDIO_CHANNEL_COUNT = 2; // Must match the input stream.
    private static final int OUTPUT_AUDIO_BIT_RATE = 128 * 1024;
    private static final int OUTPUT_AUDIO_AAC_PROFILE = MediaCodecInfo.CodecProfileLevel.AACObjectHE;
    private static final int OUTPUT_AUDIO_SAMPLE_RATE_HZ = 44100; // Must match the input stream.

    private MediaFormat mEncoderOutputVideoFormat = null;
    private MediaFormat mEncoderOutputAudioFormat = null;
	private MediaExtractor mVideoExtractor = null;
    private MediaExtractor mAudioExtractor = null;
    private MediaCodec mVideoEncoder = null;
    private MediaCodec mAudioEncoder = null;
    private MediaMuxer mMuxer = null;
	private long startWhen = 0;
	private long timeOffset = 0;
	private long pauseWhen = 0;

    // We will determine these once we have the output format.
    private int mOutputVideoTrack = -1;
    private int mOutputAudioTrack = -1;

    // Whether things are done on the video side.
    private boolean mVideoExtractorDone = false;
    private boolean mVideoDecoderDone = false;
    private boolean mVideoEncoderDone = false;
	private boolean mVideoEncoderReady = false;

    // Whether things are done on the audio side.
    private boolean mAudioExtractorDone = false;
    private boolean mAudioDecoderDone = false;
    private boolean mAudioEncoderDone = false;
	private boolean mAudioEncoderReady = false;

    public boolean mMuxerReady = false;
    private int mVideoEncodedFrameCount = 0;
    private int mAudioEncodedFrameCount = 0;

    private boolean mCopyVideo = true;
    private boolean mCopyAudio = true;
    private int mWidth = -1;
    private int mHeight = -1;
    private String mOutputFile;

	private int mVideoColorFormat;
	private boolean mRecordingStopped = false;
	private boolean mVideoStopped = false;
	private boolean mAudioStopped = false;
	private java.nio.ByteBuffer imageBuffer;
	private int imageSize;

	private boolean mRunning = false;
	private boolean mExecuting = false;
	private BufferInfo mBufferInfoVideo;
	private BufferInfo mBufferInfoAudio;
	private EGLSurface mUE4Surface = EGL14.EGL_NO_SURFACE;
	private int mUE4SurfaceWidth;
	private int mUE4SurfaceHeight;
	private int mUE4SurfaceRotation = 0;
	private Rect mVideoRect;

	private final boolean bUseSurface = true;
	private FBORenderThread renderThread;

	@Override
    public void StartRecording(int width, int height, String filename, int rotation, int offsetWidth, int offsetHeight, boolean DoSaveToGalleryOnComplete) //, SurfaceView MySurfaceView)
	{
		if ( !mExecuting )
		{
			if ( width > height ) // landscape
			{
				width = 1280;
				height = 720;
			}
			else
			{
				width = 720;
				height = 1280;
			}

			mExecuting = true;
			mUE4SurfaceWidth = width;
			mUE4SurfaceHeight = height;
			mUE4SurfaceRotation = rotation;
			setSize(width, height);
			mOutputFile = filename;
			mRunning = true;
			mRecordingStopped = false;
			mVideoStopped = false;
			mAudioStopped = false;
			prevOutputPTSUs = 0;
			CAPTURE_FILENAME = filename;
			SaveToGalleryOnComplete = DoSaveToGalleryOnComplete;

			String RendererString = GLES20.glGetString(GLES20.GL_RENDERER);
			Log.d(TAG,"--- muxer screen rendererstring:  " + RendererString);

			try {
				SetupRecording();
				showScreenRecorderOverlay();
				run();
				//renderThread = new FBORenderThread(this);
				//renderThread.start();

			} catch(Exception e) {

			}
			/*
			gameActivity.runOnUiThread(new Runnable() {
				public void run() {
						try {
							SetupRecording(mUE4SurfaceRotation);
							showScreenRecorderOverlay();
						} catch(Exception e) {
							Log.e(TAG, "Error starting recording: " + e);
						}
						renderThread.start();
					}
			});
			*/
		}
	}

	private void SetupRecording() throws Exception
	{
		startWhen = System.nanoTime();
		timeOffset = 0;
		pauseWhen = 0;

		mBufferInfoVideo = new MediaCodec.BufferInfo();
		mBufferInfoAudio = new MediaCodec.BufferInfo();
        Exception exception = null;

        mMuxerReady = false;
        mEncoderOutputVideoFormat = null;
        mEncoderOutputAudioFormat = null;
        mOutputVideoTrack = -1;
        mOutputAudioTrack = -1;
        mVideoEncoderDone = false;
        mAudioEncoderDone = false;
        mVideoEncodedFrameCount = 0;
        mAudioEncodedFrameCount = 0;

        try {
            mMuxer = createMuxer();
			mMuxer.setOrientationHint(mUE4SurfaceRotation);
			mVideoEncoderReady = false;
			mAudioEncoderReady = false;

            if (mCopyVideo)
			{
				mVideoEncoderDone = false;
				mVideoEncoder = createVideoEncoder();
            }
			else
			{
				mVideoEncoderDone = true;
			}
		
            if (mCopyAudio)
			{
				mAudioEncoderDone = false;
                mAudioEncoder = createAudioEncoder();
            }
			else
			{
				mAudioEncoderDone = true;
			}
        } finally {

        }

        if (exception != null) {
            throw exception;
        }
    }

	@Override
	public void StopRecording()
	{
		//if ( mExecuting )
		//{
			hideScreenRecorderOverlay();
			mRecordingStopped = true;
			//mRunning = false;
			Log.d(TAG, "muxer: stopping recording and watiing");
		//}
	}

	@Override
	public boolean IsRecording()
	{
		return mRunning;
	}

	private void Cleanup() 
	{
		try
		{
			if ( (!mCopyVideo || mVideoEncoderDone) && (!mCopyAudio || mAudioEncoderDone ) )
			{
				Log.d(TAG, "muxer: cleanup");
				Exception exception = null;
			
				try {
					if (mMuxer != null) {
						mMuxer.stop();
						mMuxer.release();
					}
				} catch(Exception e) {
					Log.e(TAG, "error while releasing muxer", e);
					if (exception == null) {
						exception = e;
					}
				}

				if ( mVideoEncoder != null ) mVideoEncoder.release();
				if ( mAudioEncoder != null ) mAudioEncoder.release();
				if ( mInputSurface != null ) mInputSurface.release();

				mInputSurface = null;
				mVideoEncoder = null;
				mAudioEncoder = null;
				mEncoderOutputVideoFormat = null;
				mEncoderOutputAudioFormat = null;
				mMuxer = null;

				mMuxerReady = false;
				mExecuting = false;

				//if ( renderThread != null ) renderThread.stop();
				//renderThread = null;


				if (exception != null) {
					throw exception;
				}

				AddToGallery();
				mRunning = false;
			}
			else
			{
				Log.d(TAG, "muxer: cleanup not ready");
			}
		}
		catch(Exception e)
		{
			Log.d(TAG, "muxer: error cleaning up: " + e);
		}
		finally
		{
			nativeStopScreenRecordingResponse(1,0,0,0);
		}
		mExecuting = false;
	}

	public class FBORenderThread extends Thread
	{
		private ScreenRecorderFBO fbo;

		public FBORenderThread(ScreenRecorderFBO _fbo)
		{
			fbo = _fbo;
		}

		@Override
		public void run() {
			Looper.prepare();
			fbo.run();
		}

		public void prepareStop()
		{
			fbo = null;
		}
	}

	public void run()
	{
		try {
//			SetupRecording();
//			showScreenRecorderOverlay();

			Log.d(TAG, "muxer: Starting run");
			mRunning = true;
			while (mRunning) {
				render();
			}
			mRecordingStopped = true;
			render();
			Cleanup();
			if ( renderThread != null )
			{
				renderThread.prepareStop();
				renderThread.stop();
				renderThread = null;
			}
			Log.d(TAG, "muxer: Stopping run");
		}
		catch(Exception e)
		{

		}
		finally
		{

		}
	}

	@SuppressWarnings("deprecation")
    private void render()
	{
		if ( mCopyVideo )
		{
			if ( mVideoEncoder != null ) renderVideo();
		}

		if ( mCopyAudio )
		{
			if ( !mCopyVideo || mVideoEncoderReady )
			{
				if ( mAudioEncoder != null ) renderAudio();
			}
		}
    }

// Muxer
	public void setupMuxer() {
        Log.d(TAG, "muxer: setting up.");
        if (!mMuxerReady)
		{
			if ( mCopyVideo && (mEncoderOutputVideoFormat == null) )
			{
                Log.d(TAG, "muxer: video track not ready");
				return;
			}
			if ( mCopyAudio && (mEncoderOutputAudioFormat == null) )
			{
                Log.d(TAG, "muxer: audio track not ready");
				return;
			}

            Log.d(TAG, "muxer: adding video track.");
            if ( mCopyVideo ) mOutputVideoTrack = mMuxer.addTrack(mEncoderOutputVideoFormat);
            Log.d(TAG, "muxer: adding audio track.");
            if ( mCopyAudio )
			{
				mOutputAudioTrack = mMuxer.addTrack(mEncoderOutputAudioFormat);
				if ( mAudioRecorder != null ) mAudioRecorder.startRecording();
			}

			startWhen = System.nanoTime();
            Log.d(TAG, "muxer: starting");
            mMuxer.start();
            mMuxerReady = true;
        }
		else
		{
	        Log.d(TAG, "muxer: setup failed. already muxing");
		}
    }
	
	private MediaMuxer createMuxer() throws IOException {
        return new MediaMuxer(mOutputFile, MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4);
//         return new MediaMuxer(mOutputFile, MediaMuxer.OutputFormat.MUXER_OUTPUT_3GPP);
   }

	 private static boolean isVideoFormat(MediaFormat format) {
        return getMimeTypeFor(format).startsWith("video/");
    }

    private static boolean isAudioFormat(MediaFormat format) {
        return getMimeTypeFor(format).startsWith("audio/");
    }

    private static String getMimeTypeFor(MediaFormat format){ 
        return format.getString(MediaFormat.KEY_MIME);
    }

    private static MediaCodecInfo selectCodec(String mimeType)
	{
        int numCodecs = MediaCodecList.getCodecCount();
		boolean isVideoMime = mimeType.startsWith("video/");
		boolean isAudioMime = mimeType.startsWith("audio/");

		MediaCodecInfo firstCodec = null;

        for (int i = 0; i < numCodecs; i++) {
            MediaCodecInfo codecInfo = MediaCodecList.getCodecInfoAt(i);

            if (!codecInfo.isEncoder()) {
                continue;
            }

            String[] types = codecInfo.getSupportedTypes();
            for (int j = 0; j < types.length; j++) {
                if (types[j].equalsIgnoreCase(mimeType)) {
                    return codecInfo;
                }
				else if (firstCodec == null)
				{
					if ( isVideoMime && types[j].startsWith("video/") )
					{
						firstCodec = codecInfo;
					}
					else if  ( isAudioMime && types[j].startsWith("audio/") )
					{
						firstCodec = codecInfo;
					}
				}
            }
        }

        return null; //firstCodec;
    }

	private void logState()
	{
	}

// Video
	public void renderVideo()
	{
		if (VERBOSE) Log.d(TAG, "muxer video: rendering video");
		if ( mRecordingStopped ) 
		{
			if ( !mVideoStopped ) 
			{
				if (VERBOSE) Log.d(TAG, "muxer video: signalling end of video stream");
				mVideoEncoder.signalEndOfInputStream();
				mVideoStopped = true;
			}
		}
		if ( mEncoderOutputVideoFormat == null )
		{
			mEncoderOutputVideoFormat = mVideoEncoder.getOutputFormat();
			if ( mEncoderOutputVideoFormat != null )
			{
				//nativeSetRenderSurface(mInputSurface);
				if (VERBOSE) Log.d(TAG, "muxer video: no video format, settingup");
				setupMuxer();
				mVideoEncoderReady = true;
			}
		}
		else
		{
			//mVideoEncoderReady = true;
			int status = mVideoEncoder.dequeueOutputBuffer(mBufferInfoVideo, TIMEOUT_USEC);

			if (status == MediaCodec.INFO_TRY_AGAIN_LATER)
			{
				//Bundle params = new Bundle();
				//params.putInt(MediaCodec.PARAMETER_KEY_REQUEST_SYNC_FRAME, 0);
				//params.putInt(MediaCodec.PARAMETER_KEY_SUSPEND, 0);
				//mVideoEncoder.setParameters(params);

				// no output available yet
				if (VERBOSE) Log.d(TAG, "muxer video: no output from encoder available");
				//encoderOutputAvailable = false;

			} else if (status == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
				// not expected for an encoder
				ByteBuffer[] encoderOutputBuffers = mVideoEncoder.getOutputBuffers();
				if (VERBOSE) Log.d(TAG, "muxer video: encoder output buffers changed");

			} else if (status == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
				// not expected for an encoder
				if ( mEncoderOutputVideoFormat == null )
				{
					mEncoderOutputVideoFormat = mVideoEncoder.getOutputFormat();
					if ( mEncoderOutputVideoFormat != null )
					{
						//nativeSetRenderSurface(mInputSurface);
						setupMuxer();
						mVideoEncoderReady = true;
					}
				}
				if (VERBOSE) Log.d(TAG, "muxer video: encoder output format changed: " + mEncoderOutputVideoFormat);

			} else if (status < 0) {
				if (VERBOSE) Log.d(TAG, "muxer video: unexpected result from encoder.dequeueOutputBuffer: " + status);

			} else if (status >= 0)
			{
				Log.d(TAG, "muxer video: Starting video run: " + status);
				// encoded sample
				ByteBuffer data = mVideoEncoder.getOutputBuffer(status);
				//onEncodedSample(status, mVideoEncoder, mBufferInfo, data);
				int index = status;
				MediaCodec codec = mVideoEncoder;
				MediaCodec.BufferInfo info = mBufferInfoVideo;
				ByteBuffer encoderOutputBuffer = data;
				if (VERBOSE) {
					Log.d(TAG, "muxer video: returned buffer of size " + info.size);
				}

				if ((info.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0)
				{
					if (VERBOSE) Log.d(TAG, "muxer video: codec config buffer");
					// Simply ignore codec config buffers.
					codec.releaseOutputBuffer(index, false);
					return;
				}

				// It's usually necessary to adjust the ByteBuffer values to match BufferInfo.
				encoderOutputBuffer.position(info.offset);
				encoderOutputBuffer.limit(info.offset + info.size);

				if (info.size != 0)
				{
					info.presentationTimeUs = getPresentationTime();
					mMuxer.writeSampleData(mOutputVideoTrack, encoderOutputBuffer, info);
					//prevOutputPTSUs = info.presentationTimeUs;
					if (VERBOSE) {
						Log.d(TAG, "muxer video: returned buffer for time " + info.presentationTimeUs);
					}
					//if ( mAudioEncoder != null ) renderAudio(ptsUsec);
				}
				codec.releaseOutputBuffer(index, false);//info.presentationTimeUs);
				mVideoEncodedFrameCount++;
				if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0)
				{
					if (VERBOSE) Log.d(TAG, "muxer video: EOS");
					//synchronized (this) {
						mVideoEncoderDone = true;
					//	notifyAll();
						Cleanup();
					//}
				}
				else
				{
					//if ( mAudioEncoder != null ) renderAudio(ptsUsec);
				}
			}
		}
	}

	private Canvas mCanvas;

	@Override
	public void renderFrame(int mOffscreenTexture, int mSurfaceWidth, int mSurfaceHeight,  android.opengl.EGLDisplay display, android.opengl.EGLSurface surface)
	{
		/*if ( mOffscreenTexture == 0 )
		{
			if ( mInputSurface )
			{
				mCanvas = mInputSurface.lockHardwareCanvas ();
			}
		}
		else if ( mOffscreenTexture == 1 )
		{
			if ( mInputSurface )
			{
				mInputSurface.unlockCanvasAndPost(mCanvas);
				mCanvas = null;
			}
		}
		else
		*/
		if ( mOffscreenTexture == 1 )
		{
/*			if ( mInputSurface )
			{
				mInputSurface.unlockCanvasAndPost(mCanvas);
				mCanvas = null;
			}
*/
			Log.d(TAG, "muxer video creating STextureRender");
			if ( mTextureRender == null ) mTextureRender =  new STextureRender();
			mTextureRender.surfaceCreated();
		}
		else
		{
			synchronized(this)
			{
				if ( mVideoEncoderReady )
				{
					Log.d(TAG, "muxer video renderframe: draw frame " + mSurfaceWidth + " , " + mSurfaceHeight + " = " + mWidth + " , " + mHeight);
					/*GLES20.glClearColor(1.0f, 0.0f, 0.0f, 1.0f);    // again, only really need to
					GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);     //  clear pixels outside rect
					GLES20.glViewport(mVideoRect.left, mVideoRect.top, mVideoRect.width(), mVideoRect.height());

					GLES20.glBindFramebuffer(GLES20.GL_FRAMEBUFFER, 0);
					//GlUtil.checkGlError("glBindFramebuffer");

					mFullScreen.drawFrame(mOffscreenTexture, mIdentityMatrix);
					*/

					//EGLExt.eglPresentationTimeANDROID(display, surface, getPresentationTime());
					GLES20.glClearColor(1.0f, 0.0f, 0.0f, 1.0f);    // again, only really need to
					GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);     //  clear pixels outside rect
					//GLES30.glBlitFramebuffer(0, 0, mSurfaceWidth, mSurfaceHeight, 0, 0, mWidth, mHeight,
					//	GLES30.GL_COLOR_BUFFER_BIT, GLES30.GL_NEAREST);
					int err;
					if ((err = GLES30.glGetError()) != GLES30.GL_NO_ERROR) {
						Log.w(TAG, "ERROR: glBlitFramebuffer failed: 0x" + Integer.toHexString(err));
					}
				}
				else
				{
					Log.d(TAG, "muxer video renderframe: video encoder not ready");
				}
			}
		}
	}

	private Surface mInputSurface;
    private MediaCodec createVideoEncoder() throws IOException
	{
		int mBitRate = 4000000; ///16 * 512 * 1024; //2048000; //96000;
		MediaCodecInfo codecInfo = selectCodec(MIME_TYPE);
		if (codecInfo == null) {
			// Don't fail CTS if they don't have an AVC codec (not here, anyway).
			Log.e(TAG, "Unable to find an appropriate codec for " + MIME_TYPE);
			return null;
		}
		if (VERBOSE) Log.d(TAG, "found codec: " + codecInfo.getName());
		int colorFormat = bUseSurface ? MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface : selectColorFormat(codecInfo, MIME_TYPE); //MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface;
		//int colorFormat = selectColorFormat(codecInfo, MIME_TYPE); //MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface;
		if (VERBOSE) Log.d(TAG, "found colorFormat: " + colorFormat);

		// We avoid the device-specific limitations on width and height by using values that
		// are multiples of 16, which all tested devices seem to be able to handle.

		mWidth = 640;
		mHeight = 384;

		MediaFormat format = MediaFormat.createVideoFormat(MIME_TYPE, mWidth, mHeight);
		// Set some properties.  Failing to specify some of these can cause the MediaCodec
		// configure() call to throw an unhelpful exception.

		format.setString(MediaFormat.KEY_MIME, MIME_TYPE);
		format.setInteger(MediaFormat.KEY_WIDTH, mWidth);
		format.setInteger(MediaFormat.KEY_HEIGHT, mHeight);
		format.setInteger(MediaFormat.KEY_COLOR_FORMAT, colorFormat);
		format.setInteger(MediaFormat.KEY_FRAME_RATE, 30);
		format.setInteger(MediaFormat.KEY_CAPTURE_RATE, 30);
		format.setInteger(MediaFormat.KEY_BIT_RATE, mBitRate);
		format.setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 1);
		format.setInteger(MediaFormat.KEY_INTRA_REFRESH_PERIOD, 1000);
		format.setInteger("latency", 0);
		format.setInteger(MediaFormat.KEY_REPEAT_PREVIOUS_FRAME_AFTER, 1000);
		//format.setInteger(MediaFormat.KEY_MAX_INPUT_SIZE, mBitRate * mWidth * mHeight);
		format.setInteger(MediaFormat.KEY_LEVEL, MediaCodecInfo.CodecProfileLevel.AVCProfileHigh);

		if (VERBOSE) Log.d(TAG, "format: " + format);
		mVideoColorFormat = colorFormat;

		Log.d(TAG, "video encoder 1");
		MediaCodec encoder = MediaCodec.createByCodecName(codecInfo.getName());
		try {
			Log.d(TAG, "video encoder 2");
			encoder.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);

			Log.d(TAG, "video encoder 3");
			if ( bUseSurface )
			{	
				Log.d(TAG, "video encoder 4");
				//mInputSurface = encoder.createInputSurface(); //encoder.createPersistentInputSurface();
				mInputSurface = encoder.createPersistentInputSurface();
				Log.d(TAG, "video encoder 5");
				encoder.setInputSurface(mInputSurface);//createInputSurface();
	//			nativeSetRenderSurface(mInputSurface);
			}
	
			Log.d(TAG, "video encoder 6");
			encoder.start();

			Log.d(TAG, "video encoder 6a");
			encoder.flush();


			Log.d(TAG, "video encoder 7");
			nativeSetRenderSurface(mInputSurface);

			if (VERBOSE) Log.d(TAG, "video encoder: created successfully");
		} catch(MediaCodec.CodecException e) {
			Log.e(TAG, "I got an error: " + e.getErrorCode(), e);
		}
        return encoder;
    }

// Audio
	private AudioRecord mAudioRecorder;
	private ByteBuffer mPcmBuffer;
	private int PCM_MIN_BUFFER_SIZE;

    private MediaCodec createAudioEncoder() throws IOException
	{
		MediaCodecInfo codecInfo = selectCodec(OUTPUT_AUDIO_MIME_TYPE);
		if (codecInfo == null) {
			// Don't fail CTS if they don't have an AAC codec (not here, anyway).
			Log.e(TAG, "Unable to find an appropriate codec for " + OUTPUT_AUDIO_MIME_TYPE);
			return null;
		}
		int mChannels = 1; //AudioFormat.CHANNEL_IN_STEREO;
		int mEncoding = AudioFormat.ENCODING_PCM_16BIT;
		int mSampleRate = selectAudioSamplingRate(mChannels, mEncoding);
		int mBitRate = mSampleRate * mChannels * 16; //6000000;
		//int sampleRate = format.getInteger(MediaFormat.KEY_SAMPLE_RATE);
		//int channelCount = format.getInteger(MediaFormat.KEY_CHANNEL_COUNT);
		//int inBitrate = sampleRate * channelCount * 16;  // bit/sec
		//int outBitrate = format.getInteger(MediaFormat.KEY_BIT_RATE);
		 
		//private static final int SAMPLE_RATE = 44100;	// 44.1[KHz] is only setting guaranteed to be available on all devices.
		//private static final int BIT_RATE = 64000;
		int SAMPLES_PER_FRAME = 1024;	// AAC, bytes/frame/channel
		int FRAMES_PER_BUFFER = 25; 	// AAC, frame/buffer/sec


		if (VERBOSE) Log.w(TAG, "audio found codec: " + codecInfo.getName() + " sample rate: " + mSampleRate + " bit rate: " + mBitRate);

        MediaFormat format = MediaFormat.createAudioFormat(OUTPUT_AUDIO_MIME_TYPE, mSampleRate, mChannels); //OUTPUT_AUDIO_CHANNEL_COUNT);
		//format.setInteger(MediaFormat.KEY_PCM_ENCODING, mEncoding);
        format.setInteger(MediaFormat.KEY_BIT_RATE, mBitRate);
        format.setInteger(MediaFormat.KEY_AAC_PROFILE, OUTPUT_AUDIO_AAC_PROFILE);
		//format.setInteger(MediaFormat.KEY_AAC_SBR_MODE, 0);

        MediaCodec encoder = MediaCodec.createByCodecName(codecInfo.getName());
        encoder.configure(format, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE);
        encoder.start();
	
		int sampleRate = mSampleRate; //selectAudioSamplingRate();
            
        PCM_MIN_BUFFER_SIZE = AudioRecord.getMinBufferSize(sampleRate, mChannels, mEncoding);
        final int minBufferSize = AudioRecord.getMinBufferSize(sampleRate, mChannels, mEncoding);
            
        mPcmBuffer = ByteBuffer.allocateDirect(minBufferSize);            
        mAudioRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC, sampleRate, mChannels, mEncoding, mPcmBuffer.capacity()); 

		try {
            if (mAudioRecorder.getState() != AudioRecord.STATE_INITIALIZED) {
                throw new IllegalStateException("AudioRecord instance failed to initialize, state is (" + mAudioRecorder.getState() + ")");
            }
			else
			{
				//mAudioRecorder.startRecording();
			}
		} catch (Exception e) {

		}
		// Create the Visualizer object and attach it to our media player.
        return encoder;
    }

	public void renderAudio()
	{
		Log.w(TAG, "muxer audio: render audio");
		readAudioRecorder();

		if ( mEncoderOutputAudioFormat == null )
		{
			mEncoderOutputAudioFormat = mAudioEncoder.getOutputFormat();
			if ( mEncoderOutputAudioFormat != null )
			{
				mAudioEncoderReady = true;
				setupMuxer();
				//if ( mAudioRecorder != null ) mAudioRecorder.startRecording();
				if (VERBOSE) Log.d(TAG, "muxer audio: audio encoder output format changed: " + mEncoderOutputAudioFormat);
			}
		}
		int status = mAudioEncoder.dequeueOutputBuffer(mBufferInfoAudio, TIMEOUT_USEC);
		if (status == MediaCodec.INFO_TRY_AGAIN_LATER) {
			// no output available yet
			if (VERBOSE) Log.d(TAG, "muxer audio: no output from audio encoder available");

		} else if (status == MediaCodec.INFO_OUTPUT_BUFFERS_CHANGED) {
			// not expected for an encoder
			ByteBuffer[] encoderOutputBuffers = mAudioEncoder.getOutputBuffers();
			if (VERBOSE) Log.d(TAG, "muxer audio: audio encoder output buffers changed");

		} else if (status == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
			// not expected for an encoder
			if ( mEncoderOutputAudioFormat == null )
			{
				mEncoderOutputAudioFormat = mAudioEncoder.getOutputFormat();
				if ( mEncoderOutputAudioFormat != null )
				{
					mAudioEncoderReady = true;
					setupMuxer();
					//if ( mAudioRecorder != null ) mAudioRecorder.startRecording();
					if (VERBOSE) Log.d(TAG, "muxer audio: audio encoder output format changed: " + mEncoderOutputAudioFormat);
				}
			}
		} else if (status < 0) {
			if (VERBOSE) Log.d(TAG, "muxer audio: unexpected result from audio encoder.dequeueOutputBuffer: " + status);

		} else if (status >= 0) {
			Log.d(TAG, "muxer audio: Starting Run: " + status);
			// encoded sample
			ByteBuffer data = mAudioEncoder.getOutputBuffer(status);
			//onEncodedSample(status, mAudioEncoder, mBufferInfo, data);

			int index = status;
			MediaCodec codec = mAudioEncoder;
			MediaCodec.BufferInfo info = mBufferInfoAudio;
			ByteBuffer encoderOutputBuffer = data;

			if ((info.flags & MediaCodec.BUFFER_FLAG_CODEC_CONFIG) != 0) {
				if (VERBOSE) Log.d(TAG, "muxer audio: : codec config buffer");
				// Simply ignore codec config buffers.
				codec.releaseOutputBuffer(index, false);
				return;
			}

			if (VERBOSE) {
				Log.d(TAG, "muxer audio: : returned buffer for time " + info.presentationTimeUs);
			}

			// It's usually necessary to adjust the ByteBuffer values to match BufferInfo.
			encoderOutputBuffer.position(info.offset);
			encoderOutputBuffer.limit(info.offset + info.size);

			if (info.size != 0)
			{
				//info.presentationTimeUs = getPresentationTime();
				mMuxer.writeSampleData(mOutputAudioTrack, encoderOutputBuffer, info);
				//prevOutputPTSUs = info.presentationTimeUs;
				if (VERBOSE) {
					Log.d(TAG, "muxer audio:  returned buffer for time " + info.presentationTimeUs);
				}
			}
			codec.releaseOutputBuffer(index, false);//info.presentationTimeUs);
			mAudioEncodedFrameCount++;
			if ((info.flags & MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0)
			{
				if (VERBOSE) Log.d(TAG, "muxer audio: : EOS");
				//synchronized (this) {
					mAudioEncoderDone = true;
					//notifyAll();
					Cleanup();
				//}
			}
		}
	}

	public void readAudioRecorder()
	{
		if ( mRecordingStopped )
		{
			if ( !mAudioStopped )
			{
				Log.w(TAG, "muxer audio: stop audio recorder");
				if ( mAudioRecorder != null ) mAudioRecorder.stop();
				mAudioStopped = true;
			}
		}

		if ( mAudioEncoderReady && (!mCopyVideo || mVideoEncoderReady) )
		{
			byte[] pcmByteArray = null;
			int read_size = mAudioRecorder.read(mPcmBuffer, PCM_MIN_BUFFER_SIZE); 
                    
			Log.w(TAG, "muxer audio:  PCM_MIN_BUFFER_SIZE : " + PCM_MIN_BUFFER_SIZE);
			Log.w(TAG, "muxer audio:  read_size : " + read_size);
                    
			if (read_size > 0)
			{
				pcmByteArray = Arrays.copyOf(mPcmBuffer.array(), read_size);
                        
				// convert to big endian 
				//pcmByteArray = convert2BigEndian(pcmByteArray, read_size);
                        
				if (pcmByteArray != null)
				{
					int inputBufferId = mAudioEncoder.dequeueInputBuffer(10001);
					Log.w(TAG, "muxer audio: input buffer id : " + inputBufferId);
					if (inputBufferId >= 0)
					{
						long presentationTime = getPresentationTime();
						Log.w(TAG, "muxer audio: input buffer id 1: " + inputBufferId);
						ByteBuffer inputBuffer = mAudioEncoder.getInputBuffer(inputBufferId);
						Log.w(TAG, "muxer audio: input buffer id 2: " + inputBufferId);
						inputBuffer.position(0);
						inputBuffer.put(pcmByteArray); 
						Log.w(TAG, "muxer audio: input buffer id 3: " + inputBufferId + " - " + presentationTime);
						mAudioEncoder.queueInputBuffer(inputBufferId, 0, read_size, presentationTime, mRecordingStopped ? MediaCodec.BUFFER_FLAG_END_OF_STREAM : 0);
						Log.w(TAG, "muxer audio: input buffer id 4: " + inputBufferId + " - " + presentationTime);
						//mAudioEncoder.queueInputBuffer(inputBufferId, inputBufferId, 0, read_size, presentationTime, 0);
					}
					//writeToDebugFile(pcmByteArray.length, pcmByteArray);
				}
			} else {                
				Log.w(TAG, "muxer audio: read_size is 0. AudioRecord should not happen, it is a block function");
			}
			//if ( mRecordingStopped ) mAudioRecorder.stop();
		}
	}

	/**
    * Sets the desired frame size.
    */
    private void setSize(int width, int height)
	{
		int maxsize = (width > height) ? width : height;
		if ( maxsize > 1280 )
		{
			width = width / 2;
			height = height / 2;
            Log.w(TAG, "Width is too big. Scaling down");
		}


        if ((width % 16) != 0 || (height % 16) != 0) {
            Log.w(TAG, "WARNING: width or height not multiple of 16");
        }
        mWidth = (int)(width/16) * 16;
        mHeight = (int)(height/16) * 16;
		imageSize = mWidth * mHeight * 3 / 2;
		mVideoRect = new Rect(0, 0, 0 + mWidth, 0 + mHeight);

		//frameData = new byte[imageSize];
		//Arrays.fill(frameData, (byte) 0);
		//imageBuffer = java.nio.ByteBuffer.allocateDirect(imageSize);
		//nativeSetImageBuffer(imageBuffer, imageSize);
    }

	/**
     * Returns a color format that is supported by the codec and by this test code.  If no
     * match is found, this throws a test failure -- the set of formats known to the test
     * should be expanded for new platforms.
     */
    private static int selectColorFormat(MediaCodecInfo codecInfo, String mimeType) {
        MediaCodecInfo.CodecCapabilities capabilities = codecInfo.getCapabilitiesForType(mimeType);
        for (int i = 0; i < capabilities.colorFormats.length; i++) {
            int colorFormat = capabilities.colorFormats[i];
            if (isRecognizedFormat(colorFormat)) {
                return colorFormat;
            }
        }
        Log.e(TAG,"couldn't find a good color format for " + codecInfo.getName() + " / " + mimeType);
        return 0;   // not reached
    }
    /**
     * Returns true if this is a color format that this test code understands (i.e. we know how
     * to read and generate frames in this format).
     */
    private static boolean isRecognizedFormat(int colorFormat) {
        switch (colorFormat) {
            // these are the formats we know how to handle for this test
            case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420Planar:
            case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedPlanar:
            case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420SemiPlanar:
            case MediaCodecInfo.CodecCapabilities.COLOR_FormatYUV420PackedSemiPlanar:
            case MediaCodecInfo.CodecCapabilities.COLOR_TI_FormatYUV420PackedSemiPlanar:
                return true;
            default:
                return false;
        }
    }

	private static int selectAudioSamplingRate(int channels, int encoding)
	{
		for (int rate : new int[] {44100, 22050, 16000, 11025, 8000})
		{  // add the rates you wish to check against
			int bufferSize = AudioRecord.getMinBufferSize(rate, channels, encoding);
			if (bufferSize > 0) {
				return rate;
			}
		}
		return 44100;
	}

	private long prevOutputPTSUs = 0;
	public long getPresentationTime()
	{
		long result = System.nanoTime() - startWhen - timeOffset;
		return result / 1000;
	}

	public void PauseRecording()
	{
		pauseWhen = System.nanoTime();
	}

	public void ResumeRecording()
	{
		timeOffset += System.nanoTime() - pauseWhen;
	}

	private STextureRender mTextureRender;

	/**
     * Code for rendering a texture onto a surface using OpenGL ES 2.0.
     */
    private static class STextureRender {
        private static final int FLOAT_SIZE_BYTES = 4;
        private static final int TRIANGLE_VERTICES_DATA_STRIDE_BYTES = 5 * FLOAT_SIZE_BYTES;
        private static final int TRIANGLE_VERTICES_DATA_POS_OFFSET = 0;
        private static final int TRIANGLE_VERTICES_DATA_UV_OFFSET = 3;
        private final float[] mTriangleVerticesData = {
                // X, Y, Z, U, V
                -1.0f, -1.0f, 0, 0.f, 0.f,
                 1.0f, -1.0f, 0, 1.f, 0.f,
                -1.0f,  1.0f, 0, 0.f, 1.f,
                 1.0f,  1.0f, 0, 1.f, 1.f,
        };

        private FloatBuffer mTriangleVertices;

        private static final String VERTEX_SHADER =
                "uniform mat4 uMVPMatrix;\n" +
                "uniform mat4 uSTMatrix;\n" +
                "attribute vec4 aPosition;\n" +
                "attribute vec4 aTextureCoord;\n" +
                "varying vec2 vTextureCoord;\n" +
                "void main() {\n" +
                "    gl_Position = uMVPMatrix * aPosition;\n" +
                "    vTextureCoord = (uSTMatrix * aTextureCoord).xy;\n" +
                "}\n";

        private static final String FRAGMENT_SHADER =
                "#extension GL_OES_EGL_image_external : require\n" +
                "precision mediump float;\n" +      // highp here doesn't seem to matter
                "varying vec2 vTextureCoord;\n" +
                "uniform samplerExternalOES sTexture;\n" +
                "void main() {\n" +
                "    gl_FragColor = texture2D(sTexture, vTextureCoord);\n" +
                "}\n";

        private float[] mMVPMatrix = new float[16];
        private float[] mSTMatrix = new float[16];

        private int mProgram;
        private int mTextureID = -12345;
        private int muMVPMatrixHandle;
        private int muSTMatrixHandle;
        private int maPositionHandle;
        private int maTextureHandle;

        public STextureRender() {
            mTriangleVertices = ByteBuffer.allocateDirect(
                    mTriangleVerticesData.length * FLOAT_SIZE_BYTES)
                    .order(ByteOrder.nativeOrder()).asFloatBuffer();
            mTriangleVertices.put(mTriangleVerticesData).position(0);

            Matrix.setIdentityM(mSTMatrix, 0);
        }

        public int getTextureId() {
            return mTextureID;
        }

        public void drawFrame(SurfaceTexture st) {
            checkGlError("onDrawFrame start");
            st.getTransformMatrix(mSTMatrix);

            // (optional) clear to green so we can see if we're failing to set pixels
            GLES20.glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
            GLES20.glClear(GLES20.GL_DEPTH_BUFFER_BIT | GLES20.GL_COLOR_BUFFER_BIT);

            GLES20.glUseProgram(mProgram);
            checkGlError("glUseProgram");

            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, mTextureID);

            mTriangleVertices.position(TRIANGLE_VERTICES_DATA_POS_OFFSET);
            GLES20.glVertexAttribPointer(maPositionHandle, 3, GLES20.GL_FLOAT, false,
                    TRIANGLE_VERTICES_DATA_STRIDE_BYTES, mTriangleVertices);
            checkGlError("glVertexAttribPointer maPosition");
            GLES20.glEnableVertexAttribArray(maPositionHandle);
            checkGlError("glEnableVertexAttribArray maPositionHandle");

            mTriangleVertices.position(TRIANGLE_VERTICES_DATA_UV_OFFSET);
            GLES20.glVertexAttribPointer(maTextureHandle, 2, GLES20.GL_FLOAT, false,
                    TRIANGLE_VERTICES_DATA_STRIDE_BYTES, mTriangleVertices);
            checkGlError("glVertexAttribPointer maTextureHandle");
            GLES20.glEnableVertexAttribArray(maTextureHandle);
            checkGlError("glEnableVertexAttribArray maTextureHandle");

            Matrix.setIdentityM(mMVPMatrix, 0);
            GLES20.glUniformMatrix4fv(muMVPMatrixHandle, 1, false, mMVPMatrix, 0);
            GLES20.glUniformMatrix4fv(muSTMatrixHandle, 1, false, mSTMatrix, 0);

            GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
            checkGlError("glDrawArrays");

            // IMPORTANT: on some devices, if you are sharing the external texture between two
            // contexts, one context may not see updates to the texture unless you un-bind and
            // re-bind it.  If you're not using shared EGL contexts, you don't need to bind
            // texture 0 here.
            GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, 0);
        }

        /**
         * Initializes GL state.  Call this after the EGL surface has been created and made current.
         */
        public void surfaceCreated() {

		Log.d(TAG, "muxer video creating STextureRender surfaceCreated 1");
            mProgram = createProgram(VERTEX_SHADER, FRAGMENT_SHADER);
            if (mProgram == 0) {
                throw new RuntimeException("failed creating program");
            }
            maPositionHandle = GLES20.glGetAttribLocation(mProgram, "aPosition");
            checkLocation(maPositionHandle, "aPosition");
            maTextureHandle = GLES20.glGetAttribLocation(mProgram, "aTextureCoord");
            checkLocation(maTextureHandle, "aTextureCoord");

            muMVPMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uMVPMatrix");
            checkLocation(muMVPMatrixHandle, "uMVPMatrix");
            muSTMatrixHandle = GLES20.glGetUniformLocation(mProgram, "uSTMatrix");
            checkLocation(muSTMatrixHandle, "uSTMatrix");

            int[] textures = new int[1];
            GLES20.glGenTextures(1, textures, 0);

            mTextureID = textures[0];
            GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, mTextureID);
            checkGlError("glBindTexture mTextureID");

            GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MIN_FILTER,
                    GLES20.GL_NEAREST);
            GLES20.glTexParameterf(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MAG_FILTER,
                    GLES20.GL_LINEAR);
            GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_WRAP_S,
                    GLES20.GL_CLAMP_TO_EDGE);
            GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_WRAP_T,
                    GLES20.GL_CLAMP_TO_EDGE);
            checkGlError("glTexParameter");

			Log.d(TAG, "muxer video creating STextureRender surfaceCreated 2");
        }

        /**
         * Replaces the fragment shader.  Pass in null to reset to default.
         */
        public void changeFragmentShader(String fragmentShader) {
            if (fragmentShader == null) {
                fragmentShader = FRAGMENT_SHADER;
            }
            GLES20.glDeleteProgram(mProgram);
            mProgram = createProgram(VERTEX_SHADER, fragmentShader);
            if (mProgram == 0) {
                throw new RuntimeException("failed creating program");
            }
        }

        private int loadShader(int shaderType, String source) {
            int shader = GLES20.glCreateShader(shaderType);
            checkGlError("glCreateShader type=" + shaderType);
            GLES20.glShaderSource(shader, source);
            GLES20.glCompileShader(shader);
            int[] compiled = new int[1];
            GLES20.glGetShaderiv(shader, GLES20.GL_COMPILE_STATUS, compiled, 0);
            if (compiled[0] == 0) {
                Log.e(TAG, "Could not compile shader " + shaderType + ":");
                Log.e(TAG, " " + GLES20.glGetShaderInfoLog(shader));
                GLES20.glDeleteShader(shader);
                shader = 0;
            }
            return shader;
        }

        private int createProgram(String vertexSource, String fragmentSource) {
            int vertexShader = loadShader(GLES20.GL_VERTEX_SHADER, vertexSource);
            if (vertexShader == 0) {
                return 0;
            }
            int pixelShader = loadShader(GLES20.GL_FRAGMENT_SHADER, fragmentSource);
            if (pixelShader == 0) {
                return 0;
            }

            int program = GLES20.glCreateProgram();
            if (program == 0) {
                Log.e(TAG, "Could not create program");
            }
            GLES20.glAttachShader(program, vertexShader);
            checkGlError("glAttachShader");
            GLES20.glAttachShader(program, pixelShader);
            checkGlError("glAttachShader");
            GLES20.glLinkProgram(program);
            int[] linkStatus = new int[1];
            GLES20.glGetProgramiv(program, GLES20.GL_LINK_STATUS, linkStatus, 0);
            if (linkStatus[0] != GLES20.GL_TRUE) {
                Log.e(TAG, "Could not link program: ");
                Log.e(TAG, GLES20.glGetProgramInfoLog(program));
                GLES20.glDeleteProgram(program);
                program = 0;
            }
            return program;
        }

        public void checkGlError(String op) {
            int error;
            while ((error = GLES20.glGetError()) != GLES20.GL_NO_ERROR) {
                Log.e(TAG, op + ": glError " + error);
                throw new RuntimeException(op + ": glError " + error);
            }
        }

        public static void checkLocation(int location, String label) {
            if (location < 0) {
                throw new RuntimeException("Unable to locate '" + label + "' in program");
            }
        }
    }

}