// Copyright 2018 Impreszions Solutions All Rights Reserved.
// Author : MunHon Chan

#include "CoreMinimal.h"

#if PLATFORM_ANDROID
extern FString GFilePathBase;
extern const FString &GetFileBasePath();
#endif

namespace RaveMobilePhotoGallery
{
	// Generate a filename based on the current time
	FString GetTimestampedFilename(FString extension, FString prefix = "", FString suffix = "")
	{
		// Credits to Rama
		// From Rama Victory Plugin: https://github.com/EverNewJoy/VictoryPlugin/blob/master/Source/VictoryBPLibrary/Private/VictoryBPFunctionLibrary.cpp
		// Copyright Permissions: https://forums.unrealengine.com/development-discussion/blueprint-visual-scripting/4014-39-rama-s-extra-blueprint-nodes-for-you-as-a-plugin-no-c-required

		const FDateTime Now = FDateTime::Now();
		FString NowWithMS = Now.ToString();
		int ms = Now.GetMillisecond();
		if (ms < 100) NowWithMS += "0";
		if (ms < 10) NowWithMS += "0";
		NowWithMS += FString::FromInt(Now.GetMillisecond());

		int32 utcms = FDateTime::UtcNow().GetMillisecond();
		//	FString filename = FString::FromInt(utcms) + ".png";// "Screenshot.png";
		FString filename = FApp::GetName() + "_" + NowWithMS;
		filename.ReplaceInline(TEXT("."), TEXT(""));
		filename.ReplaceInline(TEXT("-"), TEXT(""));

		if (!extension.IsEmpty() && !extension.StartsWith(".")) extension = "." + extension;
		return prefix + filename + suffix + extension;
	}

	// Generate a filename based on the current time
	FString GetTimestampedFilename(FString extension = "")
	{
		return GetTimestampedFilename(extension, "", "");
	}

	// Load file contents into a byte array
	bool LocalLoadFileToArray(TArray<uint8>& Result, const TCHAR* Filename, uint32 Flags = 0U)
	{
		return FFileHelper::LoadFileToArray(Result, Filename, Flags);
	}

	// Checks if a raw file path exists
	bool LocalCheckFileExists(const FString &FilePath)
	{
		return FPaths::FileExists(FilePath);
	}

	// Converts a relative path to its full path representation
	FString ConvertRelativePathToFull(FString Path)
	{
#if PLATFORM_ANDROID
		FString FullPath = Path;
		FullPath.RemoveFromStart(FPaths::RootDir());
		//extern FString GFilePathBase;
		//FPaths::CollapseRelativeDirectories(FullPath);
		//FString returnPath = GFilePathBase + FString("/UE4Game/") + FApp::GetGameName() + FString("/") + FullPath;
		FString returnPath = GetFileBasePath() + FullPath;
		FPaths::CollapseRelativeDirectories(returnPath);
		return returnPath;
		//		return GFilePathBase + FString("/UE4Game/") + FApp::GetGameName() + FString("/") + FullPath;
#elif PLATFORM_IOS
		return FPaths::ConvertRelativePathToFull(Path);
#else
		return FPaths::ConvertRelativePathToFull(Path);
#endif
	}

	static FString outputFolder = "";

	// Get an output folder to save most contents to
	FString GetOutputFolderPath(FString fileName, bool bAndroidSaveToExternal = false, FString AndroidSaveFolder = "")
	{
		if (outputFolder.IsEmpty())
		{
			outputFolder = FPaths::ProjectSavedDir();
		}

#if PLATFORM_ANDROID
		FString screenshotFilename = FPaths::GetCleanFilename(fileName);
		FString storagePath = bAndroidSaveToExternal ? (GFilePathBase / AndroidSaveFolder / screenshotFilename) : (outputFolder / AndroidSaveFolder / screenshotFilename);
		if ( !bAndroidSaveToExternal ) storagePath = ConvertRelativePathToFull(storagePath);
		return storagePath;
#else
		FString storagePath = outputFolder / fileName;
		FPaths::ConvertRelativePathToFull(storagePath);
		return storagePath;
#endif
	}

	// Set the output folder to use
	void SetOutputFolderPath(FString folderPath)
	{
		outputFolder = folderPath;
	}

	// Creates the folder if it does not currently exist
	bool EnsureFolderExists(FString Folder)
	{
		return IFileManager::Get().MakeDirectory(*Folder, true);
	}

	// Creates the folder that will contain the file if it does not currently exist
	bool EnsureFileFolderExists(FString File)
	{
		FString Folder = FPaths::GetPath(File);
		return IFileManager::Get().MakeDirectory(*Folder, true);
	}

	// Copys file from source to destination
	bool CopyFile(FString Src, FString Dest)
	{
		uint32 retVal = IFileManager::Get().Copy(*Dest, *Src, true, true);
		return (retVal == COPY_OK);
	}

	// Save byte array to a file (Android version)
	bool AndroidSaveArrayToFile(TArrayView<const uint8> Array, FString dest)
	{
#if PLATFORM_ANDROID
		FILE *to;
		const uint8* byteArray = const_cast<uint8*>(Array.GetData());
		int64 byteLength = Array.Num();

		/* open destination file */
		if ((to = fopen(TCHAR_TO_UTF8(*dest), "wb")) == NULL) {
			return false;
		}

		if (!fwrite(byteArray, sizeof(uint8), byteLength, to))
		{
			return false;
		}

		if (fclose(to) == EOF)
		{
			return false;
		}
#endif
		return true;
	}

	// JNI method to get Android Temporary Directory
	FString AndroidTemporaryDirectory()
	{
		static FString tempDir = "";
#if PLATFORM_ANDROID
		static bool bHasInit = false;
		if (!bHasInit)
		{
			if (JNIEnv* env = FAndroidApplication::GetJavaEnv())
			{
				//jclass activityClass = env->FindClass("android/app/NativeActivity");
				jmethodID getCacheDir = env->GetMethodID(FJavaWrapper::GameActivityClassID, "getCacheDir", "()Ljava/io/File;");
				jobject cache_dir = env->CallObjectMethod(FJavaWrapper::GameActivityThis, getCacheDir);
				jclass fileClass = env->FindClass("java/io/File");
				jmethodID getPath = env->GetMethodID(fileClass, "getPath", "()Ljava/lang/String;");
				jstring path_string = (jstring)env->CallObjectMethod(cache_dir, getPath);

				if (path_string != NULL)
				{
					const char* path_chars = env->GetStringUTFChars(path_string, 0);
					tempDir = FString(UTF8_TO_TCHAR(path_chars));
					env->ReleaseStringUTFChars(path_string, path_chars);
					env->DeleteLocalRef(path_string);
					bHasInit = true;
				}
			}
		}
#endif
		return tempDir;
	}

	FString ConvertToIOSPath(const FString& Filename, bool bForWrite)
	{
#if PLATFORM_IOS
		static FString DocumentReadPathBase = FString([NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]) + TEXT("/");
		static FString CookedPath = FString([[NSBundle mainBundle] bundlePath]) + TEXT("/cookeddata/");

		if (Filename.StartsWith(DocumentReadPathBase) || Filename.StartsWith(CookedPath)) return Filename;

		FString Result = Filename; // Filename.ToLower();
		Result.ReplaceInline(TEXT("../"), TEXT(""));
		Result.ReplaceInline(TEXT(".."), TEXT(""));
		Result.ReplaceInline(FPlatformProcess::BaseDir(), TEXT(""));

		if (bForWrite)
		{
			static FString WritePathBase = FString([NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]) + TEXT("/");
			return WritePathBase + Result;
		}
		else
		{
			// if filehostip exists in the command line, cook on the fly read path should be used
			FString Value;
			// Cache this value as the command line doesn't change...
			static bool bHasHostIP = FParse::Value(FCommandLine::Get(), TEXT("filehostip"), Value) || FParse::Value(FCommandLine::Get(), TEXT("streaminghostip"), Value);
			static bool bIsIterative = FParse::Value(FCommandLine::Get(), TEXT("iterative"), Value);
			if (bHasHostIP)
			{
				static FString ReadPathBase = DocumentReadPathBase; // FString([NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]) + TEXT("/");
				return ReadPathBase + Result.ToLower();
			}
			else if (bIsIterative)
			{
				static FString ReadPathBase = FString([NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0]) + TEXT("/");
				return ReadPathBase + Result.ToLower();
			}
			else
			{
				static FString ReadPathBase = CookedPath; // FString([[NSBundle mainBundle] bundlePath]) + TEXT("/cookeddata/");
														  //iPhoneLaunched = false;
				return ReadPathBase + Result;
			}
		}

		return Result;
#endif
		return Filename;
	}
}